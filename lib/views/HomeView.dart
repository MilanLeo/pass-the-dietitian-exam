import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pass_the_dietitian_exam/models/TopicsModel.dart';
import 'package:pass_the_dietitian_exam/utility/Impl/global.dart';
import 'package:pass_the_dietitian_exam/utility/Impl/helper.dart';
import 'package:pass_the_dietitian_exam/utility/Impl/session_impl.dart';
import 'package:pass_the_dietitian_exam/utility/Impl/utilities_impl.dart';
import 'package:pass_the_dietitian_exam/utility/endpoint/endpoint.dart';
import 'package:pass_the_dietitian_exam/utility/ui/color_pallete.dart';
import 'package:pass_the_dietitian_exam/utility/ui/customs/custom_button.dart';
import 'package:pass_the_dietitian_exam/utility/ui/styles.dart';
import 'package:pass_the_dietitian_exam/utility/ui/widgets.dart';
import 'package:pass_the_dietitian_exam/views/MockRulesView.dart';
import 'package:pass_the_dietitian_exam/views/SubTopicsView.dart';

class HomeView extends StatefulWidget {
  const HomeView({Key? key}) : super(key: key);

  @override
  State<HomeView> createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  RxBool isLoading = true.obs;
  RxList<TopicList> topicList = <TopicList>[].obs;

  @override
  void initState() {
    if (uc.isFirstTime) {
      apiLoader(asyncCall: () => updateDeviceInfoApi());
    } else {
      apiLoader(asyncCall: () => getTopics());
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.only(left: 20, right: 20),
        child: Column(
          children: [
            Obx(
              () => Expanded(
                child: isLoading.value
                    ? Container()
                    : topicList.isEmpty
                        ? Widgets.dataNotFound()
                        : ListView.builder(
                            padding: const EdgeInsets.only(top: 0, bottom: 8),
                            physics: const ClampingScrollPhysics(),
                            shrinkWrap: true,
                            itemBuilder: (context, index) {
                              return listItem(index);
                            },
                            itemCount: topicList.length),
              ),
            )
          ],
        ),
      ),
    );
  }

  listItem(int index) {
    return InkWell(
      onTap: () {
        if (topicList[index].isExam == 1) {
          Get.to(() => const MockRulesView(), arguments: [
            topicList[index].id,
            topicList[index].title,
            topicList[index].mockRule,
            topicList[index].time,
            topicList[index].isExam,
            0,
            topicList[index].isFree,
            false
          ]);
        } else {
          Get.to(() => const SubtopicsView(),
              arguments: [topicList[index].id, topicList[index].title, true],
              routeName: "/subTopic");
        }
      },
      child: Stack(
        alignment: Alignment.topRight,
        children: [
          Container(
            padding: const EdgeInsets.only(top: 11, bottom: 8),
            child: Container(
                padding: const EdgeInsets.only(
                    left: 16, right: 16, top: 24, bottom: 24),
                decoration: BoxDecoration(
                    border: Border.all(color: blackColor, width: 1),
                    color: pinkColor,
                    borderRadius: BorderRadius.circular(8)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    // Container(
                    //     width: 80,
                    //     height: 80,
                    //     padding: const EdgeInsets.all(8),
                    //     decoration: BoxDecoration(
                    //         borderRadius: BorderRadius.circular(16),
                    //         color: whiteColor
                    //     ),
                    //     child: Widgets.commonNetworkImage(networkImage: topicList[index].imageUrl)
                    // ),
                    // const SizedBox(width: 16),
                    Flexible(
                        child: Text(topicList[index].title,
                            style: textStyles.medium(
                                fontSize: 16.0, fontColor: blackColor))),
                    Text("Q : " + topicList[index].questionCounts.toString(),
                        style: textStyles.semiBold(
                            fontSize: 14.0, fontColor: themeColor)),
                  ],
                )),
          ),
          topicList[index].isFree == 1
              ? Padding(
                  padding: const EdgeInsets.only(right: 16),
                  child: Container(
                    padding: const EdgeInsets.only(
                        left: 10, right: 10, top: 3, bottom: 3),
                    decoration: const BoxDecoration(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(16)),
                        color: themeColor),
                    child: Text("Free",
                        style: textStyles.semiBold(
                            fontColor: whiteColor, fontSize: 12.0)),
                  ),
                )
              : const SizedBox.shrink()
        ],
      ),
    );
  }

  getTopics() {
    RequestManager.postRequest(
      uri: EndPoints.getTopics,
      hasBearer: true,
      retry: () {
        apiLoader(asyncCall: () => getTopics());
      },
      onStatusSuccess: (responseBody) {
        hideAppLoader();
        isLoading.value = false;
        LogicalComponents.topicModel = topicModelFromJson(responseBody);
        if (LogicalComponents.topicModel.status == 1) {
          topicList.value = LogicalComponents.topicModel.result!.topicList!;
        }
      },
      onFailure: (error) {
        hideAppLoader();
      },
    );
  }

  updateDeviceInfoApi() {
    var body = {
      "deviceType": GetPlatform.isIOS ? "IOS" : "ANDROID",
      "deviceToken": uc.deviceToken.value,
      "fcmToken": SessionImpl.getData(SessionKeys.keyFcmToken)
    };
    print(body);
    RequestManager.postRequest(
      uri: EndPoints.updateDeviceInfo,
      hasBearer: true,
      body: body,
      retry: () {
        updateDeviceInfoApi();
      },
      onStatusSuccess: (responseBody) {
        uc.isFirstTime = false;
        hideAppLoader();
        var res = jsonDecode(responseBody);
        if (res["status"] == 1) {
          uc.isSubscribe.value = res["result"]["isSubscribe"];
          uc.userName.value = res["result"]["userDeviceDetails"]["guestName"];
         // uc.id = res["result"]["userDeviceDetails"]["id"];

          uc.email.value = res["result"]["userDeviceDetails"]["email"];
          uc.firstName.value = res["result"]["userDeviceDetails"]["firstName"];
          uc.lastName.value = res["result"]["userDeviceDetails"]["lastName"];
          uc.profileImage.value =
              res["result"]["userDeviceDetails"]["profileImage"];
          uc.startDate.value = res["result"]["subscriptionStartDate"];
          uc.planName.value = res["result"]["subscriptionPlanName"];
          uc.planPrice.value = res["result"]["subscriptionPlanPrice"];
          global.iOSAppAPIVersion = res["result"]["IOSAppVersion"];
          global.androidAppAPIVersion = res["result"]["AndroidAppVersion"];
          checkAppUpdate();
        }
        if (GetPlatform.isIOS &&
            SessionImpl.getData(SessionKeys.paymentSuccess) == 1 &&
            uc.isSubscribe.value == 0) {
          apiLoader(asyncCall: () => createIOSPayment());
        } else {
          apiLoader(asyncCall: () => getTopics());
        }
      },
      onFailure: (error) {
        hideAppLoader();
        apiLoader(asyncCall: () => getTopics());
      },
    );
  }

  createIOSPayment() {
    var body = {
      "transactionId": SessionImpl.getData(SessionKeys.transId),
      "price": SessionImpl.getData(SessionKeys.planPrice),
      "status": "complete",
    };
    RequestManager.postRequest(
      uri: EndPoints.createIOSPayment,
      hasBearer: true,
      body: body,
      retry: () {
        createIOSPayment();
      },
      onStatusSuccess: (responseBody) {
        hideAppLoader();
        uc.isSubscribe.value = 1;
        apiLoader(asyncCall: () => getTopics());
      },
      onFailure: (error) {
        hideAppLoader();
        uc.isSubscribe.value = 1;
        apiLoader(asyncCall: () => getTopics());
      },
    );
  }

  Future checkAppUpdate() async {
    final version = await Helper().getVersion();
    try {
      if (GetPlatform.isIOS) {
        if (double.parse(global.iOSAppAPIVersion) > double.parse(version)) {
          forceUpdateBottomSheet();
        }
      } else {
        print(global.androidAppAPIVersion);
        print(version);
        if (double.parse(global.androidAppAPIVersion) <=
            double.parse(version)) {
        } else {
          forceUpdateBottomSheet();
        }
      }
    } on Exception catch (error) {
      print('version solving failed $error');
    }
  }

  void forceUpdateBottomSheet() {
    Get.bottomSheet(
      WillPopScope(
        onWillPop: () {
          return Future.value(false);
        },
        child: Container(
          padding: const EdgeInsets.all(20),
          color: bottomSheetBackgroundColor,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const SizedBox(height: 10),
              const Padding(
                padding: EdgeInsets.all(20.0),
                child: Text(
                  'App version expired, please update!',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: dimen.textLarge,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
              const Padding(
                padding: EdgeInsets.all(10.0),
                child: Text(
                  'The version of PassTheDietitian app is out of date and you must update to the latest version to continue.',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: dimen.textNormal,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
              const SizedBox(height: 15),
              CustomButton.normalButton(
                  title: "Update Now",
                  onTap: () {
                    Utilities.commonLaunchUrl(Platform.isAndroid ? "https://play.google.com/store/apps/details?id=com.app.dietitianexam" : "https://apps.apple.com/us/app/pass-the-dietitian-exam/id6443718211");
                    // LaunchReview.launch(
                    //         writeReview: false,
                    //         androidAppId: 'com.app.dietitianexam',
                    //         iOSAppId: '6443718211')
                    //     .then((value) {
                    //   // if (Get.isBottomSheetOpen!) Get.back();
                    // });
                  }),
              const SizedBox(height: 20),
            ],
          ),
        ),
      ),
      enableDrag: false,
      isDismissible: false,
      isScrollControlled: false,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: const Radius.circular(30),
        ),
      ),
      clipBehavior: Clip.antiAliasWithSaveLayer,
    );
  }
}
