import 'dart:async';
import 'dart:convert';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:pass_the_dietitian_exam/models/QuestionModel.dart';
import 'package:pass_the_dietitian_exam/utility/Impl/global.dart';
import 'package:pass_the_dietitian_exam/utility/endpoint/endpoint.dart';
import 'package:pass_the_dietitian_exam/utility/ui/color_pallete.dart';
import 'package:pass_the_dietitian_exam/utility/ui/customs/custom_button.dart';
import 'package:pass_the_dietitian_exam/utility/ui/images.dart';
import 'package:pass_the_dietitian_exam/utility/ui/styles.dart';
import 'package:pass_the_dietitian_exam/utility/ui/widgets.dart';
import 'package:pass_the_dietitian_exam/views/ResultView.dart';

class QuestionsView extends StatefulWidget {
  const QuestionsView({Key? key}) : super(key: key);

  @override
  _QuestionsViewState createState() => _QuestionsViewState();
}

class _QuestionsViewState extends State<QuestionsView> {
  RxBool isLoading = true.obs;
  RxList<QuestionList> questionList = <QuestionList>[].obs;
  var title = "";
  var topicId;
  var subTopicId;
  var isExam;
  var isDomain;
  var domainName;
  var timerStart = 0.obs;
  var totalTime = 0;
  Timer? _timer;
  var currentQuestion = 0.obs;
  var lastQuestion = 0.obs;

  void startTimer() {
    if (_timer != null) {
      _timer!.cancel();
    }
    const oneSec = const Duration(seconds: 1);
    _timer = Timer.periodic(
      oneSec,
      (Timer timer) {
        if (timerStart.value < 1) {
          _timer!.cancel();
          var questionData = [];
          for (var element in questionList) {
            for (var que in element.answerOptions!) {
              if (que.isSelected.value) {
                questionData.add(que);
                print(que);
              }
            }
          }
          if (questionData.isEmpty) {
            Get.back();
            Widgets.showSnackBar(
                title: "Error", errorMessage: "Time over please try again.");
          } else {
            apiLoader(asyncCall: () => submitQuestionApi(questionData,true));
          }
        } else {
          timerStart.value = timerStart.value - 1;
        }
      },
    );
  }

  @override
  void initState() {
    if (Get.arguments != null) {
      topicId = Get.arguments[0];
      title = Get.arguments[1];
      timerStart.value = Get.arguments[2];
      isExam = Get.arguments[3];
      subTopicId = Get.arguments[4];
      isDomain = Get.arguments[5];
      domainName = Get.arguments[6];
      totalTime = timerStart.value;
    }
    apiLoader(asyncCall: () => getQuestionApi());
    super.initState();
  }

  @override
  void dispose() {
    _timer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return PopScope(
      canPop: false,
      onPopInvoked: (y){
        // var questionData = [];
        // for (var element in questionList) {
        //   for (var que in element.answerOptions!) {
        //     if (que.isSelected.value) {
        //       questionData.add(que);
        //       print(que.toJson());
        //     }
        //   }
        // }
        // if (questionData.isNotEmpty) {
        //   submitAnswerConformPopUp(questionData);
        // } else {
        //   Get.back();
        // }
    },
      child: Scaffold(

        backgroundColor: Colors.white,
        body: SafeArea(
          child: Padding(
            padding: const EdgeInsets.only(top: 8),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                InkWell(
                  onTap: () {
                    var questionData = [];
                    for (var element in questionList) {
                      for (var que in element.answerOptions!) {
                        if (que.isSelected.value) {
                          questionData.add(que);
                          print(que.toJson());
                        }
                      }
                    }
                    if (questionData.isNotEmpty) {
                      submitAnswerConformPopUp(questionData);
                    } else {
                      Get.back();
                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(left: 20, right: 20),
                    child: Row(
                      children: [
                        SvgPicture.asset(SvgImages.backIc),
                        const SizedBox(width: 12),
                        Flexible(
                            child: Text(title,
                                maxLines: 2,
                                style: textStyles.semiBold(
                                    fontSize: 20.0, fontColor: blackColor))),
                      ],
                    ),
                  ),
                ),
                const SizedBox(height: 8),
                Container(height: 2, color: deviderColor),
                timerStart.value == 0
                    ? const SizedBox.shrink()
                    : Padding(
                        padding:
                            const EdgeInsets.only(left: 20, right: 20, top: 16),
                        child: Obx(() => Center(
                            child: Text(
                                "Time : ${formatHHMMSS(timerStart.value).isEmpty ? "" : formatHHMMSS(timerStart.value)}",
                                style: textStyles.semiBold(
                                    fontSize: 18.0, fontColor: themeColor)))),
                      ),
                Obx(() => isLoading.value
                    ? const SizedBox.shrink()
                    : Padding(
                        padding: const EdgeInsets.only(
                            left: 20, right: 20, bottom: 16, top: 16),
                        child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                                "Question ${currentQuestion.value+1} OF ${questionList.length}",
                                style: textStyles.semiBold(
                                    fontSize: 12.0, fontColor: greyColor))),
                      )),
                Obx(
                  () => Expanded(
                    child: isLoading.value
                        ? const SizedBox.shrink()
                        : questionList.isEmpty
                            ? Widgets.dataNotFound()
                            : SingleChildScrollView(
                                padding:
                                    const EdgeInsets.only(left: 20, right: 20),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                        questionList[currentQuestion.value]
                                            .questions,
                                        style: textStyles.medium(
                                            fontSize: 16.0,
                                            fontColor: blackColor)),
                                    const SizedBox(height: 16),
                                    ListView.builder(
                                        padding: const EdgeInsets.only(
                                            top: 0, bottom: 16),
                                        physics: const ClampingScrollPhysics(),
                                        shrinkWrap: true,
                                        itemBuilder: (context, index) {
                                          return Padding(
                                            padding: const EdgeInsets.only(
                                                top: 8, bottom: 8),
                                            child: InkWell(
                                              onTap: () {
                                                for (var element in questionList[
                                                        currentQuestion.value]
                                                    .answerOptions!) {
                                                  element.isSelected.value =
                                                      false;
                                                  element.color.value =
                                                      whiteColor;
                                                }
                                                questionList[
                                                        currentQuestion.value]
                                                    .answerOptions![index]
                                                    .isSelected
                                                    .value = true;
                                                if (isExam == 0) {
                                                  if (questionList[currentQuestion
                                                              .value]
                                                          .answerOptions![index]
                                                          .isCorrect ==
                                                      1) {
                                                    questionList[
                                                            currentQuestion.value]
                                                        .answerOptions![index]
                                                        .color
                                                        .value = themeColor;
                                                  } else {
                                                    questionList[
                                                            currentQuestion.value]
                                                        .answerOptions![index]
                                                        .color
                                                        .value = redColor;
                                                    for (var element
                                                        in questionList[
                                                                currentQuestion
                                                                    .value]
                                                            .answerOptions!) {
                                                      if (element.isCorrect ==
                                                          1) {
                                                        element.color.value =
                                                            themeColor;
                                                      }
                                                    }
                                                  }
                                                } else {
                                                  if (questionList[
                                                          currentQuestion.value]
                                                      .answerOptions![index]
                                                      .isSelected
                                                      .value) {
                                                    questionList[
                                                            currentQuestion.value]
                                                        .answerOptions![index]
                                                        .color
                                                        .value = themeColor;
                                                  }
                                                }
                                              },
                                              child: Obx(
                                                () => Container(
                                                  padding:
                                                      const EdgeInsets.all(16),
                                                  decoration: BoxDecoration(
                                                      color: questionList[
                                                              currentQuestion
                                                                  .value]
                                                          .answerOptions![index]
                                                          .color
                                                          .value,
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              12),
                                                      border: Border.all(
                                                          color: borderColor,
                                                          width: 1.5)),
                                                  child: Row(
                                                    children: [
                                                      Text(
                                                          questionList[
                                                                      currentQuestion
                                                                          .value]
                                                                  .answerOptions![
                                                                      index]
                                                                  .position +
                                                              ")",
                                                          style: textStyles.semiBold(
                                                              fontColor: questionList[currentQuestion
                                                                              .value]
                                                                          .answerOptions![
                                                                              index]
                                                                          .color
                                                                          .value ==
                                                                      whiteColor
                                                                  ? blackColor
                                                                  : whiteColor,
                                                              fontSize: 16.0)),
                                                      const SizedBox(width: 6),
                                                      SvgPicture.asset(questionList[
                                                                      currentQuestion
                                                                          .value]
                                                                  .answerOptions![
                                                                      index]
                                                                  .color
                                                                  .value ==
                                                              redColor
                                                          ? SvgImages.wrongIc
                                                          : questionList[currentQuestion
                                                                          .value]
                                                                      .answerOptions![
                                                                          index]
                                                                      .color
                                                                      .value ==
                                                                  themeColor
                                                              ? SvgImages.rightIc
                                                              : SvgImages
                                                                  .circleIc),
                                                      const SizedBox(width: 6),
                                                      Flexible(
                                                          child: Text(
                                                              questionList[
                                                                      currentQuestion
                                                                          .value]
                                                                  .answerOptions![
                                                                      index]
                                                                  .answer,
                                                              style: textStyles.medium(
                                                                  fontColor: questionList[currentQuestion.value]
                                                                              .answerOptions![
                                                                                  index]
                                                                              .color
                                                                              .value ==
                                                                          whiteColor
                                                                      ? blackColor
                                                                      : whiteColor,
                                                                  fontSize:
                                                                      14.0)))
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ),
                                          );
                                        },
                                        itemCount:
                                            questionList[currentQuestion.value]
                                                .answerOptions!
                                                .length)
                                  ],
                                ),
                              ),
                  ),
                ),
                Obx(() => isLoading.value || questionList.isEmpty
                    ? const SizedBox.shrink()
                    : Padding(
                        padding: const EdgeInsets.only(
                            left: 20, right: 20, bottom: 16),
                        child: Column(
                          children: [
                            isExam == 1
                                ? const SizedBox.shrink()
                                : InkWell(
                                    onTap: () {
                                      var isOptionSelected =
                                          questionList[currentQuestion.value]
                                              .answerOptions!
                                              .where((element) =>
                                                  element.isSelected.value ==
                                                  true);
                                      if (isOptionSelected.isNotEmpty) {
                                        retionalPopup(
                                            questionList[currentQuestion.value]
                                                .answerDescription);
                                      }
                                    },
                                    child: Container(
                                      padding: const EdgeInsets.all(12),
                                      width: 55,
                                      height: 55,
                                      decoration: const BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: pinkColor),
                                      child: SvgPicture.asset(
                                          questionList[currentQuestion.value]
                                                  .answerOptions!
                                                  .where((element) =>
                                                      element.isSelected.value ==
                                                      true)
                                                  .isEmpty
                                              ? SvgImages.retionalDisableIc
                                              : SvgImages.retionalIc),
                                    ),
                                  ),
                            isExam == 1
                                ? const SizedBox.shrink()
                                : Text("Rationale",
                                    style: textStyles.medium(
                                        fontSize: 12.0, fontColor: blackColor)),
                            const SizedBox(height: 16),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                isExam == 0
                                    ? Expanded(
                                        child: Obx(() =>
                                            CustomButton.normalButton(
                                                title: "Previous",
                                                backgroundColor:
                                                    currentQuestion.value == 0 ||currentQuestion.value<=lastQuestion.value
                                                        ? buttonDisable
                                                        : themeColor,
                                                txtColor:
                                                    currentQuestion.value == 0 ||currentQuestion.value<=lastQuestion.value
                                                        ? blackColor
                                                        : whiteColor,
                                                onTap: () {
                                                  if( currentQuestion.value<=lastQuestion.value){}else{
                                                  if (currentQuestion.value > 0) {
                                                    currentQuestion.value--;
                                                  }}
                                                })))
                                    : const SizedBox.shrink(),
                                SizedBox(width: isExam == 0 ? 20 : 0),
                                Expanded(
                                    child: CustomButton.normalButton(
                                        title: "Next",
                                        backgroundColor: themeColor,
                                        onTap: () {
                                          Get.closeAllSnackbars();
                                          if (isExam == 1 &&
                                              questionList[currentQuestion.value]
                                                  .answerOptions!
                                                  .where((element) =>
                                                      element.isSelected.value ==
                                                      true)
                                                  .isEmpty) {
                                            Widgets.showSnackBar(
                                                title: "Error",
                                                errorMessage:
                                                    "You have not selected any answer!");
                                          } else {
                                            if (currentQuestion.value + 1 <
                                                questionList.length) {
                                              currentQuestion.value++;
                                            } else {
                                              var questionData = [];
                                              for (var element in questionList) {
                                                for (var que in element.answerOptions!) {
                                                  if (que.isSelected.value) {
                                                    questionData.add(que);
                                                  }
                                                }
                                              }
                                              _timer?.cancel();
                                              if (questionData.isEmpty) {
                                                Widgets.showSnackBar(
                                                    title: "Error",
                                                    errorMessage:
                                                        "You must attempt at least 1 question");
                                              } else {
                                                apiLoader(
                                                    asyncCall: () =>
                                                        submitQuestionApi(
                                                            questionData,true));
                                              }
                                            }
                                          }
                                        }))
                              ],
                            )
                          ],
                        ),
                      )),
              ],
            ),
          ),
        ),
      ),
    );
  }

  retionalPopup(answerDescription) {
    return Get.bottomSheet(
        BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 3, sigmaY: 3),
          child: Container(
              constraints: BoxConstraints(
                maxHeight: Get.height - 200,
              ),
              decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(24),
                      topRight: Radius.circular(24)),
                  color: whiteColor),
              child: Padding(
                padding: const EdgeInsets.only(
                    left: 20.0, right: 20.0, top: 20.0, bottom: 16.0),
                child: SafeArea(
                  bottom: true,
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Container(
                          height: 5,
                          width: 60,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              color: themeDisable),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 32, bottom: 16),
                          child: SvgPicture.asset(
                            SvgImages.retionalIc,
                            height: 50,
                            width: 50,
                          ),
                        ),
                        Text(
                          "Rationale",
                          textAlign: TextAlign.center,
                          style: textStyles.semiBold(
                              fontSize: 20.0, fontColor: blackColor),
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                        Text(
                          answerDescription,
                          textAlign: TextAlign.center,
                          style: textStyles.medium(
                              fontSize: 14.0, fontColor: blackColor),
                        ),
                      ],
                    ),
                  ),
                ),
              )),
        ),
        isScrollControlled: true);
  }

  getQuestionApi() {
    var body = {"topicId": subTopicId == 0 ? topicId : subTopicId};
    print(body);
    RequestManager.postRequest(
      uri: EndPoints.getQuestion,
      hasBearer: true,
      body: body,
      retry: () {
        apiLoader(asyncCall: () => getQuestionApi());
      },
      onStatusSuccess: (responseBody) {
        hideAppLoader();
        print(responseBody);
        isLoading.value = false;
        LogicalComponents.questionModel = questionModelFromJson(responseBody);
        if (LogicalComponents.questionModel.status == 1) {
          questionList.value =
              LogicalComponents.questionModel.result!.questionList!;
          if( LogicalComponents.questionModel.result!.examRemainingTime != 0){
            timerStart.value = LogicalComponents.questionModel.result!.examRemainingTime!;
          }
          for(int i =0;i<questionList.length;i++){
            if(questionList[i].id == LogicalComponents.questionModel.result!.lastQuestionId){
                currentQuestion.value = i+1;
                lastQuestion.value = i+1;
            }
          }
        }
        if (timerStart.value > 0) {
          startTimer();
        }
      },
      onFailure: (error) {
        hideAppLoader();
      },
    );
  }

  goToQuestion() {
    return Get.to(() => ResultView(), arguments: [title, isDomain, domainName])!
        .then((value) {
      timerStart.value = totalTime;
      currentQuestion.value = 0;
      apiLoader(asyncCall: () => getQuestionApi());
    });
  }

  submitQuestionApi(List questionData,bool isDone) {
    var body = {
      "topicId": topicId,
      "subTopicId":subTopicId,
      "questionAnswerDetails": questionData,
      "examRemainingTime": timerStart.value,
      "isCompletedAnswer": isDone?1:0
    };
    print(body);
    print("!@");

    RequestManager.postRequest(
      uri: EndPoints.submitQuestion,
      hasBearer: true,
      body: body,
      onStatusSuccess: (responseBody) {
        hideAppLoader();
        print(responseBody);
        var res = jsonDecode(responseBody);
        if (res["status"] == 1) {
          if(isDone){
          goToQuestion();}else{
            Get.back();
          }
        } else {
          Widgets.showSnackBar(title: "Error", errorMessage: res["message"]);
        }
      },
      onFailure: (error) {
        hideAppLoader();
      },
    );
  }

  submitAnswerConformPopUp(questionData) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text("Save Answer",
                    style:
                        textStyles.bold(fontColor: blackColor, fontSize: 20.0)),
                const SizedBox(height: 8),
                Text("Are you sure you want to save answer?",
                    style: textStyles.medium(
                        fontColor: blackColor, fontSize: 16.0)),
                const SizedBox(height: 16),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                        child: CustomButton.normalButton(
                            title: "Cancel",
                            backgroundColor: buttonDisable,
                            txtColor: blackColor,
                            onTap: () {
                              Get.back();
                              Get.back();
                            })),
                    const SizedBox(width: 20),
                    Expanded(
                        child: CustomButton.normalButton(
                            title: "Ok",
                            backgroundColor: themeColor,
                            onTap: () {
                              Get.back();
                              print(currentQuestion.value);
                              print(LogicalComponents.questionModel.result!.questionList!.length-1);
                              apiLoader(
                                  asyncCall: () =>
                                      submitQuestionApi(questionData,currentQuestion.value == LogicalComponents.questionModel.result!.questionList!.length-1));
                            }))
                  ],
                ),
              ],
            ),
          );
        });
  }
}
