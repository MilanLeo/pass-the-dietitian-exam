import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pass_the_dietitian_exam/utility/Impl/global.dart';
import 'package:pass_the_dietitian_exam/utility/Impl/session_impl.dart';
import 'package:pass_the_dietitian_exam/utility/endpoint/endpoint.dart';
import 'package:pass_the_dietitian_exam/utility/ui/color_pallete.dart';
import 'package:pass_the_dietitian_exam/utility/ui/customs/custom_keyboard_avoider.dart';
import 'package:pass_the_dietitian_exam/utility/ui/customs/custom_textfields.dart';
import 'package:pass_the_dietitian_exam/utility/ui/styles.dart';
import 'package:pass_the_dietitian_exam/utility/ui/widgets.dart';
import 'package:pass_the_dietitian_exam/views/DashboardView.dart';
import '../utility/ui/customs/custom_button.dart';

class SignupView extends StatefulWidget {
  const SignupView({Key? key}) : super(key: key);

  @override
  _SignupViewState createState() => _SignupViewState();
}

class _SignupViewState extends State<SignupView> {
  RxBool checkError = false.obs;
  RxBool passwordVisibility = false.obs;
  RxBool confirmPasswordVisibility = false.obs;
  TextEditingController txtFirstName = TextEditingController();
  TextEditingController txtLastName = TextEditingController();
  TextEditingController txtEmail = TextEditingController();
  TextEditingController txtPassword = TextEditingController();
  TextEditingController txtConfirmPassword = TextEditingController();
  final GlobalKey<CustomTextFieldState> firstNameState =
      GlobalKey<CustomTextFieldState>();
  final GlobalKey<CustomTextFieldState> lastNameState =
      GlobalKey<CustomTextFieldState>();
  final GlobalKey<CustomTextFieldState> emailState =
      GlobalKey<CustomTextFieldState>();
  final GlobalKey<CustomTextFieldState> passwordState =
      GlobalKey<CustomTextFieldState>();
  final GlobalKey<CustomTextFieldState> confirmPasswordState =
      GlobalKey<CustomTextFieldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(left: 20, right: 20, top: 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("Welcome!",
                      style: textStyles.semiBold(
                          fontSize: 30.0, fontColor: blackColor)),
                  InkWell(
                      onTap: () {
                        Get.offAll(() => const Dashboard());
                      },
                      child: Text("SKIP>>",
                          style: textStyles.medium(
                              fontColor: themeColor, fontSize: 18.0)))
                ],
              ),
              Text("Hello, Sign up to create a new account",
                  style: textStyles.medium(
                      fontSize: 14.0, fontColor: lightGrayColor)),
              const SizedBox(height: 32),
              Expanded(
                child: KeyboardAvoider(
                  autoScroll: true,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("First Name",
                          style: textStyles.normal(
                              fontSize: 14.0, fontColor: blackColor)),
                      const SizedBox(height: 8),
                      CustomTextField(
                        key: firstNameState,
                        controller: txtFirstName,
                        validateTypes: ValidateTypes.name,
                        errorMsg: "first name",
                        textInputType: TextInputType.text,
                        textInputAction: TextInputAction.next,
                        hintText: "Enter first name",
                      ),
                      const SizedBox(
                        height: 24,
                      ),
                      Text("Last Name",
                          style: textStyles.normal(
                              fontSize: 14.0, fontColor: blackColor)),
                      const SizedBox(height: 8),
                      CustomTextField(
                        key: lastNameState,
                        controller: txtLastName,
                        validateTypes: ValidateTypes.name,
                        errorMsg: "last name",
                        textInputType: TextInputType.text,
                        textInputAction: TextInputAction.next,
                        hintText: "Enter last name",
                      ),
                      const SizedBox(
                        height: 24,
                      ),
                      Text("E-mail Address",
                          style: textStyles.normal(
                              fontSize: 14.0, fontColor: blackColor)),
                      const SizedBox(height: 8),
                      CustomTextField(
                        key: emailState,
                        controller: txtEmail,
                        validateTypes: ValidateTypes.email,
                        errorMsg: "E-mail",
                        textInputType: TextInputType.emailAddress,
                        textInputAction: TextInputAction.next,
                        hintText: "Enter E-mail",
                      ),
                      const SizedBox(
                        height: 24,
                      ),
                      Text("Password",
                          style: textStyles.normal(
                              fontSize: 14.0, fontColor: blackColor)),
                      const SizedBox(
                        height: 8,
                      ),
                      Obx(
                        () => CustomTextField(
                            maxLength: 16,
                            key: passwordState,
                            controller: txtPassword,
                            validateTypes: ValidateTypes.password,
                            errorMsg: "password",
                            textInputType: TextInputType.visiblePassword,
                            textInputAction: TextInputAction.next,
                            hintText: "Enter password",
                            obscureText: passwordVisibility.value,
                            suffixIcon: IconButton(
                              onPressed: () {
                                passwordVisibility.value =
                                    !passwordVisibility.value;
                              },
                              icon: Icon(
                                passwordVisibility.value
                                    ? Icons.visibility_outlined
                                    : Icons.visibility_off_outlined,
                                color: eyeColor,
                              ),
                            )),
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                      Text("Confirm Password",
                          style: textStyles.normal(
                              fontSize: 14.0, fontColor: blackColor)),
                      const SizedBox(
                        height: 8,
                      ),
                      Obx(
                        () => CustomTextField(
                            maxLength: 16,
                            key: confirmPasswordState,
                            controller: txtConfirmPassword,
                            validateTypes: ValidateTypes.password,
                            errorMsg: "confirm password",
                            textInputType: TextInputType.visiblePassword,
                            textInputAction: TextInputAction.done,
                            hintText: "Enter password",
                            obscureText: confirmPasswordVisibility.value,
                            suffixIcon: IconButton(
                              onPressed: () {
                                confirmPasswordVisibility.value =
                                    !confirmPasswordVisibility.value;
                              },
                              icon: Icon(
                                confirmPasswordVisibility.value
                                    ? Icons.visibility_outlined
                                    : Icons.visibility_off_outlined,
                                color: eyeColor,
                              ),
                            )),
                      ),
                      const SizedBox(height: 32),
                      CustomButton.normalButton(
                          title: "SUBMIT",
                          onTap: () {
                            Get.closeAllSnackbars();
                            FocusScope.of(context).requestFocus(FocusNode());
                            var isValid = true;
                            if (firstNameState.currentState!
                                .checkValidation(false)) {
                              isValid = false;
                            }
                            if (lastNameState.currentState!
                                .checkValidation(false)) {
                              isValid = false;
                            }
                            if (emailState.currentState!
                                .checkValidation(false)) {
                              isValid = false;
                            }
                            if (passwordState.currentState!
                                .checkValidation(false)) {
                              isValid = false;
                            }
                            if (confirmPasswordState.currentState!
                                .checkValidation(false)) {
                              isValid = false;
                            }
                            if (isValid) {
                              if (txtPassword.text != txtConfirmPassword.text) {
                                Widgets.showSnackBar(
                                    title: "Error",
                                    errorMessage:
                                        "Password and confirm password should be same");
                              } else {
                                apiLoader(asyncCall: () => registerUser());
                              }
                            }
                          }),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 16, top: 16),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "Already have an account?",
                      style: textStyles.normal(
                          fontSize: 16.0, fontColor: blackColor),
                    ),
                    InkWell(
                      onTap: () {
                        Get.back();
                      },
                      child: Text(
                        "  Log in",
                        style: textStyles.semiBold(
                            fontSize: 16.0, fontColor: themeColor),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  registerUser() {
    var body = {
      "firstName": txtFirstName.text,
      "lastName": txtLastName.text,
      "email": txtEmail.text,
      "password": txtPassword.text,
      "confirmPassword": txtConfirmPassword.text,
      "deviceToken": uc.deviceToken.value,
    };
    RequestManager.postRequest(
      uri: EndPoints.register,
      hasBearer: false,
      body: body,
      onStatusSuccess: (responseBody) {
        hideAppLoader();
        var res = jsonDecode(responseBody);
        if (res["status"] == 1) {
          SessionImpl.setData(SessionKeys.keyLogin, true);
          SessionImpl.setData(
              SessionKeys.keyBearerToken, res["result"]["accessToken"]);
          uc.isLogin.value = true;
          uc.isSubscribe.value = res["result"]["userDetail"]["isSubscribe"];
          uc.userName.value = res["result"]["userDetail"]["firstName"] +
              res["result"]["userDetail"]["lastName"];
          uc.email.value = res["result"]["userDetail"]["email"];
          uc.profileImage.value = res["result"]["userDetail"]["profileImage"];
          uc.firstName.value = res["result"]["userDetail"]["firstName"];
          uc.lastName.value = res["result"]["userDetail"]["lastName"];
          Get.offAll(() => const Dashboard());
        } else {
          Widgets.showSnackBar(title: "Error", errorMessage: res["message"]);
        }
      },
      onFailure: (error) {
        hideAppLoader();
      },
    );
  }
}
