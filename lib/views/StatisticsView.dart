import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pass_the_dietitian_exam/models/StatisticsModel.dart';
import 'package:pass_the_dietitian_exam/utility/Impl/global.dart';
import 'package:pass_the_dietitian_exam/utility/endpoint/endpoint.dart';
import 'package:pass_the_dietitian_exam/utility/ui/color_pallete.dart';
import 'package:pass_the_dietitian_exam/utility/ui/widgets.dart';
import 'package:pass_the_dietitian_exam/views/StatisticsDetail.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:pass_the_dietitian_exam/utility/ui/styles.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class StatisticsView extends StatefulWidget {
  const StatisticsView({Key? key}) : super(key: key);

  @override
  _StatisticsViewState createState() => _StatisticsViewState();
}

class _StatisticsViewState extends State<StatisticsView> {
  var score = 0.0.obs;
  RxBool isLoading = true.obs;
  RxList<Statistics> dataList = <Statistics>[].obs;

  late List<_ChartData> data;
  late TooltipBehavior _tooltip;

  @override
  void initState() {
    data = [];
    _tooltip = TooltipBehavior(enable: false);
    if (uc.isLogin.value) {
      apiLoader(asyncCall: () => getStatisticsApi());
    } else {
      isLoading.value = false;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Expanded(
          child: isLoading.value
              ? const SizedBox.shrink()
              : dataList.isNotEmpty
                  ? Padding(
                      padding: const EdgeInsets.only(
                          left: 20, right: 20, top: 8, bottom: 0),
                      child: SingleChildScrollView(
                        child: Column(
                          children: [
                            Container(
                                padding: const EdgeInsets.all(16),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(16),
                                    color: pinkColor),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Expanded(
                                      child: Text("Overall Statistics",
                                          maxLines: 2,
                                          overflow: TextOverflow.ellipsis,
                                          style: textStyles.semiBold(
                                              fontSize: 20.0,
                                              fontColor: blackColor)),
                                    ),
                                    CircularPercentIndicator(
                                      backgroundColor: pinkColor,
                                      radius: 100.0,
                                      lineWidth: 16.0,
                                      percent: score.value / 100,
                                      center: Text(
                                          "${score.value.toStringAsFixed(0)}%",
                                          style: textStyles.semiBold(
                                              fontColor: blackColor,
                                              fontSize: 22.0)),
                                      progressColor: blueColor,
                                    )
                                  ],
                                )),
                            const SizedBox(height: 20),
                            SfCartesianChart(
                              primaryXAxis:
                                  const CategoryAxis(isVisible: false),
                              primaryYAxis: const NumericAxis(
                                  minimum: 0, maximum: 100, interval: 10),
                              tooltipBehavior: _tooltip,
                              series: <CartesianSeries<dynamic, dynamic>>[
                                ...<CartesianSeries<dynamic, dynamic>>[
                                  ColumnSeries<_ChartData, String>(
                                    dataSource: data,
                                    xValueMapper: (_ChartData data, _) =>
                                        data.x,
                                    yValueMapper: (_ChartData data, _) =>
                                        data.y,
                                    pointColorMapper: (_ChartData data, _) =>
                                        data.color,
                                  ),
                                ],
                              ],
                            ),
                            const SizedBox(height: 20),
                            ListView.builder(
                                padding:
                                    const EdgeInsets.only(top: 0, bottom: 0),
                                physics: const ClampingScrollPhysics(),
                                shrinkWrap: true,
                                itemBuilder: (context, index) {
                                  return listItem(index);
                                },
                                itemCount: dataList.length)
                          ],
                        ),
                      ),
                    )
                  : Widgets.dataNotFound()),
    );
  }

  listItem(int index) {
    return InkWell(
      onTap: () {
        Get.to(() => const StatisticsDetail(),
            arguments: [dataList[index].title, dataList[index].id,]);
      },
      child: Padding(
        padding: const EdgeInsets.only(bottom: 20),
        child: Container(
            padding: const EdgeInsets.all(16),
            decoration: BoxDecoration(
                color: Color(int.parse(dataList[index]
                    .resultBarColor
                    .toString()
                    .replaceAll("#", "0xff"))),
                borderRadius: BorderRadius.circular(8)),
            child: Row(
              children: [
                Flexible(
                    child: Text(
                        "${dataList[index].title} (${dataList[index].percentage.toStringAsFixed(0)}%)",
                        style: textStyles.medium(
                            fontSize: 16.0, fontColor: blackColor))),
              ],
            )
        ),
      ),
    );
  }

  getStatisticsApi() {
    RequestManager.postRequest(
      uri: EndPoints.getStatistics,
      hasBearer: true,
      onStatusSuccess: (responseBody) {
        hideAppLoader();
        isLoading.value = false;
        LogicalComponents.statisticsModel =
            statisticsModelFromJson(responseBody);
        if (LogicalComponents.statisticsModel.status == 1) {
          score.value = LogicalComponents.statisticsModel.overAllStatistics!;
          dataList.value = LogicalComponents.statisticsModel.result!;
          dataList.forEach((element) {
            if (element.isShowBar == 1) {
              var barColor =
                  element.resultBarColor.toString().replaceAll("#", "0xff");
              data.add(_ChartData(element.id.toString(), element.percentage,
                  Color(int.parse(barColor))));
            }
          });
        }
      },
      onFailure: (error) {
        hideAppLoader();
      },
    );
  }
}

class _ChartData {
  _ChartData(this.x, this.y, this.color);

  final String x;
  final double y;
  final Color color;
}
