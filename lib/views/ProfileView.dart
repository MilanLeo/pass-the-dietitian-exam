import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:get/get_core/get_core.dart';
import 'package:pass_the_dietitian_exam/utility/Impl/global.dart';
import 'package:pass_the_dietitian_exam/utility/ui/images.dart';
import 'package:pass_the_dietitian_exam/utility/ui/styles.dart';
import 'package:pass_the_dietitian_exam/utility/ui/widgets.dart';
import 'package:pass_the_dietitian_exam/views/EditProfileView.dart';
import '../utility/ui/color_pallete.dart';

class ProfileView extends StatefulWidget {
  const ProfileView({Key? key}) : super(key: key);

  @override
  _ProfileViewState createState() => _ProfileViewState();
}

class _ProfileViewState extends State<ProfileView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(top: 8),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              InkWell(
                onTap: () {
                  Get.back();
                },
                child: Padding(
                  padding: const EdgeInsets.only(left: 20, right: 20),
                  child: Row(
                    children: [
                      SvgPicture.asset(SvgImages.backIc),
                      const SizedBox(width: 12),
                      Expanded(
                          child: Text("Profile",
                              style: textStyles.semiBold(
                                  fontSize: 20.0, fontColor: blackColor))),
                      InkWell(
                          onTap: () {
                            Get.to(() => const EditProfileView());
                          },
                          child: SvgPicture.asset(SvgImages.editIc))
                    ],
                  ),
                ),
              ),
              const SizedBox(height: 8),
              Container(height: 2, color: deviderColor),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20, top: 32),
                child: Obx(
                  () => Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                          width: 90,
                          height: 90,
                          //padding: const EdgeInsets.all(8),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(16),
                              color: themeColor),
                          child: Widgets.commonNetworkImage(
                              networkImage: uc.profileImage.value)),
                      const SizedBox(height: 24),
                      Text("Full Name",
                          style: textStyles.normal(
                              fontSize: 12.0, fontColor: lightGrayColor)),
                      const SizedBox(height: 8),
                      Text(uc.firstName.value + " " + uc.lastName.value,
                          style: textStyles.normal(
                              fontSize: 18.0, fontColor: blackColor)),
                      const SizedBox(height: 32),
                      Text("E-mail Address",
                          style: textStyles.normal(
                              fontSize: 12.0, fontColor: lightGrayColor)),
                      const SizedBox(height: 8),
                      Text(uc.email.value,
                          style: textStyles.normal(
                              fontSize: 18.0, fontColor: blackColor)),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
