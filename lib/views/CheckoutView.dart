import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pass_the_dietitian_exam/utility/Impl/global.dart';
import 'package:pass_the_dietitian_exam/views/DashboardView.dart';
import '../utility/ui/color_pallete.dart';
import 'package:webview_flutter/webview_flutter.dart';

class Checkout extends StatefulWidget {
  const Checkout({Key? key}) : super(key: key);

  @override
  _CheckoutState createState() => _CheckoutState();
}

class _CheckoutState extends State<Checkout> {
  late final WebViewController _controller;
  var paymentUrl = "";

  @override
  void initState() {
    super.initState();
    if (Get.arguments != null) {
      paymentUrl = Get.arguments;
    } else {}

    final WebViewController controller =
    WebViewController.fromPlatformCreationParams(const PlatformWebViewControllerCreationParams());
    controller
      ..setJavaScriptMode(JavaScriptMode.unrestricted)
      ..setBackgroundColor(const Color(0x00000000))
      ..setNavigationDelegate(
        NavigationDelegate(
          onProgress: (int progress) {
            debugPrint('WebView is loading (progress : $progress%)');
          },
          onPageStarted: (String url) {
            debugPrint('Page started loading: $url');
          },
          onPageFinished: (String url) {
            debugPrint('Page finished loading: $url');
            if (url.contains("cancel")) {
              Get.back();
            }
            if (url.contains("success")) {
              uc.isFirstTime = true;
              uc.isSubscribe.value = 1;
              Get.offAll(() => const Dashboard());
            }
          },
          onNavigationRequest: (NavigationRequest request) {
            return NavigationDecision.navigate;
          },
          onUrlChange: (UrlChange change) {
            debugPrint('url change to ${change.url}');
          },
        ),
      )
      ..loadRequest(Uri.parse(paymentUrl));
    _controller = controller;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: whiteColor,
      body: SafeArea(
        child: WebViewWidget(
          controller: _controller,
        ),
      ),
    );
  }
}
