import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:pass_the_dietitian_exam/utility/Impl/global.dart';
import 'package:pass_the_dietitian_exam/utility/endpoint/endpoint.dart';
import 'package:pass_the_dietitian_exam/utility/ui/color_pallete.dart';
import 'package:pass_the_dietitian_exam/utility/ui/customs/custom_button.dart';
import 'package:pass_the_dietitian_exam/utility/ui/customs/custom_keyboard_avoider.dart';
import 'package:pass_the_dietitian_exam/utility/ui/customs/custom_textfields.dart';
import 'package:pass_the_dietitian_exam/utility/ui/images.dart';
import 'package:pass_the_dietitian_exam/utility/ui/styles.dart';
import 'package:pass_the_dietitian_exam/utility/ui/widgets.dart';

class ChangePassword extends StatefulWidget {
  const ChangePassword({Key? key}) : super(key: key);

  @override
  _ChangePasswordState createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {
  TextEditingController txtOldPassword = TextEditingController();
  TextEditingController txtNewPassword = TextEditingController();
  TextEditingController txtConfirmPassword = TextEditingController();
  final GlobalKey<CustomTextFieldState> oldPasswordState =
      GlobalKey<CustomTextFieldState>();
  final GlobalKey<CustomTextFieldState> newPasswordState =
      GlobalKey<CustomTextFieldState>();
  final GlobalKey<CustomTextFieldState> confirmPasswordState =
      GlobalKey<CustomTextFieldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(left: 20, right: 20, top: 8),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              InkWell(
                onTap: () {
                  Get.back();
                },
                child: Row(
                  children: [
                    SvgPicture.asset(SvgImages.backIc),
                    const SizedBox(width: 12),
                    Text("Change Password",
                        style: textStyles.semiBold(
                            fontSize: 20.0, fontColor: blackColor)),
                  ],
                ),
              ),
              const SizedBox(height: 32),
              Expanded(
                child: KeyboardAvoider(
                  autoScroll: true,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Old Password",
                          style: textStyles.normal(
                              fontSize: 14.0, fontColor: blackColor)),
                      const SizedBox(height: 8),
                      CustomTextField(
                        maxLength: 16,
                        key: oldPasswordState,
                        controller: txtOldPassword,
                        validateTypes: ValidateTypes.password,
                        errorMsg: "password",
                        textInputType: TextInputType.visiblePassword,
                        textInputAction: TextInputAction.next,
                        hintText: "Enter password",
                        obscureText: false,
                      ),
                      const SizedBox(height: 16),
                      Text("New Password",
                          style: textStyles.normal(
                              fontSize: 14.0, fontColor: blackColor)),
                      const SizedBox(height: 8),
                      CustomTextField(
                        maxLength: 16,
                        key: newPasswordState,
                        controller: txtNewPassword,
                        validateTypes: ValidateTypes.password,
                        errorMsg: "password",
                        textInputType: TextInputType.visiblePassword,
                        textInputAction: TextInputAction.next,
                        hintText: "Enter password",
                        obscureText: false,
                      ),
                      const SizedBox(height: 16),
                      Text("Re-enter New Password",
                          style: textStyles.normal(
                              fontSize: 14.0, fontColor: blackColor)),
                      const SizedBox(height: 8),
                      CustomTextField(
                        maxLength: 16,
                        key: confirmPasswordState,
                        controller: txtConfirmPassword,
                        validateTypes: ValidateTypes.password,
                        errorMsg: "password",
                        textInputType: TextInputType.visiblePassword,
                        textInputAction: TextInputAction.done,
                        hintText: "Enter password",
                        obscureText: false,
                      ),
                      const SizedBox(height: 16),
                    ],
                  ),
                ),
              ),
              CustomButton.normalButton(
                  title: "Save",
                  onTap: () {
                    FocusScope.of(context).requestFocus(FocusNode());
                    var isValid = true;
                    if (oldPasswordState.currentState!.checkValidation(false)) {
                      isValid = false;
                    }
                    if (newPasswordState.currentState!.checkValidation(false)) {
                      isValid = false;
                    }
                    if (confirmPasswordState.currentState!
                        .checkValidation(false)) {
                      isValid = false;
                    }
                    if (isValid) {
                      if (txtNewPassword.text != txtConfirmPassword.text) {
                        Widgets.showSnackBar(
                            title: "Error",
                            errorMessage:
                                "New password and re-enter new password should be same");
                      } else {
                        apiLoader(asyncCall: () => changePasswordApi());
                      }
                    }
                  }),
              const SizedBox(height: 16),
            ],
          ),
        ),
      ),
    );
  }

  changePasswordApi() {
    var body = {
      "oldPassword": txtOldPassword.text,
      "newPassword": txtNewPassword.text,
      "confirmPassword": txtConfirmPassword.text,
      "deviceToken": uc.deviceToken.value,
    };
    RequestManager.postRequest(
      uri: EndPoints.changePassword,
      hasBearer: true,
      body: body,
      onStatusSuccess: (responseBody) {
        hideAppLoader();
        var res = jsonDecode(responseBody);
        if (res["status"] == 1) {
          Get.back();
          Widgets.showSnackBar(title: "Success", errorMessage: res["message"]);
        } else {
          Widgets.showSnackBar(title: "Error", errorMessage: res["message"]);
        }
      },
      onFailure: (error) {
        hideAppLoader();
      },
    );
  }
}
