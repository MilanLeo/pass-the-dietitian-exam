import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:pass_the_dietitian_exam/models/CommonModel.dart';
import 'package:pass_the_dietitian_exam/utility/Impl/global.dart';
import 'package:pass_the_dietitian_exam/utility/Impl/session_impl.dart';
import 'package:pass_the_dietitian_exam/utility/endpoint/endpoint.dart';
import 'package:pass_the_dietitian_exam/utility/ui/color_pallete.dart';
import 'package:pass_the_dietitian_exam/utility/ui/customs/custom_button.dart';
import 'package:pass_the_dietitian_exam/utility/ui/images.dart';
import 'package:pass_the_dietitian_exam/utility/ui/styles.dart';
import 'package:pass_the_dietitian_exam/utility/ui/widgets.dart';
import 'package:pass_the_dietitian_exam/views/LoginView.dart';

class DeleteView extends StatefulWidget {
  const DeleteView({Key? key}) : super(key: key);

  @override
  _DeleteViewState createState() => _DeleteViewState();
}

class _DeleteViewState extends State<DeleteView> {
  RxList<DeleteAccountModel> deleteList = <DeleteAccountModel>[].obs;
  TextEditingController txtOther = TextEditingController();

  @override
  void initState() {
    deleteList.add(DeleteAccountModel(title: "Not Interested"));
    deleteList.add(DeleteAccountModel(title: "Irrelevant contents"));
    deleteList.add(DeleteAccountModel(title: "Concern about my data"));
    deleteList.add(DeleteAccountModel(title: "Created a second account"));
    deleteList.add(DeleteAccountModel(title: "Just need a break"));
    deleteList.add(DeleteAccountModel(title: "Others"));
    deleteList[0].isSelected.value = true;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: Container(
        color: whiteColor,
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.only(left: 20, right: 20, top: 8),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                InkWell(
                  onTap: () {
                    Get.back();
                  },
                  child: Row(
                    children: [
                      SvgPicture.asset(SvgImages.backIc),
                      const SizedBox(width: 12),
                      Text("Delete Account",
                          style: textStyles.semiBold(
                              fontSize: 20.0, fontColor: blackColor)),
                    ],
                  ),
                ),
                const SizedBox(height: 8),
                Text("Please give the reason for Delete the account",
                    style: textStyles.medium(
                        fontSize: 14.0, fontColor: lightGrayColor)),
                const SizedBox(height: 32),
                Obx(
                  () => Expanded(
                    child: ListView.separated(
                        padding: const EdgeInsets.only(top: 0, bottom: 16),
                        physics: const ClampingScrollPhysics(),
                        shrinkWrap: true,
                        itemBuilder: (context, index) {
                          return InkWell(
                            onTap: () {
                              deleteList.forEach((element) {
                                element.isSelected.value = false;
                              });
                              deleteList[index].isSelected.value = true;
                            },
                            child: Padding(
                              padding:
                                  const EdgeInsets.only(top: 16, bottom: 16),
                              child: Obx(
                                () => Column(
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(deleteList[index].title.toString(),
                                            style: textStyles.medium(
                                                fontSize: 14.0,
                                                fontColor: blackColor)),
                                        SvgPicture.asset(
                                            deleteList[index].isSelected.value
                                                ? SvgImages.circleFillIc
                                                : SvgImages.circleIc)
                                      ],
                                    ),
                                    deleteList[index].isSelected.value &&
                                            deleteList[index].title == "Others"
                                        ? TextField(
                                            maxLength: 200,
                                            style: textStyles.medium(
                                                fontSize: 16.0,
                                                fontColor: blackColor),
                                            decoration: InputDecoration(
                                              hintText:
                                                  "Type Your Other Reason",
                                              border: InputBorder.none,
                                              hintStyle: textStyles.normal(
                                                  fontSize: 14.0,
                                                  fontColor:
                                                      const Color(0x80303030)),
                                              counterText: "",
                                              enabledBorder: InputBorder.none,
                                              focusedBorder: InputBorder.none,
                                            ),
                                            controller: txtOther,
                                          )
                                        : const SizedBox.shrink(),
                                    deleteList[index].isSelected.value &&
                                            deleteList[index].title == "Others"
                                        ? Padding(
                                            padding:
                                                const EdgeInsets.only(top: 8),
                                            child: Container(
                                                height: 2,
                                                color: settingBgColor),
                                          )
                                        : const SizedBox.shrink()
                                  ],
                                ),
                              ),
                            ),
                          );
                        },
                        separatorBuilder: (BuildContext context, int index) =>
                            Container(
                              height: 2,
                              color: settingBgColor,
                            ),
                        itemCount: deleteList.length),
                  ),
                ),
                CustomButton.normalButton(
                    title: "Submit",
                    onTap: () {
                      Get.closeAllSnackbars();
                      FocusScope.of(context).requestFocus(FocusNode());
                      var reason;
                      deleteList.forEach((element) {
                        if (element.isSelected.value) {
                          reason = element.title;
                        }
                      });
                      if (reason == "Others" && txtOther.text.isEmpty) {
                        Widgets.showSnackBar(
                            title: "Error",
                            errorMessage: "Please enter your reason");
                      } else {
                        if (reason == "Others") {
                          reason = txtOther.text;
                          apiLoader(asyncCall: () => deleteAccountApi(reason));
                        } else {
                          apiLoader(asyncCall: () => deleteAccountApi(reason));
                        }
                      }
                    }),
                const SizedBox(height: 16),
              ],
            ),
          ),
        ),
      ),
    );
  }

  deleteAccountApi(reason) {
    var body = {
      "reason": reason,
    };
    RequestManager.postRequest(
      uri: EndPoints.deleteAccount,
      hasBearer: true,
      body: body,
      onStatusSuccess: (responseBody) {
        hideAppLoader();
        var res = jsonDecode(responseBody);
        if (res["status"] == 1) {
          Get.offAll(() => const LoginView());
          uc.isFirstTime = true;
          SessionImpl.box!.erase();
          uc.isLogin.value = false;
          uc.isSubscribe.value = 0;
          uc.userName.value = "";
          uc.email.value = "";
          uc.profileImage.value = "";
          uc.firstName.value = "";
          uc.lastName.value = "";
          Get.offAll(() => const LoginView());
          Widgets.showSnackBar(title: "Success", errorMessage: res["message"]);
        } else {
          Widgets.showSnackBar(title: "Error", errorMessage: res["message"]);
        }
      },
      onFailure: (error) {
        hideAppLoader();
      },
    );
  }
}
