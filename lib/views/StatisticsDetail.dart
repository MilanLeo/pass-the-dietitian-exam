import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:pass_the_dietitian_exam/models/StatisticsModel.dart';
import 'package:pass_the_dietitian_exam/utility/Impl/global.dart';
import 'package:pass_the_dietitian_exam/utility/endpoint/endpoint.dart';
import 'package:pass_the_dietitian_exam/utility/ui/color_pallete.dart';
import 'package:pass_the_dietitian_exam/utility/ui/images.dart';
import 'package:pass_the_dietitian_exam/utility/ui/styles.dart';
import 'package:get/get.dart';
import 'package:pass_the_dietitian_exam/utility/ui/widgets.dart';
import 'package:pass_the_dietitian_exam/views/quiz_history.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class StatisticsDetail extends StatefulWidget {
  const StatisticsDetail({Key? key}) : super(key: key);

  @override
  _StatisticsDetailState createState() => _StatisticsDetailState();
}

class _StatisticsDetailState extends State<StatisticsDetail> {
  RxBool isLoading = true.obs;
  RxList<Statistics> dataList = <Statistics>[].obs;

  late List<_ChartData> data;
  late TooltipBehavior _tooltip;
  var topicId;
  var title = "";

  @override
  void initState() {
    if (Get.arguments != null) {
      topicId = Get.arguments[1];
      title = Get.arguments[0];
    }
    data = [];
    _tooltip = TooltipBehavior(enable: false);
    apiLoader(asyncCall: () => getStatisticsDetailApi());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(top: 8),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              InkWell(
                onTap: () {
                  Get.back();
                },
                child: Padding(
                  padding: const EdgeInsets.only(left: 20, right: 20),
                  child: Row(
                    children: [
                      SvgPicture.asset(SvgImages.backIc),
                      const SizedBox(width: 12),
                      Flexible(
                          child: Text(title,
                              style: textStyles.semiBold(
                                  fontSize: 20.0, fontColor: blackColor))),
                    ],
                  ),
                ),
              ),
              const SizedBox(height: 8),
              Container(height: 2, color: deviderColor),
              Obx(
                () => Expanded(
                  child: isLoading.value
                      ? const SizedBox.shrink()
                      : dataList.isNotEmpty
                          ? Column(
                              children: [
                                const SizedBox(
                                  height: 20,
                                ),
                  SfCartesianChart(
                    primaryXAxis: const CategoryAxis(isVisible: false),
                    primaryYAxis: const NumericAxis(minimum: 0, maximum: 100, interval: 10),
                    tooltipBehavior: _tooltip,
                    series: <CartesianSeries<dynamic, dynamic>>[
                      ...<CartesianSeries<dynamic, dynamic>>[
                        ColumnSeries<_ChartData, String>(
                          dataSource: data,
                          xValueMapper: (_ChartData data, _) => data.x,
                          yValueMapper: (_ChartData data, _) => data.y,
                          pointColorMapper: (_ChartData data, _) => data.color,
                        ),
                      ],
                    ],
                  ),
                                const SizedBox(height: 20),
                                Expanded(
                                  child: ListView.builder(
                                      padding: const EdgeInsets.only(
                                          top: 0,
                                          bottom: 16,
                                          left: 20,
                                          right: 20),
                                      physics: const ClampingScrollPhysics(),
                                      shrinkWrap: true,
                                      itemBuilder: (context, index) {
                                        return listItem(index);
                                      },
                                      itemCount: dataList.length),
                                )
                              ],
                            )
                          : Widgets.dataNotFound(),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  listItem(int index) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 16),
      child: GestureDetector(
        onTap: () {
          Get.to(()=>QuizHistory(categoryId: topicId,subCategoryId: dataList[index].id,subCategoryName: dataList[index].title));
        },
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 4.0, right: 16),
              child: Container(
                width: 15,
                height: 15,
                decoration: BoxDecoration(
                    color: Color(int.parse(dataList[index]
                        .resultBarColor!
                        .toString()
                        .replaceAll("#", "0xff"))),
                    shape: BoxShape.circle),
              ),
            ),
            Flexible(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                      child: Text(dataList[index].title,
                          style: textStyles.normal(
                              fontSize: 14.0, fontColor: blackColor))),
                  Row(
                    children: [
                      Text("${dataList[index].percentage.toStringAsFixed(0)}%",
                          style: textStyles.semiBold(
                              fontColor: blackColor, fontSize: 14.0)),
                      const SizedBox(width: 5,),
                      const Icon(Icons.arrow_forward_ios_outlined,size: 12,)
                    ],
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  getStatisticsDetailApi() {
    var body = {"topicId": topicId};
    RequestManager.postRequest(
      uri: EndPoints.getStatisticsDetail,
      hasBearer: true,
      body: body,
      onStatusSuccess: (responseBody) {
        hideAppLoader();
        isLoading.value = false;
        LogicalComponents.statisticsModel =
            statisticsModelFromJson(responseBody);
        if (LogicalComponents.statisticsModel.status == 1) {
          dataList.value = LogicalComponents.statisticsModel.result!;
          dataList.forEach((element) {
            var barColor =
                element.resultBarColor.toString().replaceAll("#", "0xff");
            data.add(_ChartData(element.id.toString(), element.percentage,
                Color(int.parse(barColor))));
          });
          print(data.length);
        }
      },
      onFailure: (error) {
        hideAppLoader();
      },
    );
  }
}

class _ChartData {
  _ChartData(this.x, this.y, this.color);
  final String x;
  final double y;
  final Color color;
}
