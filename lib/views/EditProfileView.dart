import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:pass_the_dietitian_exam/utility/Impl/global.dart';
import 'package:pass_the_dietitian_exam/utility/Impl/helper.dart';
import 'package:pass_the_dietitian_exam/utility/endpoint/endpoint.dart';
import 'package:pass_the_dietitian_exam/utility/ui/color_pallete.dart';
import 'package:pass_the_dietitian_exam/utility/ui/customs/custom_button.dart';
import 'package:pass_the_dietitian_exam/utility/ui/customs/custom_keyboard_avoider.dart';
import 'package:pass_the_dietitian_exam/utility/ui/customs/custom_textfields.dart';
import 'package:pass_the_dietitian_exam/utility/ui/images.dart';
import 'package:pass_the_dietitian_exam/utility/ui/styles.dart';
import 'package:pass_the_dietitian_exam/utility/ui/widgets.dart';

class EditProfileView extends StatefulWidget {
  const EditProfileView({Key? key}) : super(key: key);

  @override
  _EditProfileViewState createState() => _EditProfileViewState();
}

class _EditProfileViewState extends State<EditProfileView> {
  TextEditingController txtFirstName = TextEditingController();
  TextEditingController txtLastName = TextEditingController();
  TextEditingController txtEmail = TextEditingController();
  final GlobalKey<CustomTextFieldState> firstNameState =
      GlobalKey<CustomTextFieldState>();
  final GlobalKey<CustomTextFieldState> lastNameState =
      GlobalKey<CustomTextFieldState>();
  final GlobalKey<CustomTextFieldState> emailState =
      GlobalKey<CustomTextFieldState>();

  @override
  void initState() {
    super.initState();
    txtFirstName.text = uc.firstName.value;
    txtLastName.text = uc.lastName.value;
    txtEmail.text = uc.email.value;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(top: 8),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              InkWell(
                onTap: () {
                  Get.back();
                },
                child: Padding(
                  padding: const EdgeInsets.only(left: 20, right: 20),
                  child: Row(
                    children: [
                      SvgPicture.asset(SvgImages.backIc),
                      const SizedBox(width: 12),
                      Flexible(
                          child: Text("Profile",
                              style: textStyles.semiBold(
                                  fontSize: 20.0, fontColor: blackColor))),
                    ],
                  ),
                ),
              ),
              const SizedBox(height: 8),
              Container(height: 2, color: deviderColor),
              Expanded(
                child: KeyboardAvoider(
                  autoScroll: true,
                  child: Padding(
                    padding:
                        const EdgeInsets.only(left: 20, right: 20, top: 32),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        InkWell(
                          onTap: () {
                            Widgets.imageUploadOptionSheet(onPressTitle1: () {
                              getImage(true);
                            }, onPressTitle2: () {
                              getImage(false);
                            });
                          },
                          child: Stack(
                            children: [
                              Container(
                                  width: 90,
                                  height: 90,
                                  // padding: const EdgeInsets.all(8),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(16),
                                      color: themeColor),
                                  child: selectedProfile.value.isAbsolute
                                      ? ClipRRect(
                                          borderRadius: const BorderRadius.all(
                                              Radius.circular(16)),
                                          child: Image.file(
                                              selectedProfile.value,
                                              fit: BoxFit.fill))
                                      : Widgets.commonNetworkImage(
                                          networkImage: uc.profileImage.value)),
                              Padding(
                                padding:
                                    const EdgeInsets.only(top: 70, left: 70),
                                child:
                                    SvgPicture.asset(SvgImages.editProfileIc),
                              )
                            ],
                          ),
                        ),
                        const SizedBox(height: 24),
                        Text("First Name",
                            style: textStyles.normal(
                                fontSize: 14.0, fontColor: blackColor)),
                        const SizedBox(height: 8),
                        CustomTextField(
                          key: firstNameState,
                          controller: txtFirstName,
                          validateTypes: ValidateTypes.name,
                          errorMsg: "first name",
                          textInputType: TextInputType.text,
                          textInputAction: TextInputAction.next,
                          hintText: "Enter first name",
                        ),
                        const SizedBox(
                          height: 24,
                        ),
                        Text("Last Name",
                            style: textStyles.normal(
                                fontSize: 14.0, fontColor: blackColor)),
                        const SizedBox(height: 8),
                        CustomTextField(
                          key: lastNameState,
                          controller: txtLastName,
                          validateTypes: ValidateTypes.name,
                          errorMsg: "last name",
                          textInputType: TextInputType.text,
                          textInputAction: TextInputAction.next,
                          hintText: "Enter last name",
                        ),
                        const SizedBox(
                          height: 24,
                        ),
                        Text("E-mail Address",
                            style: textStyles.normal(
                                fontSize: 14.0, fontColor: blackColor)),
                        const SizedBox(height: 8),
                        CustomTextField(
                          key: emailState,
                          controller: txtEmail,
                          validateTypes: ValidateTypes.email,
                          errorMsg: "E-mail",
                          textInputType: TextInputType.emailAddress,
                          textInputAction: TextInputAction.done,
                          hintText: "Enter E-mail",
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20, bottom: 16),
                child: CustomButton.normalButton(
                    title: "Save",
                    onTap: () {
                      FocusScope.of(context).requestFocus(FocusNode());
                      var isValid = true;
                      if (firstNameState.currentState!.checkValidation(false)) {
                        isValid = false;
                      }
                      if (lastNameState.currentState!.checkValidation(false)) {
                        isValid = false;
                      }
                      if (emailState.currentState!.checkValidation(false)) {
                        isValid = false;
                      }
                      if (isValid) {
                        apiLoader(asyncCall: () => updateProfileApi());
                      }
                    }),
              )
            ],
          ),
        ),
      ),
    );
  }

  // final picker = ImagesPicker();
  var selectedProfile = File("").obs;

  Future getImage(isCamera) async {
    Helper.of(context).hideKeyBoard();
    if (Get.isBottomSheetOpen!) Get.back();
    final ImagePicker picker = ImagePicker();
    final XFile? image = await picker.pickImage(
        source: isCamera ? ImageSource.camera : ImageSource.gallery,imageQuality: 70);
    if (image != null) {
      cropImage(image.path).then((value) {
        apiLoader(asyncCall: () => uploadProfileImageApi(File(value)));
        selectedProfile.value = File(value.toString());
      });
    }
  }

  Future cropImage(filePath) async {
    CroppedFile? croppedFile = await ImageCropper().cropImage(
      sourcePath: filePath,
      aspectRatio: const CropAspectRatio(ratioX: 0.5,ratioY: 0.5),
      uiSettings: [
        AndroidUiSettings(
            toolbarTitle: 'Cropper',
            toolbarColor: primaryColor,
            toolbarWidgetColor: Colors.white,
            initAspectRatio: CropAspectRatioPreset.square,
            hideBottomControls:true,
            lockAspectRatio: true),
        IOSUiSettings(
          title: 'Cropper',
          aspectRatioLockEnabled: true,
          aspectRatioPickerButtonHidden:true,
        ),
      ],
    );
    return croppedFile != null ? croppedFile.path : "";
  }

  updateProfileApi() {
    var body = {
      "firstName": txtFirstName.text,
      "lastName": txtLastName.text,
      "email": txtEmail.text,
    };
    RequestManager.postRequest(
      uri: EndPoints.updateProfile,
      hasBearer: true,
      body: body,
      onStatusSuccess: (responseBody) {
        hideAppLoader();
        var res = jsonDecode(responseBody);
        if (res["status"] == 1) {
          uc.userName.value = res["result"]["userDetails"]["firstName"] +
              res["result"]["userDetails"]["lastName"];
          uc.email.value = res["result"]["userDetails"]["email"];
          uc.profileImage.value = res["result"]["userDetails"]["profileImage"];
          uc.firstName.value = res["result"]["userDetails"]["firstName"];
          uc.lastName.value = res["result"]["userDetails"]["lastName"];
          Get.back();
        } else {
          Widgets.showSnackBar(title: "Error", errorMessage: res["message"]);
        }
      },
      onFailure: (error) {
        hideAppLoader();
      },
    );
  }

  uploadProfileImageApi(var path) {
    RequestManager.uploadImage(
      uri: EndPoints.uploadFile,
      hasBearer: true,
      file: path,
      type: "profile",
      onSuccess: (responseBody) {
        hideAppLoader();
        var res = jsonDecode(responseBody);
        if (res["status"] == 1) {
          uc.profileImage.value = res["result"]["imageUrl"];
        }
      },
      onFailure: (error) {
        hideAppLoader();
      },
    );
  }
}
