import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pass_the_dietitian_exam/utility/Impl/global.dart';
import 'package:pass_the_dietitian_exam/utility/Impl/session_impl.dart';
import 'package:pass_the_dietitian_exam/utility/endpoint/endpoint.dart';
import 'package:pass_the_dietitian_exam/utility/ui/color_pallete.dart';
import 'package:pass_the_dietitian_exam/utility/ui/styles.dart';
import 'package:pass_the_dietitian_exam/utility/ui/widgets.dart';
import 'package:pass_the_dietitian_exam/views/DashboardView.dart';
import 'package:pass_the_dietitian_exam/views/SignupView.dart';
import 'package:url_launcher/url_launcher.dart';
import '../utility/ui/customs/custom_button.dart';
import '../utility/ui/customs/custom_keyboard_avoider.dart';
import '../utility/ui/customs/custom_textfields.dart';
import 'ForgotPasswordView.dart';

class LoginView extends StatefulWidget {
  const LoginView({Key? key}) : super(key: key);

  @override
  _LoginViewState createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  RxBool passwordVisibility = false.obs;
  RxBool checkError = false.obs;

  TextEditingController txtEmail = TextEditingController();
  TextEditingController txtPassword = TextEditingController();
  final GlobalKey<CustomTextFieldState> emailState =
      GlobalKey<CustomTextFieldState>();
  final GlobalKey<CustomTextFieldState> passwordState =
      GlobalKey<CustomTextFieldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(left: 20, right: 20, top: 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("Welcome!",
                      style: textStyles.semiBold(
                          fontSize: 30.0, fontColor: blackColor)),
                  InkWell(
                      onTap: () {
                        Get.offAll(() => const Dashboard());
                      },
                      child: Text("SKIP>>",
                          style: textStyles.medium(
                              fontColor: themeColor, fontSize: 18.0)))
                ],
              ),
              Text("Hello, Log in to your account",
                  style: textStyles.medium(
                      fontSize: 14.0, fontColor: lightGrayColor)),
              const SizedBox(height: 84),
              Expanded(
                child: KeyboardAvoider(
                  autoScroll: true,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("E-mail Address",
                          style: textStyles.normal(
                              fontSize: 14.0, fontColor: blackColor)),
                      const SizedBox(height: 8),
                      CustomTextField(
                        key: emailState,
                        controller: txtEmail,
                        validateTypes: ValidateTypes.email,
                        errorMsg: "E-mail",
                        textInputType: TextInputType.emailAddress,
                        textInputAction: TextInputAction.next,
                        hintText: "Enter E-mail",
                      ),
                      const SizedBox(
                        height: 24,
                      ),
                      Text("Password",
                          style: textStyles.normal(
                              fontSize: 14.0, fontColor: blackColor)),
                      const SizedBox(
                        height: 8,
                      ),
                      Obx(
                        () => CustomTextField(
                          maxLength: 16,
                          key: passwordState,
                          controller: txtPassword,
                          validateTypes: ValidateTypes.password,
                          errorMsg: "password",
                          textInputType: TextInputType.visiblePassword,
                          textInputAction: TextInputAction.done,
                          hintText: "Enter password",
                          obscureText: passwordVisibility.value,
                          suffixIcon: IconButton(
                            onPressed: () {
                              passwordVisibility.value =
                                  !passwordVisibility.value;
                            },
                            icon: Icon(
                              passwordVisibility.value
                                  ? Icons.visibility_outlined
                                  : Icons.visibility_off_outlined,
                              color: eyeColor,
                            ),
                          ),
                        ),
                      ),
                      Container(
                          alignment: Alignment.centerRight,
                          child: TextButton(
                            onPressed: () {
                              Get.to(() => const ForgotPasswordView());
                            },
                            child: Text(
                              "Forgot Password?",
                              style: textStyles.normal(
                                  fontSize: 12.0, fontColor: blackColor),
                            ),
                          )),
                      const SizedBox(
                        height: 16,
                      ),
                      CustomButton.normalButton(
                          title: "LOG IN",
                          onTap: () {
                            FocusScope.of(context).requestFocus(FocusNode());
                            var isValid = true;
                            if (emailState.currentState!
                                .checkValidation(false)) {
                              isValid = false;
                            }
                            if (passwordState.currentState!
                                .checkValidation(false)) {
                              isValid = false;
                            }
                            if (isValid) {
                              apiLoader(asyncCall: () => loginApi());
                            }
                          }),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 16),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "Don’t have an account?",
                          style: textStyles.normal(
                              fontSize: 16.0, fontColor: blackColor),
                        ),
                        InkWell(
                          onTap: () {
                            Get.to(() => const SignupView());
                          },
                          child: Text(
                            "  Signup",
                            style: textStyles.semiBold(
                                fontSize: 16.0, fontColor: themeColor),
                          ),
                        ),
                      ],
                    ),
                    Container(
                        alignment: Alignment.center,
                        child: TextButton(
                          onPressed: () {
                            _launchUrl();
                          },
                          child: Text(
                            "Privacy & Terms",
                            style: textStyles.medium(
                                fontSize: 14.0, fontColor: darkBlue),
                          ),
                        )),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _launchUrl() async {
    if (!await launchUrl(
        Uri.parse("http://admin.passthedietitianexamapp.com/privacy-policy"))) {
      throw 'Could not launch';
    }
  }

  loginApi() {
    var body = {
      "email": txtEmail.text,
      "password": txtPassword.text,
      "deviceToken": uc.deviceToken.value,
    };
    RequestManager.postRequest(
      uri: EndPoints.login,
      hasBearer: false,
      body: body,
      onStatusSuccess: (responseBody) {
        hideAppLoader();
        var res = jsonDecode(responseBody);
        if (res["status"] == 1) {
          SessionImpl.setData(SessionKeys.keyLogin, true);
          SessionImpl.setData(
              SessionKeys.keyBearerToken, res["result"]["accessToken"]);
          uc.isLogin.value = true;
          uc.isFirstTime = true;
          uc.isSubscribe.value = res["result"]["userDetail"]["isSubscribe"];
          uc.userName.value = res["result"]["userDetail"]["firstName"] +
              res["result"]["userDetail"]["lastName"];
          uc.email.value = res["result"]["userDetail"]["email"];
          uc.profileImage.value = res["result"]["userDetail"]["profileImage"];
          uc.firstName.value = res["result"]["userDetail"]["firstName"];
          uc.lastName.value = res["result"]["userDetail"]["lastName"];
          Get.offAll(() => const Dashboard());
        } else {
          Widgets.showSnackBar(title: "Error", errorMessage: res["message"]);
        }
      },
      onFailure: (error) {
        hideAppLoader();
      },
    );
  }
}
