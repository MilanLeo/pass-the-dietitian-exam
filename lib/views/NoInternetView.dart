import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:pass_the_dietitian_exam/utility/Impl/utilities_impl.dart';
import 'package:pass_the_dietitian_exam/utility/ui/color_pallete.dart';
import 'package:pass_the_dietitian_exam/utility/ui/images.dart';
import 'package:pass_the_dietitian_exam/utility/ui/styles.dart';
import 'package:pass_the_dietitian_exam/utility/ui/widgets.dart';
import '../utility/ui/customs/custom_button.dart';

class NoInterNetView extends StatefulWidget {
  const NoInterNetView({Key? key}) : super(key: key);

  @override
  _NoInterNetViewState createState() => _NoInterNetViewState();
}

class _NoInterNetViewState extends State<NoInterNetView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SvgPicture.asset(SvgImages.noInternetIc),
            const SizedBox(height: 48),
            Text("No internet connection", style: textStyles.bold(fontSize: 20.0, fontColor: themeColor)),
            const SizedBox(height: 8),
            Padding(
              padding: const EdgeInsets.only(left: 40, right: 40),
              child: Text("Please check your internet connection and try again", style: textStyles.normal(fontSize: 14.0, fontColor: themeColor), textAlign: TextAlign.center,),
            ),
            Padding(padding: const EdgeInsets.only(left: 20,right: 20,top: 32),
              child: CustomButton.normalButton(
                  title: "Try Again",
                  onTap: () {
                    apiLoader(asyncCall: () async {
                      bool isConnected = await Utilities.isConnectedNetwork();
                      if (isConnected) {
                        hideAppLoader();
                        Get.back();
                      }else{
                        hideAppLoader();
                      }
                    });
                  }),
            )
          ],
        ),
      ),
    );
  }
}
