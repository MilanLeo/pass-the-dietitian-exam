import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:pass_the_dietitian_exam/utility/Impl/global.dart';
import 'package:pass_the_dietitian_exam/utility/ui/color_pallete.dart';
import 'package:pass_the_dietitian_exam/utility/ui/images.dart';
import 'package:pass_the_dietitian_exam/utility/ui/styles.dart';
import 'package:pass_the_dietitian_exam/utility/ui/widgets.dart';
import 'package:pass_the_dietitian_exam/views/HomeView.dart';
import 'package:pass_the_dietitian_exam/views/ProfileView.dart';
import 'package:pass_the_dietitian_exam/views/SettingView.dart';
import 'package:pass_the_dietitian_exam/views/StatisticsView.dart';

class Dashboard extends StatefulWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  var selectedIndex = 0.obs;
  DateTime? currentBackPressTime;

  tabItem(title, icon, index) {
    return InkWell(
      onTap: () {
        selectedIndex.value = index;
      },
      child: Obx(
        () => Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SvgPicture.asset(
              icon,
              color: selectedIndex == index ? themeColor : themeDisable,
            ),
            Text(
              title,
              style: textStyles.medium(
                  fontColor: selectedIndex == index ? themeColor : themeDisable,
                  fontSize: 14.0),
            )
          ],
        ),
      ),
    );
  }

  loadBottomTab() {
    return Container(
      decoration: BoxDecoration(
        color: whiteColor,
        borderRadius: const BorderRadius.only(
            topRight: Radius.circular(28), topLeft: Radius.circular(28)),
        boxShadow: const [
          BoxShadow(
              spreadRadius: -4,
              blurRadius: 10,
              color: Colors.black12,
              offset: Offset(0.0, -8.0)),
        ],
      ),
      child: SafeArea(
        bottom: true,
        top: false,
        child: Padding(
          padding: const EdgeInsets.only(left: 20, right: 20, top: 16),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              tabItem("Home", SvgImages.homeIc, 0),
              tabItem("Statistics", SvgImages.statasticIc, 1),
              tabItem("Settings", SvgImages.settingIc, 2),
            ],
          ),
        ),
      ),
    );
  }

  Future<bool> onWillPop() {
    DateTime now = DateTime.now();
    if (currentBackPressTime == null ||
        now.difference(currentBackPressTime!) > Duration(seconds: 2)) {
      currentBackPressTime = now;
      Fluttertoast.showToast(
          msg: "Tap again to exit",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.black,
          textColor: Colors.white,
          fontSize: 14.0);
      return Future.value(false);
    }
    return Future.value(true);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: WillPopScope(
        onWillPop: onWillPop,
        child: Column(
          children: [
            SafeArea(
              bottom: false,
              child: Padding(
                padding: const EdgeInsets.only(left: 20, right: 20, top: 8),
                child: InkWell(
                  onTap: () {
                    if (uc.isLogin.value) {
                      Get.to(() => const ProfileView());
                    }
                  },
                  child: Obx(
                    () => Row(
                      children: [
                        Widgets.commonNetworkImage(
                            networkImage: uc.profileImage.value,
                            height: 55,
                            width: 55),
                        const SizedBox(width: 16),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Hi,",
                                  style: textStyles.normal(
                                      fontColor: themeColor, fontSize: 14.0)),
                              Text(
                                  uc.isLogin.value
                                      ? (uc.firstName.value +
                                          " " +
                                          uc.lastName.value)
                                      : uc.userName.value,
                                  overflow: TextOverflow.ellipsis,
                                  style: textStyles.semiBold(
                                      fontColor: themeColor, fontSize: 18.0)),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
            const SizedBox(height: 8),
            Obx(
              () => selectedIndex.value == 0
                  ? const HomeView()
                  : selectedIndex.value == 1
                      ? const StatisticsView()
                      : const SettingView(),
            ),
            loadBottomTab()
          ],
        ),
      ),
    );
  }
}
