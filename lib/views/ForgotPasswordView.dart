import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:get/get_core/get_core.dart';
import 'package:pass_the_dietitian_exam/utility/Impl/global.dart';
import 'package:pass_the_dietitian_exam/utility/endpoint/endpoint.dart';
import 'package:pass_the_dietitian_exam/utility/ui/customs/custom_textfields.dart';
import 'package:pass_the_dietitian_exam/utility/ui/images.dart';
import 'package:pass_the_dietitian_exam/utility/ui/styles.dart';
import 'package:pass_the_dietitian_exam/utility/ui/widgets.dart';
import '../utility/ui/color_pallete.dart';
import '../utility/ui/customs/custom_button.dart';
import '../utility/ui/customs/custom_keyboard_avoider.dart';

class ForgotPasswordView extends StatefulWidget {
  const ForgotPasswordView({Key? key}) : super(key: key);

  @override
  _ForgotPasswordViewState createState() => _ForgotPasswordViewState();
}

class _ForgotPasswordViewState extends State<ForgotPasswordView> {
  TextEditingController txtEmail = TextEditingController();
  final GlobalKey<CustomTextFieldState> emailState =
      GlobalKey<CustomTextFieldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(left: 20, right: 20, top: 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              InkWell(
                onTap: () {
                  Get.back();
                },
                child: Row(
                  children: [
                    SvgPicture.asset(SvgImages.backIc),
                    const SizedBox(width: 12),
                    Text("Forgot Password",
                        style: textStyles.semiBold(
                            fontSize: 20.0, fontColor: blackColor)),
                  ],
                ),
              ),
              const SizedBox(height: 8),
              Text("Please enter your email to request the new password",
                  style: textStyles.medium(
                      fontSize: 14.0, fontColor: lightGrayColor)),
              const SizedBox(height: 32),
              Expanded(
                child: KeyboardAvoider(
                  autoScroll: true,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("E-mail Address",
                          style: textStyles.normal(
                              fontSize: 14.0, fontColor: blackColor)),
                      const SizedBox(height: 8),
                      CustomTextField(
                        key: emailState,
                        controller: txtEmail,
                        validateTypes: ValidateTypes.email,
                        errorMsg: "E-mail",
                        textInputType: TextInputType.emailAddress,
                        textInputAction: TextInputAction.done,
                        hintText: "Enter E-mail",
                      ),
                      const SizedBox(height: 16),
                    ],
                  ),
                ),
              ),
              CustomButton.normalButton(
                  title: "SUBMIT",
                  onTap: () {
                    FocusScope.of(context).requestFocus(FocusNode());
                    var isValid = true;
                    if (emailState.currentState!.checkValidation(false)) {
                      isValid = false;
                    }
                    if (isValid) {
                      apiLoader(asyncCall: () => forgotPasswordApi());
                    }
                  }),
              const SizedBox(height: 16),
            ],
          ),
        ),
      ),
    );
  }

  forgotPasswordApi() {
    var body = {
      "email": txtEmail.text,
    };
    RequestManager.postRequest(
      uri: EndPoints.forgotPassword,
      hasBearer: false,
      body: body,
      onStatusSuccess: (responseBody) {
        hideAppLoader();
        var res = jsonDecode(responseBody);
        if (res["status"] == 1) {
          Get.back();
          Widgets.showSnackBar(title: "Success", errorMessage: res["message"]);
        } else {
          Widgets.showSnackBar(title: "Error", errorMessage: res["message"]);
        }
      },
      onFailure: (error) {
        hideAppLoader();
      },
    );
  }
}
