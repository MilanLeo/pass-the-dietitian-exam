import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:pass_the_dietitian_exam/models/ResultModel.dart';
import 'package:pass_the_dietitian_exam/utility/Impl/global.dart';
import 'package:pass_the_dietitian_exam/utility/endpoint/endpoint.dart';
import 'package:pass_the_dietitian_exam/utility/ui/color_pallete.dart';
import 'package:pass_the_dietitian_exam/utility/ui/images.dart';
import 'package:pass_the_dietitian_exam/utility/ui/styles.dart';
import 'package:pass_the_dietitian_exam/utility/ui/widgets.dart';
import 'package:percent_indicator/percent_indicator.dart';

class QuizHistoryDetails extends StatefulWidget {
  var uniqueCheck;
   QuizHistoryDetails(this.uniqueCheck);

  @override
  State<QuizHistoryDetails> createState() => _QuizeHistoryDetailsState();
}

class _QuizeHistoryDetailsState extends State<QuizHistoryDetails> {
  RxBool isLoading = true.obs;
  RxList<ResultElement> resultList = <ResultElement>[].obs;
  var score = 0.0.obs;
  var rightAnswer = 0.obs;
  var totalQuestion = 0.obs;
  var title = "";
  var isDomain;
  var domainName;

  @override
  void initState() {
    getResultApi();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(top: 8),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              InkWell(
                onTap: () {
                 Get.back();
                },
                child: Padding(
                  padding: const EdgeInsets.only(left: 20, right: 20),
                  child: Row(
                    children: [
                      SvgPicture.asset(SvgImages.backIc),
                      const SizedBox(width: 12),
                      Flexible(
                          child: Text("Result",
                              style: textStyles.semiBold(
                                  fontSize: 20.0, fontColor: blackColor))),
                    ],
                  ),
                ),
              ),
              const SizedBox(height: 8),
              Container(height: 2, color: deviderColor),
              Obx(() => isLoading.value
                  ? Container()
                  : Padding(
                padding: const EdgeInsets.only(
                    left: 20, right: 20, top: 20, bottom: 20),
                child: Container(
                    padding: const EdgeInsets.all(16),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(16),
                        color: pinkColor),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: Column(
                            crossAxisAlignment:
                            CrossAxisAlignment.start,
                            children: [
                              Text(title,
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                  style: textStyles.semiBold(
                                      fontSize: 20.0,
                                      fontColor: blackColor)),
                              Text(
                                  "${rightAnswer.value} of ${totalQuestion.value} Questions",
                                  style: textStyles.medium(
                                      fontSize: 14.0,
                                      fontColor: blackColor)),
                            ],
                          ),
                        ),
                        CircularPercentIndicator(
                          backgroundColor: pinkColor,
                          radius: 100.0,
                          lineWidth: 16.0,
                          percent: score.value / 100,
                          center: Text(
                              "${score.value.toStringAsFixed(0)}%",
                              style: textStyles.semiBold(
                                  fontColor: blackColor,
                                  fontSize: 22.0)),
                          progressColor: blueColor,
                        )
                      ],
                    )),
              )),
              Obx(
                    () => Expanded(
                  child: isLoading.value
                      ? Container()
                      : resultList.isEmpty
                      ? Widgets.dataNotFound()
                      : ListView.builder(
                      padding: const EdgeInsets.only(
                          top: 0, left: 20, right: 20),
                      physics: const ClampingScrollPhysics(),
                      shrinkWrap: true,
                      itemBuilder: (context, index) {
                        return resultItem(index);
                      },
                      itemCount: resultList.length),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  resultItem(index) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("QUESTION ${index + 1}",
            style: textStyles.semiBold(fontSize: 12.0, fontColor: greyColor)),
        const SizedBox(height: 8),
        Text(resultList[index].questions,
            style: textStyles.medium(fontSize: 16.0, fontColor: blackColor)),
        const SizedBox(height: 16),
        ListView.builder(
            padding: const EdgeInsets.only(top: 0, bottom: 16),
            physics: const ClampingScrollPhysics(),
            shrinkWrap: true,
            itemBuilder: (context, subIndex) {
              return Padding(
                padding: const EdgeInsets.only(top: 8, bottom: 8),
                child: Container(
                  padding: const EdgeInsets.all(16),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(12),
                      color: resultList[index]
                          .answerOptions![subIndex]
                          .isCorrect ==
                          1
                          ? themeColor
                          : resultList[index].answerId ==
                          resultList[index].answerOptions![subIndex].id
                          ? redColor
                          : whiteColor,
                      border: Border.all(
                          color: borderColor,
                          width: resultList[index]
                              .answerOptions![subIndex]
                              .isCorrect ==
                              1
                              ? 0.0
                              : resultList[index].answerId ==
                              resultList[index]
                                  .answerOptions![subIndex]
                                  .id
                              ? 0.0
                              : 1.5)),
                  child: Row(
                    children: [
                      Text(
                          resultList[index].answerOptions![subIndex].position +
                              ")",
                          style: textStyles.semiBold(
                              fontColor: resultList[index]
                                  .answerOptions![subIndex]
                                  .isCorrect ==
                                  1
                                  ? whiteColor
                                  : resultList[index].answerId ==
                                  resultList[index]
                                      .answerOptions![subIndex]
                                      .id
                                  ? whiteColor
                                  : blackColor,
                              fontSize: 16.0)),
                      const SizedBox(width: 6),
                      SvgPicture.asset(resultList[index]
                          .answerOptions![subIndex]
                          .isCorrect ==
                          1
                          ? SvgImages.rightIc
                          : resultList[index].answerId ==
                          resultList[index].answerOptions![subIndex].id
                          ? SvgImages.wrongIc
                          : SvgImages.circleIc),
                      const SizedBox(width: 6),
                      Flexible(
                          child: Text(
                              resultList[index].answerOptions![subIndex].answer,
                              style: textStyles.medium(
                                  fontColor: resultList[index]
                                      .answerOptions![subIndex]
                                      .isCorrect ==
                                      1
                                      ? whiteColor
                                      : resultList[index].answerId ==
                                      resultList[index]
                                          .answerOptions![subIndex]
                                          .id
                                      ? whiteColor
                                      : blackColor,
                                  fontSize: 14.0)))
                    ],
                  ),
                ),
              );
            },
            itemCount: resultList[index].answerOptions!.length),
        Center(
          child: InkWell(
            onTap: () {
              retionalPopup(resultList[index].answerDescription);
            },
            child: Container(
              padding: const EdgeInsets.all(12),
              width: 55,
              height: 55,
              decoration:
              const BoxDecoration(shape: BoxShape.circle, color: pinkColor),
              child: SvgPicture.asset(SvgImages.retionalIc),
            ),
          ),
        ),
        Center(
          child: Text("Rationale",
              style: textStyles.medium(fontSize: 12.0, fontColor: blackColor)),
        ),
        const SizedBox(height: 16),
      ],
    );
  }

  retionalPopup(answerDescription) {
    return Get.bottomSheet(
        BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 3, sigmaY: 3),
          child: Container(
              constraints: BoxConstraints(
                maxHeight: Get.height - 200,
              ),
              decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(24),
                      topRight: Radius.circular(24)),
                  color: whiteColor),
              child: Padding(
                padding: const EdgeInsets.only(
                    left: 20.0, right: 20.0, top: 20.0, bottom: 16.0),
                child: SafeArea(
                  bottom: true,
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Container(
                          height: 5,
                          width: 60,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              color: themeDisable),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 32, bottom: 16),
                          child: SvgPicture.asset(
                            SvgImages.retionalIc,
                            height: 50,
                            width: 50,
                          ),
                        ),
                        Text(
                          "Rationale",
                          textAlign: TextAlign.center,
                          style: textStyles.semiBold(
                              fontSize: 20.0, fontColor: blackColor),
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                        Text(
                          answerDescription,
                          textAlign: TextAlign.center,
                          style: textStyles.medium(
                              fontSize: 14.0, fontColor: blackColor),
                        ),
                      ],
                    ),
                  ),
                ),
              )),
        ),
        isScrollControlled: true);
  }


  getResultApi() {
    print(widget.uniqueCheck);
    print("uniqueCheck");
   apiLoader(asyncCall: ()=> RequestManager.postRequest(
      uri: EndPoints.getAllSubQuestionAnswerHistory,
      hasBearer: true,
      body: {
        "uniqueCheck": widget.uniqueCheck
      },
      onStatusSuccess: (responseBody) {
        hideAppLoader();
        isLoading.value = false;
print(responseBody);
        LogicalComponents.resultModel = resultModelFromJson(responseBody);
        if (LogicalComponents.resultModel.status == 1) {
          score.value = LogicalComponents.resultModel.result!.score;
          rightAnswer.value = LogicalComponents.resultModel.result!.rightAnswer;
          totalQuestion.value =
              LogicalComponents.resultModel.result!.totalQuewstion;
          resultList.value = LogicalComponents.resultModel.result!.result!;
        }
      },
      onFailure: (error) {
        hideAppLoader();
      },
    ));
  }
}
