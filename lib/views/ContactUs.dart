import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:pass_the_dietitian_exam/utility/Impl/global.dart';
import 'package:pass_the_dietitian_exam/utility/endpoint/endpoint.dart';
import 'package:pass_the_dietitian_exam/utility/ui/color_pallete.dart';
import 'package:pass_the_dietitian_exam/utility/ui/customs/custom_button.dart';
import 'package:pass_the_dietitian_exam/utility/ui/customs/custom_textfields.dart';
import 'package:pass_the_dietitian_exam/utility/ui/images.dart';
import 'package:pass_the_dietitian_exam/utility/ui/styles.dart';
import 'package:pass_the_dietitian_exam/utility/ui/widgets.dart';


class ContactUs extends StatefulWidget {
  const ContactUs({Key? key}) : super(key: key);

  @override
  State<ContactUs> createState() => _ContactUsState();
}

class _ContactUsState extends State<ContactUs> {
  RxBool checkError = false.obs;
  TextEditingController txtMessage = TextEditingController();
  final GlobalKey<CustomTextFieldState> messageState =
  GlobalKey<CustomTextFieldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(top: 16,left: 20,right: 20,bottom: 20),
          child: Column(
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    InkWell(
                      onTap: () {
                        Get.back();
                      },
                      child: Row(
                        children: [
                          SvgPicture.asset(SvgImages.backIc),
                          const SizedBox(width: 12),
                          Flexible(
                              child: Text("Contact Us",
                                  style: textStyles.semiBold(
                                      fontSize: 20.0, fontColor: blackColor))),
                        ],
                      ),
                    ),
                    const SizedBox(height: 20),
                    Text("Message",
                        style: textStyles.normal(
                            fontSize: 14.0, fontColor: blackColor)),
                    const SizedBox(height: 8),
                    CustomTextField(
                      maxLines: 4,
                      key: messageState,
                      controller: txtMessage,
                      validateTypes: ValidateTypes.name,
                      errorMsg: "Message",
                      textInputType: TextInputType.multiline,
                      textInputAction: TextInputAction.done,
                      hintText: "Enter Message",
                    ),
                  ],
                ),
              ),
              CustomButton.normalButton(
                  title: "Send",
                  onTap: () {
                    FocusScope.of(context).requestFocus(FocusNode());
                    var isValid = true;
                    if (messageState.currentState!
                        .checkValidation(false)) {
                      isValid = false;
                    }
                    if (isValid) {
                      apiLoader(asyncCall: () => contactUsApi());
                    }
                  }),
            ],
          ),
        ),
      ),
    );
  }

  contactUsApi() {
    var body = {
      "content": txtMessage.text,
    };
    RequestManager.postRequest(
      uri: EndPoints.contactUs,
      hasBearer: true,
      body: body,
      onStatusSuccess: (responseBody) {
        hideAppLoader();
        var res = jsonDecode(responseBody);
        if (res["status"] == 1) {
          RequestManager.getSnackToast(
              title: "Success",
              messageText: Text(res["message"],
                  style: const TextStyle(
                    fontSize: dimen.textMedium,
                    color: Colors.white)),colorText: whiteColor,backgroundColor: themeColor,
              onDismissed: () {
                Future.delayed(const Duration(microseconds: 200), () {
                  Get.back();
                });
              });
        } else {
          Widgets.showSnackBar(title: "Error", errorMessage: res["message"]);
        }
      },
      onFailure: (error) {
        hideAppLoader();
      },
    );
  }

}
