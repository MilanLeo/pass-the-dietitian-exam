import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:pass_the_dietitian_exam/models/SubTopicModel.dart';
import 'package:pass_the_dietitian_exam/utility/Impl/global.dart';
import 'package:pass_the_dietitian_exam/utility/endpoint/endpoint.dart';
import 'package:pass_the_dietitian_exam/utility/ui/color_pallete.dart';
import 'package:pass_the_dietitian_exam/utility/ui/images.dart';
import 'package:pass_the_dietitian_exam/utility/ui/styles.dart';
import 'package:pass_the_dietitian_exam/utility/ui/widgets.dart';
import 'package:pass_the_dietitian_exam/views/LoginView.dart';
import 'package:pass_the_dietitian_exam/views/MockRulesView.dart';
import 'package:pass_the_dietitian_exam/views/QuestionsView.dart';

class SubtopicsView extends StatefulWidget {
  const SubtopicsView({Key? key}) : super(key: key);

  @override
  _SubtopicsViewState createState() => _SubtopicsViewState();
}

class _SubtopicsViewState extends State<SubtopicsView> {
  RxBool isLoading = true.obs;
  RxList<SubTopicList> subTopicList = <SubTopicList>[].obs;
  var title = "";
  var topicId;
  var isDomain;

  @override
  void initState() {
    super.initState();
    if (Get.arguments != null) {
      title = Get.arguments[1];
      topicId = Get.arguments[0];
      isDomain = Get.arguments[2];
    }
    apiLoader(asyncCall: () => getSubTopics());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(top: 8),
          child: Column(
            children: [
              InkWell(
                onTap: () {
                  Get.back();
                },
                child: Padding(
                  padding: const EdgeInsets.only(left: 20, right: 20),
                  child: Row(
                    children: [
                      SvgPicture.asset(SvgImages.backIc),
                      const SizedBox(width: 12),
                      Flexible(
                          child: Text(title,
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: textStyles.semiBold(
                                  fontSize: 20.0, fontColor: blackColor))),
                    ],
                  ),
                ),
              ),
              const SizedBox(height: 6),
              Container(height: 2, color: deviderColor),
              Obx(
                () => Expanded(
                  child: isLoading.value
                      ? Container()
                      : subTopicList.isEmpty
                          ? Widgets.dataNotFound()
                          : ListView.builder(
                              padding: const EdgeInsets.only(
                                  top: 8, bottom: 16, left: 20, right: 20),
                              physics: const ClampingScrollPhysics(),
                              shrinkWrap: true,
                              itemBuilder: (context, index) {
                                return listItem(index);
                              },
                              itemCount: subTopicList.length),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  listItem(int index) {
    return Container(
      padding: const EdgeInsets.only(top: 8, bottom: 8),
      child: Container(
          padding:
              const EdgeInsets.only(left: 16, right: 16, top: 16, bottom: 16),
          decoration: BoxDecoration(
              color: pinkColor, borderRadius: BorderRadius.circular(10)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Flexible(
                      child: Text(subTopicList[index].title,
                          style: textStyles.medium(
                              fontSize: 12.0, fontColor: blackColor)),
                    ),
                    const SizedBox(height: 4),
                    Row(
                      children: [
                        Text(
                            "Questions : ${subTopicList[index].totalQuestionCount}",
                            style: textStyles.semiBold(
                                fontSize: 14.0, fontColor: themeColor)),
                        const SizedBox(width: 8),
                        subTopicList[index].isFree == 1
                            ? Padding(
                                padding: const EdgeInsets.only(right: 16),
                                child: Container(
                                  padding: const EdgeInsets.only(
                                      left: 10, right: 10, top: 2, bottom: 2),
                                  decoration: const BoxDecoration(
                                      borderRadius: const BorderRadius.all(
                                          Radius.circular(16)),
                                      color: Colors.green),
                                  child: Text("Free",
                                      style: textStyles.semiBold(
                                          fontColor: whiteColor,
                                          fontSize: 10.0)),
                                ),
                              )
                            : const SizedBox.shrink()
                      ],
                    ),
                  ],
                ),
              ),
              InkWell(
                onTap: () {
                  print(subTopicList[index].isExam);
                  if (subTopicList[index].isExam == 0) {
                    if (uc.isLogin.value) {
                      if (subTopicList[index].isFree == 1 ||
                          uc.isSubscribe.value == 1) {
                        Get.to(() => const QuestionsView(), arguments: [
                          topicId,
                          subTopicList[index].title,
                          subTopicList[index].time,
                          subTopicList[index].isExam,
                          subTopicList[index].id,
                          isDomain,
                          title
                        ])!.then((value) {
                          getSubTopics();
                        });
                      } else {
                        Widgets.showAlertDialog(context);
                      }
                    } else {
                      Widgets.showSnackBar(
                          title: "Error",
                          errorMessage: "Please login to access free version.");
                      Get.offAll(() => const LoginView());
                    }
                  } else {
                    Get.to(() => const MockRulesView(), arguments: [
                      topicId,
                      subTopicList[index].title,
                      subTopicList[index].mockRule,
                      subTopicList[index].time,
                      subTopicList[index].isExam,
                      subTopicList[index].id,
                      subTopicList[index].isFree,
                      isDomain
                    ])!.then((value){
                       getSubTopics();
                    });
                  }
                },
                child: Container(
                  padding: const EdgeInsets.only(
                      left: 16, right: 16, top: 4, bottom: 4),
                  decoration: BoxDecoration(
                      color: themeColor,
                      borderRadius: BorderRadius.circular(10)),
                  child: Text(subTopicList[index].isCompletedAnswer ==0?"Resume":"Start",
                      style: textStyles.medium(
                          fontSize: 10.0, fontColor: whiteColor)),
                ),
              )
            ],
          )),
    );
  }

  getSubTopics() {
    var body = {"subTopicId": topicId,"deviceToken":uc.deviceToken.value};
    print(body);
    RequestManager.postRequest(
      uri: EndPoints.getSubTopics,
      hasBearer: true,
      body: body,
      retry: () {
        apiLoader(asyncCall: () => getSubTopics());
      },
      onStatusSuccess: (responseBody) {
        hideAppLoader();
        print(responseBody);
        isLoading.value = false;
        LogicalComponents.subTopicModel = subTopicModelFromJson(responseBody);
        if (LogicalComponents.subTopicModel.status == 1) {
          subTopicList.value =
              LogicalComponents.subTopicModel.result!.subTopicList!;
        }
      },
      onFailure: (error) {
        hideAppLoader();
      },
    );
  }
}
