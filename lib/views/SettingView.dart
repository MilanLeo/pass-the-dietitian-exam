import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:pass_the_dietitian_exam/utility/Impl/global.dart';
import 'package:pass_the_dietitian_exam/utility/Impl/helper.dart';
import 'package:pass_the_dietitian_exam/utility/Impl/session_impl.dart';
import 'package:pass_the_dietitian_exam/utility/endpoint/endpoint.dart';
import 'package:pass_the_dietitian_exam/utility/ui/color_pallete.dart';
import 'package:pass_the_dietitian_exam/utility/ui/customs/custom_button.dart';
import 'package:pass_the_dietitian_exam/utility/ui/images.dart';
import 'package:pass_the_dietitian_exam/utility/ui/styles.dart';
import 'package:pass_the_dietitian_exam/utility/ui/widgets.dart';
import 'package:pass_the_dietitian_exam/views/BuySubscription.dart';
import 'package:pass_the_dietitian_exam/views/ChangePassword.dart';
import 'package:pass_the_dietitian_exam/views/DeleteView.dart';
import 'package:pass_the_dietitian_exam/views/LoginView.dart';
import 'package:pass_the_dietitian_exam/views/SubscriptionPlan.dart';

class SettingView extends StatefulWidget {
  const SettingView({Key? key}) : super(key: key);

  @override
  _SettingViewState createState() => _SettingViewState();
}

class _SettingViewState extends State<SettingView> {
  var vName = "0.0".obs;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getAppVersion();
  }

  getAppVersion() async {
    vName.value = await Helper().getVersion();
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: Column(
      children: [
        Expanded(
          child: Padding(
              padding: const EdgeInsets.only(left: 20, right: 20, top: 8),
              child: uc.isLogin.value
                  ? Column(
                      children: [
                        itemView(
                            uc.isSubscribe.value == 1
                                ? "Current Plan"
                                : "Buy The Plan",
                            SvgImages.nextArrowIc),
                        const SizedBox(height: 16),
                        itemView("Change Password", SvgImages.nextArrowIc),
                        const SizedBox(height: 16),
                        itemView("Delete Account", SvgImages.nextArrowIc),
                        const SizedBox(height: 16),
                        itemView("Logout", SvgImages.logoutIc),
                      ],
                    )
                  : Column(
                      children: [
                        itemView("Login", SvgImages.nextArrowIc),
                      ],
                    )),
        ),
        Obx(
          () => Text("App Version : ${vName.value}",
              style: textStyles.medium(fontColor: grayColor, fontSize: 18.0)),
        ),
        const SizedBox(height: 16)
      ],
    ));
  }

  itemView(title, icon) {
    return InkWell(
      onTap: () {
        if (title == "Current Plan") {
          Get.to(() => const SubscriptionPlan(), arguments: title);
        } else if (title == "Buy The Plan") {
          Get.to(() => const BuySubscription(), arguments: title);
        } else if (title == "Change Password") {
          Get.to(() => const ChangePassword());
        } else if (title == "Delete Account") {
          Get.to(() => const DeleteView());
        } else if (title == "Logout") {
          logoutPopup();
        } else {
          Get.offAll(() => const LoginView());
        }
      },
      child: Container(
        padding: const EdgeInsets.all(16),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10), color: settingBgColor),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(title,
                style:
                    textStyles.semiBold(fontSize: 14.0, fontColor: blackColor)),
            SvgPicture.asset(icon)
          ],
        ),
      ),
    );
  }

  logoutPopup() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Container(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text("Logout",
                      style: textStyles.bold(
                          fontColor: blackColor, fontSize: 20.0)),
                  const SizedBox(height: 8),
                  Text("Are you sure you want to logout?",
                      style: textStyles.medium(
                          fontColor: blackColor, fontSize: 16.0)),
                  const SizedBox(height: 16),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                          child: CustomButton.normalButton(
                              title: "Cancel",
                              backgroundColor: buttonDisable,
                              txtColor: blackColor,
                              onTap: () {
                                Get.back();
                              })),
                      const SizedBox(width: 20),
                      Expanded(
                          child: CustomButton.normalButton(
                              title: "Ok",
                              backgroundColor: themeColor,
                              onTap: () {
                                Get.back();
                                apiLoader(asyncCall: () => logoutApi());
                              }))
                    ],
                  ),
                ],
              ),
            ),
          );
        });
  }

  logoutApi() {
    RequestManager.postRequest(
      uri: EndPoints.logout,
      hasBearer: true,
      onStatusSuccess: (responseBody) {
        hideAppLoader();
        var res = jsonDecode(responseBody);
        if (res["status"] == 1) {
          uc.isFirstTime = true;
          SessionImpl.box!.erase();
          uc.isLogin.value = false;
          uc.isSubscribe.value = 0;
          uc.userName.value = "";
          uc.email.value = "";
          uc.profileImage.value = "";
          uc.firstName.value = "";
          uc.lastName.value = "";
          SessionImpl.setData(SessionKeys.keyFcmToken, "test");
          Get.offAll(() => const LoginView());
        }
      },
      onFailure: (error) {
        hideAppLoader();
      },
    );
  }
}
