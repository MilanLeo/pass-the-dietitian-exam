import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:pass_the_dietitian_exam/models/SubscriptionModel.dart';
import 'package:pass_the_dietitian_exam/utility/Impl/global.dart';
import 'package:pass_the_dietitian_exam/utility/Impl/session_impl.dart';
import 'package:pass_the_dietitian_exam/utility/endpoint/endpoint.dart';
import 'package:pass_the_dietitian_exam/utility/ui/customs/custom_button.dart';
import 'package:pass_the_dietitian_exam/utility/ui/customs/in_app_purchase.dart';
import 'package:pass_the_dietitian_exam/utility/ui/images.dart';
import 'package:pass_the_dietitian_exam/utility/ui/styles.dart';
import 'package:pass_the_dietitian_exam/utility/ui/widgets.dart';
import 'package:pass_the_dietitian_exam/views/CheckoutView.dart';
import 'package:pass_the_dietitian_exam/views/ContactUs.dart';
import 'package:pass_the_dietitian_exam/views/DashboardView.dart';
import '../utility/ui/color_pallete.dart';

class BuySubscription extends StatefulWidget {
  const BuySubscription({Key? key}) : super(key: key);

  @override
  _BuySubscriptionState createState() => _BuySubscriptionState();
}

class _BuySubscriptionState extends State<BuySubscription> {
  RxBool isLoading = true.obs;
  RxList<PlanList> planList = <PlanList>[].obs;
  var title = "";

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    if (Get.arguments != null) {
      title = Get.arguments;
    }
    apiLoader(asyncCall: () => getSubscriptionPlan());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(top: 8),
          child: Obx(
            () => Column(
              children: [
                InkWell(
                  onTap: () {
                    Get.back();
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(left: 20, right: 20),
                    child: Row(
                      children: [
                        SvgPicture.asset(SvgImages.backIc),
                        const SizedBox(width: 12),
                        Flexible(
                            child: Text(title,
                                style: textStyles.semiBold(
                                    fontSize: 20.0, fontColor: blackColor))),
                      ],
                    ),
                  ),
                ),
                isLoading.value
                    ? const SizedBox.shrink()
                    : planList.isEmpty
                        ? Expanded(child: Widgets.dataNotFound())
                        : Expanded(
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Padding(
                                  padding:
                                      const EdgeInsets.only(top: 8, bottom: 20),
                                  child: Container(
                                    width: Get.width,
                                    padding: const EdgeInsets.only(
                                        left: 16,
                                        right: 16,
                                        top: 32,
                                        bottom: 32),
                                    color: pinkColor,
                                    child: Column(
                                      children: [
                                        Text(planList[0].planName,
                                            style: textStyles.semiBold(
                                                fontSize: 20.0,
                                                fontColor: blackColor)),
                                        const SizedBox(height: 4),
                                        Text("\$${planList[0].price}",
                                            style: textStyles.bold(
                                                fontColor: themeColor,
                                                fontSize: 30.0)),
                                        const SizedBox(height: 4),
                                        Text(
                                            "Validity: ${planList[0].planName}",
                                            style: textStyles.medium(
                                                fontColor: blackColor,
                                                fontSize: 12.0)),
                                      ],
                                    ),
                                  ),
                                ),
                                Expanded(
                                    child: Padding(
                                        padding: const EdgeInsets.only(
                                            left: 20, right: 20),
                                        child: Column(
                                          children: List.generate(
                                              planList[0].description!.length,
                                              (index) {
                                            return rulesView(planList[0]
                                                .description![index]);
                                          }),
                                        ))),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 20, right: 20, bottom: 16),
                                  child: CustomButton.normalButton(
                                      title: "Pay Now",
                                      onTap: () {
                                        if (GetPlatform.isIOS) {
                                          if (planList[0].isInApp == 0) {
                                            apiLoader(
                                                asyncCall: () =>
                                                    createAndroidPayment());
                                          } else {
                                            uc.isPurchase = true;
                                            apiLoader(asyncCall: () => InAppPurchaseUtils.inAppPurchaseUtilsInstance.buySubscriptionPlan(planList[0].price));
                                          }
                                        } else {
                                          apiLoader(
                                              asyncCall: () =>
                                                  createAndroidPayment());
                                        }
                                      }),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 20, right: 20, bottom: 16),
                                  child: CustomButton.normalButton(
                                      title: "Contact Us",
                                      onTap: () {
                                        Get.to(() => const ContactUs());
                                      }),
                                ),
                              ],
                            ),
                          ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  rulesView(title) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 16),
      child: Row(
        children: [
          SvgPicture.asset(SvgImages.checkIc),
          const SizedBox(width: 8),
          Text(title,
              style: textStyles.normal(fontColor: blackColor, fontSize: 14.0)),
        ],
      ),
    );
  }

  getSubscriptionPlan() {
    RequestManager.postRequest(
      uri: EndPoints.getPlanList,
      hasBearer: true,
      retry: () {
        apiLoader(asyncCall: () => getSubscriptionPlan());
      },
      onStatusSuccess: (responseBody) {
        hideAppLoader();
        isLoading.value = false;
        LogicalComponents.subscriptionModel =
            subscriptionModelFromJson(responseBody);
        if (LogicalComponents.subscriptionModel.status == 1) {
          planList.value =
              LogicalComponents.subscriptionModel.result!.planList!;
        }
      },
      onFailure: (error) {
        hideAppLoader();
      },
    );
  }

  createAndroidPayment() {
    RequestManager.postRequest(
      uri: EndPoints.createAndroidPayment,
      hasBearer: true,
      onStatusSuccess: (responseBody) {
        hideAppLoader();
        var res = jsonDecode(responseBody);
        if (res["status"] == 1) {
          Get.to(() => const Checkout(), arguments: res["result"]["url"]);
        } else {
          Widgets.showSnackBar(title: "Error", errorMessage: res["message"]);
        }
      },
      onFailure: (error) {
        hideAppLoader();
      },
    );
  }

  // createIOSPayment(String? transactionId) {
  //   var body = {
  //     "transactionId": transactionId,
  //     "price": planList[0].price,
  //     "status": "complete",
  //   };
  //   RequestManager.postRequest(
  //     uri: EndPoints.createIOSPayment,
  //     hasBearer: true,
  //     body: body,
  //     onStatusSuccess: (responseBody) {
  //       hideAppLoader();
  //       var res = jsonDecode(responseBody);
  //       if (res["status"] == 1) {
  //         uc.isFirstTime = true;
  //         RequestManager.getSnackToast(
  //             title: "Success",
  //             messageText: Text(res["message"],
  //                 style: const TextStyle(
  //                   fontSize: dimen.textMedium,
  //                   color: Colors.white,
  //                 )),
  //             colorText: whiteColor,
  //             backgroundColor: themeColor,
  //             onDismissed: () {
  //               Get.offAll(() => const Dashboard());
  //             });
  //       } else {
  //         Widgets.showSnackBar(title: "Error", errorMessage: res["message"]);
  //       }
  //     },
  //     onFailure: (error) {
  //       hideAppLoader();
  //     },
  //   );
  // }
}
