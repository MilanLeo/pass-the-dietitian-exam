import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:pass_the_dietitian_exam/utility/Impl/global.dart';
import 'package:pass_the_dietitian_exam/utility/ui/images.dart';
import 'package:pass_the_dietitian_exam/utility/ui/styles.dart';
import '../utility/ui/color_pallete.dart';

class SubscriptionPlan extends StatefulWidget {
  const SubscriptionPlan({Key? key}) : super(key: key);

  @override
  _SubscriptionPlanState createState() => _SubscriptionPlanState();
}

class _SubscriptionPlanState extends State<SubscriptionPlan> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(top: 8),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              InkWell(
                onTap: () {
                  Get.back();
                },
                child: Padding(
                  padding: const EdgeInsets.only(left: 20, right: 20),
                  child: Row(
                    children: [
                      SvgPicture.asset(SvgImages.backIc),
                      const SizedBox(width: 12),
                      Flexible(
                          child: Text(Get.arguments,
                              style: textStyles.semiBold(
                                  fontSize: 20.0, fontColor: blackColor))),
                    ],
                  ),
                ),
              ),
              const SizedBox(height: 8),
              Container(height: 2, color: deviderColor),
              Padding(
                padding: const EdgeInsets.all(20),
                child: InkWell(
                  onTap: () {},
                  child: Container(
                    padding: const EdgeInsets.only(
                        left: 16, right: 16, top: 32, bottom: 32),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(16),
                        color: pinkColor),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(uc.planName.value,
                                  style: textStyles.semiBold(
                                      fontSize: 20.0, fontColor: blackColor)),
                              Text("Start Date: ${uc.startDate.value}",
                                  style: textStyles.medium(
                                      fontSize: 14.0, fontColor: blackColor)),
                            ],
                          ),
                        ),
                        Flexible(
                            child: Text("\$${uc.planPrice.value}",
                                style: textStyles.bold(
                                    fontColor: themeColor, fontSize: 26.0)))
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
