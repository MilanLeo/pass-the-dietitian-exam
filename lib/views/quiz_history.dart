import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:pass_the_dietitian_exam/models/quiz_history_model.dart';
import 'package:pass_the_dietitian_exam/utility/Impl/global.dart';
import 'package:pass_the_dietitian_exam/utility/endpoint/endpoint.dart';
import 'package:pass_the_dietitian_exam/utility/ui/color_pallete.dart';
import 'package:pass_the_dietitian_exam/utility/ui/images.dart';
import 'package:pass_the_dietitian_exam/utility/ui/styles.dart';
import 'package:pass_the_dietitian_exam/utility/ui/widgets.dart';
import 'package:pass_the_dietitian_exam/views/quiz_history_details.dart';


class QuizHistory extends StatefulWidget {
  var subCategoryName;
  var subCategoryId;
  var categoryId;
   QuizHistory({this.categoryId,this.subCategoryId,this.subCategoryName});

  @override
  State<QuizHistory> createState() => _QuizHistoryState();
}

class _QuizHistoryState extends State<QuizHistory> {
  QuizHistoryModel? quizHistoryModel;
var isLoading = true.obs;
  @override
  void initState() {
    apiLoader(asyncCall: () =>getHistoryApi());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              InkWell(
                onTap: () {
                  Get.back();
                },
                child: Padding(
                  padding: const EdgeInsets.only(left: 20, right: 20),
                  child: Row(
                    children: [
                      SvgPicture.asset(SvgImages.backIc),
                      const SizedBox(width: 12),
                      Flexible(
                          child: Text(widget.subCategoryName,
                              style: textStyles.semiBold(
                                  fontSize: 20.0, fontColor: blackColor))),

                    ],
                  ),
                ),
              ),
              const SizedBox(height: 8),
              Container(height: 2, color: deviderColor),
             Obx(() =>  isLoading.value ? const SizedBox() :Expanded(
               child: ListView.separated(
                    shrinkWrap: true,
                    padding: const EdgeInsets.all(20),
                    itemBuilder: (context,index){
                      DateTime parseDate =
                      new DateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(quizHistoryModel!.result![index].createdAt!);
                      var inputDate = DateTime.parse(parseDate.toString());
                      var outputFormat = DateFormat('MM/dd/yyyy');
                      var outputDate = outputFormat.format(inputDate);

                      return  GestureDetector(
                        onTap: (){
               Get.to(()=>QuizHistoryDetails(quizHistoryModel!.result![index].uniqueCheck));
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(top: 4.0, right: 16),
                              child: Container(
                                width: 15,
                                height: 15,
                                decoration: BoxDecoration(
                                    color: Color(int.parse(quizHistoryModel!.result![index]
                                        .resultBarColor!
                                        .toString()
                                        .replaceAll("#", "0xff"))),
                                    shape: BoxShape.circle),
                              ),
                            ),
                            Flexible(
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Expanded(
                                      child: Text(outputDate,
                                          style: textStyles.normal(
                                              fontSize: 14.0, fontColor: blackColor))),
                                  Row(
                                    children: [
                                      Text("${quizHistoryModel!.result![index].percentage.toStringAsFixed(0)}%",
                                          style: textStyles.semiBold(
                                              fontColor: blackColor, fontSize: 14.0)),
                                      const SizedBox(width: 10,),
                                      const Icon(Icons.arrow_forward_ios,size: 15,)
                                    ],
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      );
                    }, separatorBuilder: (context,index)=>const SizedBox(height: 20,), itemCount: quizHistoryModel!.result!.length),
             ))
            ],
          ),
        ),
      ),
    );
  }


  getHistoryApi() {
    var body = {
      "topicId" : widget.categoryId,
      "subTopicId" : widget.subCategoryId
    };
    print(body);
    RequestManager.postRequest(
      uri: EndPoints.getAllSubHistory,
      hasBearer: true,
      body: body,
      onStatusSuccess: (responseBody) {
        hideAppLoader();
        print(responseBody);
        isLoading.value = false;
        setState(() {
           quizHistoryModel = quizHistoryModelFromJson(responseBody);
        });

      },
      onFailure: (error) {
        hideAppLoader();
      },
    );
  }
}
