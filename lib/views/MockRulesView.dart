import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:pass_the_dietitian_exam/utility/Impl/global.dart';
import 'package:pass_the_dietitian_exam/utility/ui/color_pallete.dart';
import 'package:pass_the_dietitian_exam/utility/ui/customs/custom_button.dart';
import 'package:pass_the_dietitian_exam/utility/ui/images.dart';
import 'package:pass_the_dietitian_exam/utility/ui/styles.dart';
import 'package:pass_the_dietitian_exam/utility/ui/widgets.dart';
import 'package:pass_the_dietitian_exam/views/LoginView.dart';
import 'package:pass_the_dietitian_exam/views/QuestionsView.dart';

class MockRulesView extends StatefulWidget {
  const MockRulesView({Key? key}) : super(key: key);

  @override
  _MockRulesViewState createState() => _MockRulesViewState();
}

class _MockRulesViewState extends State<MockRulesView> {
  RxBool isLoading = true.obs;
  var mockRulesList;
  var title = "";
  var topicId;
  var subTopicId;
  var time;
  var isExam;
  var isFree;
  var isDomain;

  @override
  void initState() {
    super.initState();
    if (Get.arguments != null) {
      topicId = Get.arguments[0];
      title = Get.arguments[1];
      mockRulesList = Get.arguments[2];
      time = Get.arguments[3];
      isExam = Get.arguments[4];
      subTopicId = Get.arguments[5];
      isFree = Get.arguments[6];
      isDomain = Get.arguments[7];
    } else {
      isLoading.value = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(top: 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              InkWell(
                onTap: () {
                  Get.back();
                },
                child: Padding(
                  padding: const EdgeInsets.only(left: 20, right: 20),
                  child: Row(
                    children: [
                      SvgPicture.asset(SvgImages.backIc),
                      const SizedBox(width: 12),
                      Flexible(
                          child: Text("Mock Rule",
                              style: textStyles.semiBold(
                                  fontSize: 20.0, fontColor: blackColor))),
                    ],
                  ),
                ),
              ),
              const SizedBox(height: 16),
              Container(height: 2, color: deviderColor),
              Padding(
                padding: const EdgeInsets.only(
                    left: 20, right: 20, top: 20, bottom: 20),
                child: Align(
                    alignment: Alignment.topLeft,
                    child: Text("Mock rules before you start",
                        style: textStyles.semiBold(
                            fontSize: 18.0, fontColor: blackColor))),
              ),
              Expanded(
                child: mockRulesList.isEmpty
                    ? Widgets.dataNotFound()
                    : ListView.builder(
                        padding:
                            const EdgeInsets.only(top: 0, left: 20, right: 20),
                        physics: const ClampingScrollPhysics(),
                        shrinkWrap: true,
                        itemBuilder: (context, index) {
                          return Padding(
                            padding: const EdgeInsets.only(bottom: 32),
                            child: Text(
                                (index + 1).toString() +
                                    ". " +
                                    mockRulesList[index],
                                style: textStyles.normal(
                                    fontColor: blackColor, fontSize: 16.0)),
                          );
                        },
                        itemCount: mockRulesList.length),
              ),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: CustomButton.normalButton(
                    title: "Let's Start",
                    onTap: () {
                      if (uc.isLogin.value) {
                        if (isFree == 1 || uc.isSubscribe.value == 1) {
                          Get.to(() => const QuestionsView(), arguments: [
                            topicId,
                            title,
                            time,
                            isExam,
                            subTopicId,
                            isDomain,
                            title
                          ]);
                        } else {
                          Widgets.showAlertDialog(context);
                        }
                      } else {
                        Widgets.showSnackBar(
                            title: "Error",
                            errorMessage:
                                "Please login to access free version.");
                        Get.offAll(() => const LoginView());
                      }
                    }),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
