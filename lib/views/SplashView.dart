import 'package:flutter/material.dart';
import 'package:pass_the_dietitian_exam/utility/Impl/global.dart';
import 'package:pass_the_dietitian_exam/utility/ui/color_pallete.dart';
import 'package:pass_the_dietitian_exam/utility/ui/images.dart';
import '../utility/Impl/session_impl.dart';

class SplashView extends StatefulWidget {
  const SplashView({Key? key}) : super(key: key);

  @override
  _SplashViewState createState() => _SplashViewState();
}

class _SplashViewState extends State<SplashView> {
  @override
  initState() {
    //Get And Setup Notification and token
    SessionImpl.setData(SessionKeys.keyFcmToken, "abcd");
    // var page = Routes.home;
    // Future.delayed(Duration(seconds: 4), () {
    //   Get.offAllNamed(page);
    // });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: themeColor,
      body: Stack(
        children: [
          Container(
            height: double.infinity,
            width: double.infinity,
            decoration: BoxDecoration(
              image: DecorationImage(
                fit: BoxFit.cover,
                colorFilter: ColorFilter.mode(
                    Colors.black.withOpacity(1.0), BlendMode.dstATop),
                image: AssetImage(ImageResource.splashImg),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
