
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:pass_the_dietitian_exam/utility/Impl/global.dart';
import 'package:pass_the_dietitian_exam/utility/Impl/session_impl.dart';
import 'package:pass_the_dietitian_exam/utility/ui/customs/in_app_purchase.dart';
import 'utility/Impl/nav_pages.dart';
import 'utility/Impl/utilities_impl.dart';
import 'utility/ui/styles.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await SessionImpl().initializeApp();
  uc.isLogin.value = SessionImpl.getData(SessionKeys.keyLogin) ?? false;
  uc.deviceToken.value = await Utilities.getDeviceToken();
  SessionImpl.setData(SessionKeys.keyFcmToken, "test");
  Future.delayed(const Duration(seconds: 3), () {
    runApp(const DietitianApp());
  });
}

class DietitianApp extends StatefulWidget {
  const DietitianApp({Key? key}) : super(key: key);

  @override
  State<DietitianApp> createState() => _DietitianAppState();

}

class _DietitianAppState extends State<DietitianApp> {
  final InAppPurchaseUtils inAppPurchaseUtils = InAppPurchaseUtils.inAppPurchaseUtilsInstance;
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle.dark.copyWith(statusBarColor: Colors.transparent));
    return GetMaterialApp(
      builder: (context, child) {
        return MediaQuery(
          child: child!,
          data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
        );
      },
      title: 'Pass The Dietitian Exam',
      debugShowCheckedModeBanner: false,
      defaultTransition: Transition.cupertino,
      initialRoute: Routes.dashboard,
      getPages: Pages.pages,
      theme: Styles.lightTheme(),
      themeMode: ThemeMode.light,
      initialBinding: BindingsBuilder(() {
        Get.put<InAppPurchaseUtils>(inAppPurchaseUtils);
      }),
    );
  }
}
