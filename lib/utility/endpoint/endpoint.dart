import 'dart:convert';
import 'dart:io';
import 'package:pass_the_dietitian_exam/utility/Impl/global.dart';
import 'package:path/path.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:pass_the_dietitian_exam/utility/Impl/session_impl.dart';
import 'package:pass_the_dietitian_exam/utility/Impl/utilities_impl.dart';
import 'package:pass_the_dietitian_exam/utility/ui/color_pallete.dart';
import 'package:pass_the_dietitian_exam/utility/ui/widgets.dart';
import 'package:pass_the_dietitian_exam/views/NoInternetView.dart';

class RequestManager {
  RequestManager();

  static postRequest(
      {@required uri,
      baseUrl, // require for login, and other API url get from Login Response
      body,
      header,
      jsonEncoded = true,
      bool hasBearer = false,
        Function(dynamic responseBody)? onSuccess,
      Function(dynamic responseBody)? onStatusSuccess,
      @required Function(dynamic error)? onFailure,
      Function? onTimeout,
      Function? retry,
        Function(dynamic error)? onConnectionFailed}) async {
    bool isConnected = await Utilities.isConnectedNetwork();
    if (!isConnected) {
      if (onConnectionFailed != null)
        onConnectionFailed("Check your internet connection and try again");
      // getSnackToast(
      //   title: 'Connection Error',
      //   message: "Check your internet connection and try again",
      // );
      hideAppLoader();
      Get.to(() => const NoInterNetView())!.then((value) {
        if (retry != null) {
          retry();
        }
      });
      return;
    }

    if (hasBearer) {
      header = {
        'Content-Type': 'application/json',
        'Authorization':
            'Bearer ${SessionImpl.getData(SessionKeys.keyBearerToken)}',
      };
      var tkn = SessionImpl.getData(SessionKeys.keyBearerToken);
      print('Bearer : $tkn');
    } else {
      if (header == null) {
        header = {
          'Content-Type': 'application/json',
          "Accept": "application/json"
        };
      }
    }

    if (jsonEncoded) {
      body = jsonEncode(body);
    }

    // print(body);
    if (baseUrl == null) {
      baseUrl = EndPoints.baseUrl;
    }
    var url = baseUrl + uri;
print(url);
    //make sure body should not have integer values, below body accept only String values.
    //make sure here body only accept Map type data till you not set 'Content-Type': 'application/json',
    http.post(Uri.parse(url), body: body, headers: header).then((response) {
      if (onSuccess != null)onSuccess(response.body);
      if (response.body.isNotEmpty) {
        var json = jsonDecode(response.body);
        print(json);
        if (json['status'] != null) {
          if (onStatusSuccess != null) {
            onStatusSuccess(response.body);
          }
        } else if (json['status'] == 3) {
          onStatusSuccess!(response.body);
        } else {
          hideAppLoader();
        }
      }
    }).catchError((error) {
      print('Error : catchError $error');
      print(url);
      hideAppLoader();
      RequestManager.getSnackToast(
          message: '${error.toString()}',
          colorText: Colors.white,
          backgroundColor: Colors.red);
      if (onFailure != null) {
        onFailure(error);
      }
    }).timeout(const Duration(seconds: global.REQUEST_MAX_TIMEOUT),
        onTimeout: () {
      print(url);
      print('Error : TimeOut');
      if (onTimeout != null) onTimeout();
    });
  }

  // RequestManager.postRequest(
  //   body: body,
  //   onSuccess: (response, requestCode) {},
  //   onFailure: (errorMsg, requestCode) {},
  //   onTimeout: (requestCode) {},
  //   onConnectionFailed: (error, requestCode) {});

  static uploadImage({
    @required uri,
    @required File? file,
    @required String? type,
    header,
    bool hasBearer = false,
    @required Function(dynamic responseBody)? onSuccess,
    @required Function(dynamic error)? onFailure,
    Function? onTimeout,
    Function? onConnectionFailed,
  }) async {
    bool isConnected = await Utilities.isConnectedNetwork();
    if (!isConnected) {
      if (onConnectionFailed != null)
        onConnectionFailed("Check your internet connection and try again");
      getSnackToast(
        title: 'Connection Error',
        message: "Check your internet connection and try again",
      );
      // Get.to(NoInternetView());
      return;
    }

    var url = EndPoints.baseUrl + uri;
    var request = http.MultipartRequest('POST', Uri.parse(url));
    if (hasBearer) {
      header = {
        'Authorization':
            'Bearer ${SessionImpl.getData(SessionKeys.keyBearerToken)}',
      };
      request.headers.addAll(header);
    }

    request.fields.addAll({
      'type': type!,
    });

    if (file != null) {
      //DelegatingStream.typed(file.openRead())
      var stream = http.ByteStream(Stream.castFrom(file.openRead()));
      var length = await file.length();
      var multipartFile = http.MultipartFile("file", stream, length,
          filename: basename(file.path));
      request.files.add(multipartFile);
    }

    await request.send().then((resStream) async {
      resStream.stream.transform(utf8.decoder).listen((response) {
        final responseBody = json.decode(response);
        print(responseBody);
        onSuccess!(response);
      });
    }).catchError((error) {
      print('Error : catchError $error');
      print(url);
      // RequestManager.getSnackToast(
      //     message: error.toString(),
      //     colorText: Colors.white,
      //     backgroundColor: Colors.red);

      onFailure!(error);
    }).timeout(const Duration(seconds: global.REQUEST_MAX_TIMEOUT),
        onTimeout: () {
      print(url);
      print('Error : TimeOut');
      if (onTimeout != null) onTimeout();
    });
  }

  apiPostFormData({
    @required url,
    required Map<String, String> map,
    header,
    bool hasBearer = false,
    @required Function? onSuccess,
    @required Function? onFailure,
    Function? onTimeout,
    Function? onConnectionFailed,
  }) async {
    if (hasBearer && header == null)
      header = {
        'Authorization':
            'Bearer ${SessionImpl.getData(SessionKeys.keyBearerToken)}',
      };

    var request = http.MultipartRequest('POST', Uri.parse(url))
      ..headers.addAll(header)
      ..fields.addAll(map);

    await request.send().then((response) async {
      var res = await http.Response.fromStream(response);
      if (onSuccess != null) onSuccess(res);
    }).catchError((error) {
      print('Error : catchError $error');
      print(url);
      // RequestManager.getSnackToast(
      //     message: error.toString(),
      //     colorText: Colors.white,
      //     backgroundColor: Colors.red);

      onFailure!(error);
    }).timeout(const Duration(seconds: global.REQUEST_MAX_TIMEOUT),
        onTimeout: () {
      print(url);
      print('Error : TimeOut');
      if (onTimeout != null) onTimeout();
    });
  }

  static getRequest(
      {@required uri,
      header,
      Function? onSuccess,
      Function? onFailure,
      Function? onTimeout,
      Function? onConnectionFailed}) async {
    bool isConnected = await Utilities.isConnectedNetwork();
    if (!isConnected) {
      onConnectionFailed!(
        "Check your internet connection and try again",
      );
      return;
    }
    String url = EndPoints.baseUrl + uri;
    http.get(Uri.parse(url), headers: header).then((response) {
      print('Done : Success');
      if (onSuccess != null) onSuccess(response);
    }).catchError((error) {
      print('Error : catchError');
      if (onFailure != null) onFailure(error.toString());
    }).timeout(const Duration(seconds: global.REQUEST_MAX_TIMEOUT),
        onTimeout: () {
      print('Error : TimeOut');
      if (onTimeout != null) onTimeout();
    });
  }

  /// if hasCloseButton = true, then make sure text should be get from "title" variable for default titleText widget
  static getSnackToast({
    title = 'Error',
    message = '',
    snackPosition: SnackPosition.TOP,
    backgroundColor,
    colorText,
    Widget? icon,
    hasCloseButton = false,
    Widget? titleText,
    Widget? messageText,
    Duration? duration,
    Function? onTapSnackBar,
    Function? onTapButton,
    bool withButton = false,
    buttonText = 'Ok',
    Function? onDismissed,
  }) {
    backgroundColor =
        backgroundColor ?? (isDarkMode ? Colors.white : Colors.black);
    colorText = colorText ?? (isDarkMode ? Colors.black : Colors.white);
    if (hasCloseButton) {
      titleText = titleText ??
          Row(
            mainAxisSize: MainAxisSize.max,
            children: [
              Expanded(
                child: Text('$title',
                    style: TextStyle(
                        color: colorText,
                        fontSize: dimen.textLarge,
                        fontWeight: FontWeight.bold)),
              ),
              InkWell(
                onTap: () {
                  if (Get.isSnackbarOpen) Get.back();
                },
                child: Icon(Icons.close, color: colorText, size: 20),
              )
            ],
          );
      if (message == null || message.toString().isEmpty) {
        messageText = messageText ?? const Center();
      }
    }

    if (!Get.isSnackbarOpen)
      Get.snackbar(
        !hasCloseButton ? title : null,
        message,
        mainButton: withButton
            ? TextButton(
                onPressed: () {
                  if (onTapButton != null) onTapButton();
                },
                child: Text(buttonText),
              )
            : null,
        onTap: (tap) {
          if (onTapSnackBar != null) onTapSnackBar(tap);
        },
        duration: duration ?? 2.seconds,
        snackPosition: snackPosition,
        titleText: titleText,
        messageText: messageText,
        backgroundColor: backgroundColor,
        icon: icon,
        colorText: colorText,
        snackbarStatus: (status) {
          print(status);
          if (status == SnackbarStatus.CLOSED) {
            if (onDismissed != null) onDismissed();
          }
        },
      );
  }
}

/*
eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiJlMmVlMzEzNi0wMzRlLTQxYzEtOGJlOS00NGIyZjkxYmIxZGYiLCJqdGkiOiI1Mzk0MzI3NTE4ZGE0MmJmMGNlMDk3ZTdjN2UyZTRjMjY4ZjA4OTY0MmM5NWYzN2I2OTk2YWM0NjFmZTcxYjFlYzFiOTc3OGE1OTAwOWMwMyIsImlhdCI6MTYyNTE0NTI5OCwibmJmIjoxNjI1MTQ1Mjk4LCJleHAiOjE2NTY2ODEyOTgsInN1YiI6IjlkNmViOTI0LTVhYTQtNGE1NS05N2IzLTY5MzZlZDc0NjkzZSIsInNjb3BlcyI6W119.Gh_-e3IVj7fvcMZZrsWCLudkN--AJo3HCkCoEQIM0iMjG7yPmevY_y_YNiviOmjbYNxDoq-ciqY6y4bxoRIhoE5boEb20FMpiNDKncuUvuadePWw9uCI9g43ue6CmQXKT1gru7mRY326wkZNhT6WrxIf5s13nnmmdd1ilhpv8Jrtu9GpSuBqtIICCey40lSMPCzaK2gNz9esPZis5ucDZp4RPrpDsXIqlI0jKCy7T305q6BJDnEjbSkLWx8engtNura6UcwTAjXpRuyMIx8ZFh626Uonb7iyvXu44ET1KCtCnlQG9Ehk5n-CWie_-0UonOfXDguWsrBipAHrXP_1Vy9S-dkAkjEieEdCPtCHJ4W7YpR_IWz1bPJ99jmxK1xIe1u-QzSlsMJcUQtMZE8fnaE7ga2vMF0r5IFMBfIdP6wngsI1OKhKlAB0TsO1YgkFvNKtM1IHaY1QQngrAY3AJOSSJoYGHf7sh7ljwLLfo-ZCDNVWZttNxzvOHEvOd1OH78notuzMzxvzgoZdhfXg8UR31qrdC9MGg-3OHzWIFuX1Wnb9xsGm4IFHxECaJIcb4cpKWd2XHDJ8BthybHuJgZyHj5IRnHXj4FaPjX2q-XWKGp-DotNHoPB6E1Ji308OyOtLB2AtUORcyAz-dSdczIq8eFKIzeAxFi6OhcFh5ws

*/
