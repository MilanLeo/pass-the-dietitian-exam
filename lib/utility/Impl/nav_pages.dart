import 'package:get/get.dart';
import 'package:pass_the_dietitian_exam/views/DashboardView.dart';
import 'package:pass_the_dietitian_exam/views/LoginView.dart';
import 'package:pass_the_dietitian_exam/views/SplashView.dart';

class Pages {
  static final pages = [
    GetPage(name: Routes.splash, page: () => SplashView()),
    GetPage(name: Routes.dashboard, page: () => Dashboard()),
    GetPage(name: Routes.login, page: () => LoginView()),
    GetPage(name: Routes.login, page: () => LoginView()),
  ];
}

class Routes {
  static const splash = '/splash';
  static const login = '/login';
  static const dashboard = '/dashboard';
  static const verification = '/verification';
  static const infoauth = '/information';
  static const contactDetail = '/contactDetail';
  static const nearByPersonDetail = '/nearByPersonDetail';
  static const blockList = '/blockedList';
  static const aboutUs = '/about';
  static const help = '/help';
  static const settings = '/settings';

  static const faq = '/faq';
  static const policy = '/policy';
  static const tc = '/tc';
  static const notification = '/notification';
  static const detailRequestNotification = '/detailNotification';
  static const profile = '/profile';
  static const profileUpdate = '/profileUpdate';
  // static const biometricAuth = '/biometricAuth';
  static const sysContact = '/user_contacts';
  static const mySub = '/mySub';
  static const viewImage = '/viewImage';
  static const pdfViewer = '/pdfViewer';
  static const qrCodeView = '/qrCodeView';
  static const changeNumber = '/changeNumber';
}
