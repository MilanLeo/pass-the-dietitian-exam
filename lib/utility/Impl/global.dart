import 'package:get/get.dart';
import 'package:pass_the_dietitian_exam/controllers/UIcontroller.dart';
import 'package:pass_the_dietitian_exam/models/QuestionModel.dart';
import 'package:pass_the_dietitian_exam/models/ResultModel.dart';
import 'package:pass_the_dietitian_exam/models/StatisticsModel.dart';
import 'package:pass_the_dietitian_exam/models/SubTopicModel.dart';
import 'package:pass_the_dietitian_exam/models/SubscriptionModel.dart';
import 'package:pass_the_dietitian_exam/models/TopicsModel.dart';
import 'package:pass_the_dietitian_exam/models/UserModel.dart';

// ignore: camel_case_types
class SessionKeys {
  static var keyBearerToken = 'keyBearerToken';
  static var keyFcmToken = 'fcmToken';
  static var keyLogin = "isLoggedIn";
  static var keyFirstName = "firstname";
  static var keyLastName = "lastName";
  static var keyEmail = "email";
  static var keymobile = "mobile";
  static var keyUserId = "userId";
  static var keyLoginProfile = "keyLoginProfile";
  static var paymentSuccess = "paymentSuccess";
  static var transId = "transId";
  static var planPrice = "planPrice";
}

// ignore: camel_case_types
abstract class global {
  //numbericals
  static const REQUEST_MAX_TIMEOUT = 20;

  //App strings
  static var androidAppAPIVersion = "1.0";
  static var iOSAppAPIVersion = "1.0";
}

class EndPoints {
  static var baseUrl = "http://app.passthedietitianexamapp.com/api/v1/";
  //   static var baseUrl = "https://testnftapp.com/dietitian-app-backend/public/api/v1/";

  static var accessToken = '';

  static const getTopics = 'getTopics';
  static const getSubTopics = 'getSubTopics';
  static const updateDeviceInfo = 'updateDeviceInfo';
  static const register = 'register';
  static const updateProfile = 'updateProfile';
  static const uploadFile = 'uploadFile';
  static const logout = 'logout';
  static const login = 'userLogin';
  static const changePassword = 'changePassword';
  static const deleteAccount = 'deleteAccount';
  static const forgotPassword = 'forgotPassword';
  static const getQuestion = 'getQuestion';
  static const submitQuestion = 'submitQuestion';
  static const getPlanList = 'getPlanList';
  static const getResult = 'getResult';
  static const getAllSubQuestionAnswerHistory = 'getAllSubQuestionAnswerHistory';
  static const createAndroidPayment = 'createAndroidPayment';
  static const createIOSPayment = 'createIosPayment';
  static const getStatistics = 'userStatistics';
  static const getStatisticsDetail = 'userSubStatistics';
  static const getAllSubHistory = 'getAllSubHistory';
  static const contactUs = 'contact-us';
}

// ignore: camel_case_types
class payload {
  static var page = 'page';
}

// ignore: camel_case_types
class dimen {
  //fontSize
  static const textMini = 8.0;
  static const textExtraSmall = 10.0;
  static const textSmall = 12.0;
  static const textSmallUp = 13.0;
  static const textNormal = 14.0;
  static const textTheme = 15.0;
  static const textMedium = 16.0;
  static const textXMedium = 17.0;
  static const textLarge = 18.0;
  static const textExtraLarge = 20.0;
  static const textSemiHuge = 22.0;
  static const textSHuge = 24.0;
  static const textHuge = 26.0;
  static const textMediumHuge = 28.0;
  static const textMonster = 35.0;
  static const textExtraHuge = 30.0;

//padding
  static const commonLftRhtPadding = 16.0;
  static const pad1 = 1.0;
  static const pad4 = 4.0;
  static const pad5 = 5.0;
  static const pad6 = 5.0;
  static const pad8 = 8.0;
  static const pad12 = 12.0;
  static const pad10 = 10.0;
  static const pad14 = 14.0;
  static const pad15 = 15.0;
  static const pad16 = 16.0;
  static const pad18 = 18.0;
  static const pad20 = 20.0;
  static const pad22 = 22.0;
  static const pad24 = 24.0;
  static const pad25 = 25.0;
  static const pad30 = 30.0;
  static const pad35 = 35.0;
  static const pad40 = 40.0;
  static const pad45 = 45.0;
  static const pad50 = 50.0;
  static const pad60 = 60.0;
  static const pad75 = 75.0;
  static const pad100 = 100.0;

//
  static const paddingLarge = 16.0;
  static const paddingMedium = 12.0;
  static const paddingSmall = 8.0;
  static const paddingVerySmall = 5.0;

  static const smallIconSize = 15.0;
  static const mediumIconSize = 18.0;
  static const largeIconSize = 28.0;
  static const actionsIconSize = 22.0;
  static const smallActionsIconSize = 18.0;
  static const iconSize = 24.0;
  static const iconButtonSize = 40.0;
  static const userIconSize = 60.0;
  static const userIconSizeLarge = 80.0;
  static const userIconSizeSmall = 50.0;
}

UIController uc = Get.put(UIController(), permanent: true);

class LogicalComponents {
  static var notificationListPage = 1;
  static var storeListPage = 1;
  static UserModel userModel = UserModel();
  static TopicModel topicModel = TopicModel();
  static SubTopicModel subTopicModel = SubTopicModel();
  static QuestionModel questionModel = QuestionModel();
  static SubscriptionModel subscriptionModel = SubscriptionModel();
  static ResultModel resultModel = ResultModel();
  static StatisticsModel statisticsModel = StatisticsModel();
}
