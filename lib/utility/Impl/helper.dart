import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:package_info_plus/package_info_plus.dart';

class Helper {
  BuildContext? context;
  Helper();
  Helper.of(this.context);

  String getRandomName() {
    final List<String> preFix = ['Aa', 'bo', 'Ce', 'Do', 'Ha', 'Tu', 'Zu'];
    final List<String> surFix = [
      'sad',
      'bad',
      'lad',
      'nad',
      'kat',
      'pat',
      'my'
    ];
    preFix.shuffle();
    surFix.shuffle();
    return '${preFix.first}${surFix.first}';
  }

  bool equalsIgnoreCase(String? string1, String? string2) {
    return string1!.toLowerCase().contains(string2!.toLowerCase());
  }

  hideKeyBoard() {
    if (context != null)
      FocusScope.of(context!).requestFocus(FocusNode());
    else {
      print('context required to hide the keyboard!!');
    }
  }

  // Function to convert milliseconds time to
  // Timer Format
  // Hours:Minutes:Seconds

  String formateMilliSeccond(double milliseconds) {
    String finalTimerString = "";
    String secondsString = "";

    // Convert total duration into time
    int hours = (milliseconds / (1000 * 60 * 60)).floor();
    int minutes = ((milliseconds % (1000 * 60 * 60)) / (1000 * 60)).floor();
    int seconds =
        ((milliseconds % (1000 * 60 * 60)) % (1000 * 60) / 1000).floor();

    // Add hours if there
    if (hours > 0) {
      finalTimerString = '$hours :';
    }

    // Prepending 0 to seconds if it is one digit
    if (seconds < 10) {
      secondsString = "0$seconds";
    } else {
      secondsString = '$seconds';
    }

    finalTimerString = finalTimerString + '$minutes : $secondsString';

    //      return  String.format("%02d Min, %02d Sec",
    //                TimeUnit.MILLISECONDS.toMinutes(milliseconds),
    //                TimeUnit.MILLISECONDS.toSeconds(milliseconds) -
    //                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(milliseconds)));

    // return timer string
    return finalTimerString;
  }

  systemOverlayBack([statusBarColor = Colors.black26]) {
    Future.delayed(Duration(milliseconds: 1500), () {
      SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: SystemUiOverlay.values);
    });

    // SystemChrome.setSystemUIOverlayStyle(
    //     SystemUiOverlayStyle(statusBarColor: statusBarColor));
  }

  onFullScreen() {
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: []);
  }

  ///primarySwatch is not a Color. It's MaterialColor. Which means it's a the different shades of a color a material app will use.
  /// primaryColor is one of those shades. To be exact, primaryColor is normally equal to primarySwatch[500].
  ///It is usually better to define a primarySwatch instead of primaryColor. Because some material components may use a different shade of the primaryColor for things such as shadow, border,
  MaterialColor createMaterialColor(Color color) {
    List strengths = <double>[.05];
    final swatch = <int, Color>{};
    final int r = color.red, g = color.green, b = color.blue;

    for (int i = 1; i < 10; i++) {
      strengths.add(0.1 * i);
    }
    strengths.forEach((strength) {
      final double ds = 0.5 - strength;
      swatch[(strength * 1000).round()] = Color.fromRGBO(
        r + ((ds < 0 ? r : (255 - r)) * ds).round(),
        g + ((ds < 0 ? g : (255 - g)) * ds).round(),
        b + ((ds < 0 ? b : (255 - b)) * ds).round(),
        1,
      );
    });
    return MaterialColor(color.value, swatch);
  }


  getVersionCode() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    var versionCode = packageInfo.buildNumber;
    print(versionCode);
    return versionCode;
  }

  getVersionName() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    var version = packageInfo.version;
    print(version);
    return version;
  }

  getVersion() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    return GetPlatform.isIOS ? packageInfo.version : packageInfo.buildNumber;
  }

  isDarkMode() {
    Get.isDarkMode;
    Get.isPlatformDarkMode;
    // var brightness = MediaQuery.of(context).platformBrightness;
    // bool darkModeOn = brightness == Brightness.dark;
  }

}
