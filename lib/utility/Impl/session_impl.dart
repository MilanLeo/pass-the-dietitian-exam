import 'dart:convert';
import 'package:get_storage/get_storage.dart';
import 'package:pass_the_dietitian_exam/models/UserModel.dart';
import 'package:pass_the_dietitian_exam/utility/Impl/global.dart';

class SessionImpl {
  static GetStorage? box;

  initializeApp() async {
    await GetStorage.init('dietitianExam');
    box = GetStorage('dietitianExam');
  }

  static setData(key, value) {
    box!.write(key, value);
  }

  static getData(key) {
    if (box!.hasData(key)) return box!.read(key);
    return null;
  }

  static setLoginProfileModel(var model) {
    box!.write(SessionKeys.keyLoginProfile, jsonEncode(model));
  }

  static getLoginProfileModel() {
    if (box!.hasData(SessionKeys.keyLoginProfile)) {
      var json = box!.read(SessionKeys.keyLoginProfile);
      return userDetailsFromJson(json);
    }
    return null;
  }
}
