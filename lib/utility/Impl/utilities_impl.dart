import 'dart:io';
import 'dart:math';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

class Utilities {
  static Future<bool> isConnectedNetwork() async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        print("Internet available");
        return true;
      } else {
        print("Internet not available");
        return false;
      }
    } on SocketException catch (_) {
      print("Something went wrong with connection");
      return false;
    }
  }

  static Future<String> getDeviceToken() async {
    final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
    if (GetPlatform.isAndroid) {
      return await deviceInfoPlugin.androidInfo
          .then((deviceInfo) => deviceInfo.id.toString());
    } else if (GetPlatform.isIOS) {
      return await deviceInfoPlugin.iosInfo
          .then((ss) => ss.identifierForVendor.toString());
    } else {
      return "";
    }
  }

  static String greeting() {
    var hour = DateTime.now().hour;
    if (hour < 12) {
      return 'Good Morning!';
    }
    if (hour < 17) {
      return 'Good Afternoon!';
    }
    return 'Good Evening!';
  }

  static String getRandomString({int length = 9}) {
    const _chars =
        'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
    Random _rnd = Random();
    return String.fromCharCodes(Iterable.generate(
        length, (_) => _chars.codeUnitAt(_rnd.nextInt(_chars.length))));
  }

  static int getRandomNumber({int max = 1000}) {
    var rng = Random();
    return rng.nextInt(max);
  }

  static Future<void> commonLaunchUrl(url) async {
    if (!await launchUrl(Uri.parse(url))) {
      throw Exception('Could not launch $url');
    }
  }

}
