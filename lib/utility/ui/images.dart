class ImageResource {
  static const splashImg = 'assets/images/splash.png';
  static const profileIc = 'assets/images/profile_placeholder.png';

}

class SvgImages {
  static String backIc = 'assets/icons/ic_back.svg';
  static String homeIc = 'assets/icons/ic_home.svg';
  static String statasticIc = 'assets/icons/ic_statastic.svg';
  static String settingIc = 'assets/icons/ic_setting.svg';
  static String nextIc = 'assets/icons/ic_next.svg';
  static String retionalIc = 'assets/icons/ic_retional.svg';
  static String retionalDisableIc = 'assets/icons/ic_retional_dis.svg';
  static String circleIc = 'assets/icons/ic_circle.svg';
  static String rightIc = 'assets/icons/ic_right.svg';
  static String wrongIc = 'assets/icons/ic_wrong.svg';
  static String logoutIc = 'assets/icons/ic_logout.svg';
  static String nextArrowIc = 'assets/icons/ic_next_arrow.svg';
  static String checkIc = 'assets/icons/ic_check.svg';
  static String circleFillIc = 'assets/icons/ic_circle_fill.svg';
  static String editIc = 'assets/icons/ic_edit.svg';
  static String editProfileIc = 'assets/icons/ic_edit_profile.svg';
  static String dataNotFoundIc = 'assets/icons/ic_datanotfound.svg';
  static String noInternetIc = 'assets/icons/ic_noInternet.svg';

}
