import 'package:flutter/material.dart';
import 'package:pass_the_dietitian_exam/utility/ui/color_pallete.dart';
import 'package:pass_the_dietitian_exam/utility/ui/styles.dart';

class CustomButton {
  static normalButton({title, onTap, backgroundColor, txtColor,fontSize}) {
    return GestureDetector(
      onTap: onTap ?? (){},
      child: Container(
        height: 50,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8.0),
          color:backgroundColor ?? themeColor,
        ),
        child: Center(
          child: Text(
              title,
            style: textStyles.semiBold(
                fontSize: fontSize ?? 18.0, fontColor: txtColor ?? whiteColor),
          ),
        ),
      ),
    );
  }
}
