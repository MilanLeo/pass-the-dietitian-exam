import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pass_the_dietitian_exam/utility/ui/color_pallete.dart';
import 'package:pass_the_dietitian_exam/utility/ui/styles.dart';
import 'package:pass_the_dietitian_exam/utility/ui/customs/validations.dart';

class CustomTextField extends StatefulWidget {
  String? hintText;
  String? prefixText;
  bool? obscureText = false, enabled;
  bool? enabledTextField = true;
  TextInputType? textInputType;
  int? maxLength;
  int? minLength;
  int? maxLines = 1;
  TextInputAction? textInputAction;
  String? error;
  TextEditingController? controller;
  ValidateTypes? validateTypes;
  ValidateState? validateState;
  Widget? suffixIcon;
  String? suffixText;
  bool? isVerifyButton;
  bool? isVerify;
  bool? showCounterText;
  List<TextInputFormatter>? inputFormat;
  Function(String)? onCountText;
  bool? isCounterCallback;
  int? counter;
  String? labelText;
  Function? applyPromo;
  String? errorMsg;
  FocusNode? focusNext;
  FocusNode? focusNode;
  bool? isOptional = false;
  int? minLines;
  void Function(String)? onChange;
  TextCapitalization? textCapitalization;
  Widget? prefix;
  Color? fillColor;
  bool? showBorder = true;
  Color? borderColor;
  bool? showBoxShadow = true;
  bool? decoration;


  CustomTextField({
    Key? key,
    this.maxLength,
    this.minLength,
    this.error,
    this.textInputAction,
    this.maxLines,
    @required this.hintText,
    this.obscureText = false,
    this.enabled,
    this.textInputType,
    @required this.controller,
    this.validateTypes,
    this.validateState,
    this.suffixIcon,
    this.suffixText,
    this.enabledTextField,
    this.isVerifyButton,
    this.isVerify,
    this.inputFormat,
    this.onCountText,
    this.isCounterCallback = false,
    this.showCounterText = false,
    this.counter,
    this.applyPromo,
    this.labelText,
    this.errorMsg,
    this.focusNext,
    this.focusNode,
    this.isOptional = false,
    this.minLines,
    this.prefixText = "",
    this.onChange,
    this.textCapitalization,
    this.fillColor,
    this.prefix,
    this.showBorder = true,
    this.borderColor,
    this.showBoxShadow = true,
    this.decoration,
  }): super(key: key);

  @override
  CustomTextFieldState createState() => CustomTextFieldState();
}

class CustomTextFieldState extends State<CustomTextField> {
  FocusNode? _focusNode;
  bool isFocus = false;
  String? hintText;
  bool? enabled;
  TextInputType? textInputType;
  int? maxLength;
  int? maxLines;

  TextInputAction? textInputAction;
  TextEditingController? controller;
  ValidateTypes? validateTypes;
  bool? isVerifyButton;
  bool? isVerify;
  String? errorMsg;
  FocusNode? focusNext;
  bool? isOptional;
  String? text = "";
  ValidateState? validateState;
  String? error;
  int? counter;


  @override
  void initState() {
    super.initState();
    if (widget.focusNode == null) {
      _focusNode = FocusNode();
    } else {
      _focusNode = widget.focusNode;
      _focusNode!.addListener(() {
        setState(() {
          isFocus = _focusNode!.hasFocus;
        });
        if (!_focusNode!.hasFocus) {
          checkValidation(_focusNode!.hasFocus);
        }
      });
    }
    if (widget.controller != null) {
      widget.controller!.selection = TextSelection.fromPosition(
          TextPosition(offset: widget.controller!.text.length));
    }
    widget.enabled ??= true;
    widget.maxLines ??= 1;
    widget.textInputAction ??= TextInputAction.next;
    widget.error ??= "";
    widget.validateTypes ??= ValidateTypes.empty;
    widget.validateState ??= ValidateState.initial;
    widget.isVerifyButton ??= false;
    widget.decoration ??=true;
    widget.isVerify ??= false;
    hintText = widget.hintText;
    enabled = widget.enabled;
    textInputType = widget.textInputType;
    maxLength = widget.maxLength;
    maxLines = widget.maxLines;
    textInputAction = widget.textInputAction;
    error = widget.error;
    controller = widget.controller;
    validateTypes = widget.validateTypes;
    validateState = widget.validateState;
    isVerifyButton = widget.isVerifyButton;
    focusNext = widget.focusNext;
    isOptional = widget.isOptional;
    isVerify = widget.isVerify;
    if (controller != null && controller!.text.isNotEmpty) {
      checkValidation(true);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8.0), color: textBgColor),
          padding: const EdgeInsets.only(left: 20),
          child: TextField(
            enabled: widget.enabledTextField ?? true,
            textInputAction: textInputAction,
            keyboardType: widget.textInputType,
            maxLength: widget.maxLength,
            maxLines: widget.maxLines ?? 1,
            obscureText: widget.obscureText ?? false,
            onChanged: ((val) {
              if (widget.onChange != null) {
                widget.onChange!(val);
              }
              checkValidation(isFocus);
            }),
            style:
                textStyles.medium(fontSize: 16.0, fontColor: blackColor),
            decoration: InputDecoration(
              hintText: widget.hintText,
              border: InputBorder.none,
              suffixIcon: widget.suffixIcon,
              prefixText: widget.prefixText??"",
              prefixStyle: textStyles.medium(
                  fontSize: 14.0, fontColor: Colors.white30),
              hintStyle: textStyles.normal(
                  fontSize: 14.0, fontColor: placeHolderColor),
              counterText: "",
              enabledBorder: InputBorder.none,
              focusedBorder: InputBorder.none,
            ),
            controller: controller,
          ),
        ),
        if (validateState == ValidateState.inValidate && error!.isNotEmpty)
          Padding(
            padding: const EdgeInsets.only(top: 5),
            child: Row(
              children: [
                Container(
                  decoration: const BoxDecoration(
                      shape: BoxShape.circle, color: errorColor),
                  child: const Icon(
                    Icons.clear,
                    color: Colors.white,
                    size: 12.0,
                  ),
                ),
                const SizedBox(width: 5,),
                Text(
                  error!,
                  style: textStyles.normal(
                      fontSize: 12.0, fontColor: errorColor),
                )
              ],
            ),
          )
      ],
    );
  }

  bool checkValidation([bool isFocus = false]) {
    String errorText = "";
    var isCheck = true;
    if (isOptional! && controller!.text.isEmpty) {
      isCheck = false;
    }
    if (isCheck) {
      switch (validateTypes!) {
        case ValidateTypes.amount:
          {
            errorText = Validations.validateAmount(
                controller!.text,
                widget.errorMsg == null
                    ? hintText.toString()
                    : widget.errorMsg.toString());
            break;
          }
        case ValidateTypes.name:
          {
            errorText = Validations.validateName(
                controller!.text,
                widget.errorMsg == null
                    ? hintText.toString()
                    : widget.errorMsg.toString());
            break;
          }
        case ValidateTypes.empty:
          {
            errorText = Validations.validateEmpty(
                controller!.text,
                widget.errorMsg == null
                    ? hintText.toString()
                    : widget.errorMsg.toString());
            break;
          }
        case ValidateTypes.email:
          {
            errorText = Validations.validateEmail(
                controller!.text,
                widget.errorMsg == null
                    ? hintText.toString()
                    : widget.errorMsg.toString());
            break;
          }
        case ValidateTypes.mobile:
          {
            errorText = Validations.validateMobile(
                controller!.text,
                widget.errorMsg == null
                    ? hintText.toString()
                    : widget.errorMsg.toString());
            break;
          }
        case ValidateTypes.userName:
          {
            errorText = Validations.validateUserName(
                controller!.text,
                widget.errorMsg == null
                    ? hintText.toString()
                    : widget.errorMsg.toString());
            break;
          }
        case ValidateTypes.password:
          {
            errorText = Validations.validatePassword(
                controller!.text,
                widget.errorMsg == null
                    ? hintText.toString()
                    : widget.errorMsg.toString());
            break;
          }
        case ValidateTypes.cvv:
          {
            errorText = Validations.validateCVV(
                controller!.text,
                widget.errorMsg == null
                    ? hintText.toString()
                    : widget.errorMsg.toString());
            break;
          }
        case ValidateTypes.pan:
          // TODO: Handle this case.
          break;

      }
    }

    if (errorText.isNotEmpty) {
      if (validateState != ValidateState.inValidate) {
        setState(() {
          validateState = ValidateState.inValidate;
        });
      }
      if (!isFocus) {
        setState(() {
          error = errorText;
        });
      } else if (error!.isNotEmpty) {
        setState(() {
          error = "";
        });
      }
      return true;
    } else {
      if (validateState != ValidateState.validate) {
        setState(() {
          error = "";
          validateState = ValidateState.validate;
        });
      }
      return false;
    }
  }

  void setError(String errorText) {
    if (validateState != ValidateState.inValidate) {
      setState(() {
        validateState = ValidateState.inValidate;
      });
    }
    setState(() {
      error = errorText;
    });
  }
}

class UpperCaseTextFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    debugPrint(newValue.text);
    return TextEditingValue(
      text: newValue.text.toUpperCase().toString(),
      selection: newValue.selection,
    );
  }
}

enum ValidateTypes {
  email,
  mobile,
  password,
  name,
  empty,
  userName,
  cvv,
  pan,
  amount,
}

enum ValidateState { initial, validate, inValidate }
