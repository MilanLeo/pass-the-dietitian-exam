import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:in_app_purchase/in_app_purchase.dart';
import 'package:pass_the_dietitian_exam/utility/Impl/global.dart';
import 'package:pass_the_dietitian_exam/utility/Impl/session_impl.dart';
import 'package:pass_the_dietitian_exam/utility/endpoint/endpoint.dart';
import 'package:pass_the_dietitian_exam/utility/ui/color_pallete.dart';
import 'package:pass_the_dietitian_exam/utility/ui/widgets.dart';
import 'package:pass_the_dietitian_exam/views/DashboardView.dart';

class InAppPurchaseUtils extends GetxController {

  InAppPurchaseUtils._();

  static final InAppPurchaseUtils _instance = InAppPurchaseUtils._();

  static InAppPurchaseUtils get inAppPurchaseUtilsInstance => _instance;

  final InAppPurchase _iap = InAppPurchase.instance;

  late StreamSubscription<List<PurchaseDetails>> _purchasesSubscription;

  List<ProductDetails> _products = [];

  var planPrice;

  @override
  void onInit() {
    super.onInit();
    initialize();
  }

  @override
  void onClose() {
    _purchasesSubscription.cancel();
    super.onClose();
  }

  Future<void> initialize() async {
    print("in app init");
    if(!(await _iap.isAvailable())) return;
    _purchasesSubscription = InAppPurchase.instance.purchaseStream.listen((List<PurchaseDetails> purchaseDetailsList) {
      print("start listening");
      handlePurchaseUpdates(purchaseDetailsList);
    },
      onDone: () {
        _purchasesSubscription.cancel();
        print("done");
      },
      onError: (error) {
        print("init error");
      },
    );
  }

  void handlePurchaseUpdates(List<PurchaseDetails> purchaseDetailsList) {
    for (var purchaseDetails in purchaseDetailsList) {
      if (purchaseDetails.status == PurchaseStatus.pending) {
        hideAppLoader();
      } else {
        if (purchaseDetails.status == PurchaseStatus.error) {
          hideAppLoader();
        } else if (purchaseDetails.status == PurchaseStatus.purchased ||
            purchaseDetails.status == PurchaseStatus.restored) {
          hideAppLoader();
          print("purchase done");
          if(uc.isPurchase) {
            print("isUserPurchase");
            String transactionId = purchaseDetails.purchaseID ?? 'N/A';
            SessionImpl.setData(SessionKeys.paymentSuccess, 1);
            SessionImpl.setData(SessionKeys.transId, transactionId);
            SessionImpl.setData(SessionKeys.planPrice, planPrice);
            uc.isSubscribe.value = 1;
            createIOSPayment(transactionId);
          }
        }
        if (purchaseDetails.pendingCompletePurchase) {
          print("complete purchase called");
          _iap.completePurchase(purchaseDetails);
        }
      }
    }
  }

  Future<void> buySubscriptionPlan(price) async {
    planPrice = price;
    try {
      Set<String> productIds = {"com.app.passexam_lifetime"};
      final ProductDetailsResponse response = await _iap.queryProductDetails(productIds);
      if (response.error != null) {
        hideAppLoader();
        return;
      } else {
        _products = response.productDetails;
        hideAppLoader();
      }
      final PurchaseParam purchaseParam = PurchaseParam(productDetails: _products.first);
      await _iap.buyConsumable(purchaseParam: purchaseParam,autoConsume: true);
    } catch (e) {
      hideAppLoader();
      print('Failed to buy plan: $e');
    }
  }

  createIOSPayment(String? transactionId) {
    var body = {
      "transactionId": transactionId,
      "price": planPrice,
      "status": "complete",
    };
    RequestManager.postRequest(
      uri: EndPoints.createIOSPayment,
      hasBearer: true,
      body: body,
      onStatusSuccess: (responseBody) {
        hideAppLoader();
        uc.isPurchase = false;
        var res = jsonDecode(responseBody);
        if (res["status"] == 1) {
          uc.isFirstTime = true;
          RequestManager.getSnackToast(
              title: "Success",
              messageText: Text(res["message"],
                  style: const TextStyle(
                    fontSize: dimen.textMedium,
                    color: Colors.white,
                  )),
              colorText: whiteColor,
              backgroundColor: themeColor,
              onDismissed: () {
                Get.offAll(() => const Dashboard());
              });
        } else {
          Widgets.showSnackBar(title: "Error", errorMessage: res["message"]);
        }
      },
      onFailure: (error) {
        hideAppLoader();
      },
    );
  }

}