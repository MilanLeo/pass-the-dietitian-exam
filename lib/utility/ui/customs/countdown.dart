import 'package:flutter/material.dart';
import 'package:pass_the_dietitian_exam/utility/Impl/global.dart';

class CountDown extends AnimatedWidget {
  CountDown(
      {Key? key,
      this.animation,
      this.onTimeOver,
      this.textColor,
      this.fontWeight = FontWeight.normal})
      : super(key: key, listenable: animation!);

  final Animation<int>? animation;
  final Function(dynamic responseBody)? onTimeOver;
  final Color? textColor;
  final fontWeight;

  @override
  build(BuildContext context) {
    Duration duration = Duration(seconds: animation!.value);
    var mins = 60;

    var minute = duration.inMinutes.remainder(mins).toString();
    if (minute.length == 1) {
      minute = '0' + minute;
    }
    var timerText =
        '$minute:${duration.inSeconds.remainder(mins).toString().padLeft(2, '0')}';

    var timer = '$timerText';

    timerText = '(Resend Code in $timerText)';

    if (timer == '00:00') {
      timerText = 'Resend Code';
      onTimeOver!(timerText);
    }
    print(timerText);

    // print('animation.value  ${animation.value} ');
    // print('inMinutes ${duration.inMinutes.toString()}');
    // print('inSeconds ${duration.inSeconds.toString()}');
    // print('inSeconds.remainder ${duration.inSeconds.remainder(60).toString()}');

    return Text(
      '$timerText',
      style: TextStyle(
        color: textColor,
        fontWeight: fontWeight,
        fontFamily: 'Proxima',
        fontSize: dimen.textLarge,
      ),
    );
  }
}
