import 'dart:typed_data';
import 'dart:ui';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:loading_indicator/loading_indicator.dart';
import 'package:pass_the_dietitian_exam/utility/Impl/global.dart';
import 'package:pass_the_dietitian_exam/utility/ui/color_pallete.dart';
import 'package:pass_the_dietitian_exam/utility/ui/images.dart';
import 'package:pass_the_dietitian_exam/utility/ui/styles.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pass_the_dietitian_exam/views/BuySubscription.dart';
import 'package:share_plus/share_plus.dart';

abstract class Widgets {
  static bool e = false;

  static RxDouble totalPrice = 0.0.obs;

  static showAlertDialog(BuildContext context) {
    Widget cancelButton = TextButton(
      child: Text("NO",style: textStyles.semiBold(fontSize: 16.0,fontColor: themeColor)),
      onPressed:  () {
        Get.back();
      },
    );
    Widget continueButton = TextButton(
      child: Text("YES",style: textStyles.semiBold(fontSize: 16.0,fontColor: themeColor)),
      onPressed:  () {
        Get.back();
        Get.to(()=> const BuySubscription(),arguments: "Buy The Plan");
      },
    );
    AlertDialog alert = AlertDialog(
      // title: const Text(""),
      content: Text("To access the paid version,\nRequest you to purchase.",style: textStyles.medium(fontSize: 18.0,fontColor: blackColor)),
      actions: [
        cancelButton,
        continueButton,
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  static imageUploadOptionSheet(
      {Function? onPressTitle1,
      Function? onPressTitle2,
      String? title1,
      String? title2}) {
    return Get.bottomSheet(
        Container(
          decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(16),
                  topRight: Radius.circular(16))),
          child: SafeArea(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.only(
                      top: dimen.pad16, bottom: dimen.pad16),
                  child: Text(
                    "Choose option",
                    style: textStyles.bold(fontSize: 20.0, fontColor: blackColor),
                  ),
                ),
                Column(children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10),
                    child: InkWell(
                      onTap: () {
                        if (onPressTitle1 != null) {
                          onPressTitle1();
                        }
                      },
                      child: Text(
                        title1 ?? "Camera",
                        style: textStyles.medium(
                            fontSize: 18.0, fontColor: blackColor),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10),
                    child: InkWell(
                      onTap: () {
                        if (onPressTitle2 != null) {
                          onPressTitle2();
                        }
                      },
                      child: Text(
                        title2 ?? "Gallery",
                        style: textStyles.medium(
                            fontSize: 18.0, fontColor: blackColor),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ]),
              ],
            ),
          ),
        ),
        isDismissible: true,
        enableDrag: true,
        isScrollControlled: false);
  }

  static dataNotFound() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SvgPicture.asset(SvgImages.dataNotFoundIc),
        Padding(
          padding: const EdgeInsets.all(20.0),
          child: Text(
            "No Data Found",
            style: textStyles.bold(fontColor: themeColor, fontSize: 16.0),
          ),
        ),
      ],
    );
  }

  static share(context, {shareText, subject = 'Pass The Dietitian Exam'}) async {
    // assert(context != null);
    // final RenderBox box = context.findRenderObject() as RenderBox;
    return await Share.share(
      '$shareText',
      subject: '$subject',
      // sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size,
    );
  }

  static showSnackBar({title, errorMessage, position}) {
    return Get.snackbar('', '',
        titleText: Text(
          title,
          style: const TextStyle(
            fontSize: dimen.textLarge,
            fontWeight: FontWeight.bold,
            color: Colors.white,
          ),
        ),
        messageText: Text(
          errorMessage,
          style: const TextStyle(
            fontSize: dimen.textMedium,
            color: Colors.white,
          ),
        ),
        backgroundColor: title == 'Error' ? Colors.red : themeColor,
        snackPosition: position == null ? SnackPosition.TOP : SnackPosition.BOTTOM);
  }

  static Widget commonNetworkImage({
    String? networkImage,
    double? width,
    double? height,
  }) {
    return ClipRRect(
      borderRadius: const BorderRadius.all(Radius.circular(16)),
      child: CachedNetworkImage(
        height: height,
        width: width,
        fadeInDuration: const Duration(milliseconds: 250),
        imageUrl: networkImage!,
        errorWidget: (context, url, error) => Opacity(
          opacity: 0.9,
          child: Image.asset(
            ImageResource.profileIc,
          ),
        ),
        fit: BoxFit.cover,
        placeholder: ((context, String d) => Center(
              child: Image.asset(
                ImageResource.profileIc,
              ),
            )),
      ),
    );
  }
}

Widget heroWrap({tag, isProfile = false, child}) {
  if (isProfile) {
    return Hero(tag: tag, child: child);
  } else {
    return Container(child: child);
  }
}

emptyWidget() {
  return const SizedBox.shrink();
}

appGradient({double opacity = 1.0}) {
  // return LinearGradient(colors: [Colors.red, Colors.blue]);

  return LinearGradient(colors: [
    const Color(0xff8B64E8).withOpacity(opacity),
    const Color(0xff263E8E).withOpacity(opacity),
    const Color(0xff187DB0).withOpacity(opacity),
  ]);

  // Color(0xff7146DD),
  // Color(0xff212D80),
  // Color(0xff18649E),
}

appFieldHeaderGradient({double opacity = 1.0}) {
  // return LinearGradient(colors: [Colors.red, Colors.blue]);

  return LinearGradient(colors: [
    const Color(0xff8B64E8).withOpacity(opacity),
    // Color(0xff263E8E).withOpacity(opacity),
    const Color(0xff187DB0).withOpacity(opacity),
  ]);
}

appGradientDisable() {
  return const LinearGradient(colors: [
    Color(0xffEDECFA),
    Color(0xffEDECFA), //0xffEDECFA
  ]);
}

hideAppLoader() {
  if (Get.isDialogOpen!) {
    // Future.delayed(Duration(milliseconds: 250), () {
    if (Get.isSnackbarOpen) {
      Get.back();
    }
    Get.back();
    // });
  }
}

hideDialog() {
  if (Get.isDialogOpen!) {
    Get.back();
  }
}

apiLoader({
  loadingText = "Loading...",
  Indicator indicator = Indicator
      .ballScaleMultiple, //.ballRotateChase, //ballTrianglePathColoredFilled, //ballPulseSync
  bool showPathBackground = false,
  Function? asyncCall,
  asyncDuration = const Duration(seconds: 1),
}) {
  Future.delayed(const Duration(milliseconds: 250), () {
    Get.dialog(
      Scaffold(
        backgroundColor: Colors.transparent,
        body: Padding(
          padding: const EdgeInsets.symmetric(vertical: 120, horizontal: 100),
          child: Center(
            child: Container(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 25),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: Colors.transparent, //Colors.black87,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  LoadingIndicator(
                    // backgroundColor: Colors.black87,
                    indicatorType: indicator,
                    colors: _kDefaultRainbowColors,
                    strokeWidth: 4.0,
                    pathBackgroundColor:
                        showPathBackground ? Colors.black45 : null,
                  ),
                  const SizedBox(height: 8),
                  Flexible(
                    child: Text('$loadingText',
                        style: TextStyle(
                            color: inverseBWColor,
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold)),
                  ),
                  const SizedBox(height: 8),
                ],
              ),
            ),
          ),
        ),
      ),
      useSafeArea: true,
      barrierDismissible: true,
    );
    Future.delayed(asyncDuration, () {
      if (asyncCall != null) asyncCall();
    });
  });
}

const List<Color> _kDefaultRainbowColors = const [
  Color(0xff2C6753),
  Color(0xffECDAE0),
  Color(0xffF1BABF),
];

appLoader({
  loadingText = "Loading...",
  Indicator indicator = Indicator.ballScaleMultiple,
  bool showPathBackground = false,
}) {
  return Container(
    color: Colors.black54,
    child: Padding(
      padding: const EdgeInsets.all(120),
      child: Center(
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 25),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: Colors.transparent), //Colors.black87),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              LoadingIndicator(
                // backgroundColor: Colors.black87,
                indicatorType: indicator,
                colors: _kDefaultRainbowColors,
                strokeWidth: 4.0,
                pathBackgroundColor: showPathBackground ? Colors.black45 : null,
              ),
              const SizedBox(height: 8),
              Text('$loadingText',
                  style: TextStyle(
                      color: inverseBWColor,
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold)),
              const SizedBox(height: 8),
            ],
          ),
        ),
      ),
    ),
  );

  //return Center(child: CircularProgressIndicator());
}


inAppUpdate() async {}

String formatHHMMSS(int value) {
  if (value > 0) {
    int h, m, s;
    h = value ~/ 3600;
    m = ((value - h * 3600)) ~/ 60;
    s = value - (h * 3600) - (m * 60);
    String hourLeft = h.toString().length < 2 ? "0" + h.toString() : h.toString();
    String minuteLeft =
    m.toString().length < 2 ? "0" + m.toString() : m.toString();
    String secondsLeft =
    s.toString().length < 2 ? "0" + s.toString() : s.toString();
    String result = "$hourLeft:$minuteLeft:$secondsLeft";
    return result;
  } else {
    return "";
  }
}

final Uint8List kTransparentImage = new Uint8List.fromList(<int>[
  0x89,
  0x50,
  0x4E,
  0x47,
  0x0D,
  0x0A,
  0x1A,
  0x0A,
  0x00,
  0x00,
  0x00,
  0x0D,
  0x49,
  0x48,
  0x44,
  0x52,
  0x00,
  0x00,
  0x00,
  0x01,
  0x00,
  0x00,
  0x00,
  0x01,
  0x08,
  0x06,
  0x00,
  0x00,
  0x00,
  0x1F,
  0x15,
  0xC4,
  0x89,
  0x00,
  0x00,
  0x00,
  0x0A,
  0x49,
  0x44,
  0x41,
  0x54,
  0x78,
  0x9C,
  0x63,
  0x00,
  0x01,
  0x00,
  0x00,
  0x05,
  0x00,
  0x01,
  0x0D,
  0x0A,
  0x2D,
  0xB4,
  0x00,
  0x00,
  0x00,
  0x00,
  0x49,
  0x45,
  0x4E,
  0x44,
  0xAE,
]);
