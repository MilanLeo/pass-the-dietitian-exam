import 'package:flutter/material.dart';
import 'package:pass_the_dietitian_exam/utility/Impl/global.dart';
import 'package:pass_the_dietitian_exam/utility/ui/color_pallete.dart';

class Styles {
  static const String FontNameRegular = 'Poppins-Regular';
  static const String FontNameMedium = 'Poppins-Medium';
  static const String FontNameBold = 'Poppins-Bold';
  static const String FontNameSemiBold = 'Poppins-SemiBold';

  static get raisedButtonShape => RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(18.0),
      side: BorderSide(color: Colors.blueGrey));

  static get elevatedButtonStyle =>
      ElevatedButton.styleFrom(shape: raisedButtonShape);

  static get defaultTextStyle =>
      TextStyle(fontSize: 14.0, color: Color(0xff111111));
  static get textStyleMedium =>
      TextStyle(fontSize: 16.0, color: Color(0xff111111));
  static get textStyleLarge => TextStyle(fontSize: 20.0);
  static get textStyleLargeWhite =>
      TextStyle(fontSize: 25.0, color: Color(0xffffffff));

  static lightTheme() {
    // assert(context != null);

    return ThemeData(
      fontFamily: 'Poppins',
      primaryColor: Colors.black,
      primaryColorDark: Colors.black87,
      primaryColorLight: Colors.white,
      primarySwatch: primarySwatchOrg,
      visualDensity: VisualDensity.adaptivePlatformDensity,
      brightness: Brightness.light,
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      textTheme: ThemeData(brightness: Brightness.light, fontFamily: 'Poppins')
          .textTheme,
    );
  }

  static darkTheme() {
    // assert(context != null);

    return ThemeData(
      fontFamily: 'Poppins',
      primaryColor: Colors.white,
      primaryColorDark: Colors.white70,
      primaryColorLight: Colors.black45,
      primarySwatch: primarySwatchOrg,
      visualDensity: VisualDensity.adaptivePlatformDensity,
      brightness: Brightness.dark,
      textTheme: ThemeData(brightness: Brightness.dark, fontFamily: 'Poppins')
          .textTheme,
    );
  }

  static get elevatedCircleShape => RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(18.0),
      side: BorderSide(color: primaryColor));

  static get elevatedRectShape => RoundedRectangleBorder(
      borderRadius: BorderRadius.all(Radius.elliptical(5.0, 5.0)),
      side: BorderSide(color: primaryColor));

  static textShadow() {
    var offset = 3.0;
    return <Shadow>[
      Shadow(
        offset: Offset(offset, offset),
        blurRadius: 8.0,
        color: Colors.black54,
      ),
      Shadow(
        offset: Offset(offset, offset),
        blurRadius: 8.0,
        color: Colors.black,
      ),
    ];
  }

  static outlineBorder({color, double? width}) {
    return OutlineInputBorder(
      borderSide: BorderSide(color: color, width: width!),
    );
  }
}

// ignore: camel_case_types
class textStyles {
  static normal({fontSize = dimen.textNormal, fontColor = Colors.black}) {
    return TextStyle(
        color: fontColor, fontFamily: Styles.FontNameRegular, fontSize: fontSize);
  }

  static semiBold({fontSize = dimen.textNormal, fontColor = Colors.black}) {
    return TextStyle(
        color: fontColor, fontFamily: Styles.FontNameSemiBold, fontSize: fontSize);
  }

  static bold({fontSize = dimen.textNormal, fontColor = Colors.black}) {
    return TextStyle(
        color: fontColor, fontFamily: Styles.FontNameBold, fontSize: fontSize);
  }

  static medium({fontSize = dimen.textNormal, fontColor = Colors.black}) {
    return TextStyle(
        color: fontColor, fontFamily: Styles.FontNameMedium, fontSize: fontSize);
  }
}
