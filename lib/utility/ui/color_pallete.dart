import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pass_the_dietitian_exam/utility/Impl/helper.dart';

get isDarkMode => (Get.isDarkMode || Get.isPlatformDarkMode);

const mainBg = const Color(0xffF5F5F5);
const themeColor = const Color(0xff2C6753);
const pinkColor = const Color(0xffFCE6ED);
const highetsColor = const Color(0xffCCF0E4);
const lowestColor = const Color(0xffFFCFCF);
const lightGreen = const Color(0xff899494);
const placeHolder = const Color(0xffD0D4D4);
const buttonDisable = const Color(0xffF1F1F1);
const grayColor = const Color(0xff808080);
const lightGrayColor = const Color(0x50000000);
const textBgColor = const Color(0x10000000);
const placeHolderColor = const Color(0x20000000);
const redColor = const Color(0xffE4002C);
const brownColor = const Color(0xffCF9F6E);
const themeDisable = const Color(0xffC0D1CB);
const deviderColor = const Color(0xffEAF0EE);
const lightBrownColor = const Color(0x80CF9F6E);
const lightYellowColor = const Color(0xffF8F1E9);
const eyeColor = const Color(0xff737373);
const blackColor = const Color(0xff000000);
const settingBgColor = const Color(0xffF2F2F2);
const greyColor = const Color(0x30000000);
const blueColor = const Color(0xff917EF7);
const darkBlue = const Color(0xff007AFF);
const borderColor = const Color(0xffCCCCCC);
const darkGray = const Color(0xffB2B2B2);
const greenColor = const Color(0xff1EBEA5);
const hintColor = const Color(0xff47456D);
const disableColor = const Color(0x40C4C4C4);
const whiteColor = const Color(0xffFFFFFF);


get systemBg => isDarkMode ? Color(0xff303030) : Color(0xffFFFFFF);
const errorColor = const Color(0xffFF4A40);
const borderColorGray = const Color(0xffDDDDDD);
const textFieldOutlineBorderColor = const Color(0xffE9E5E5);
const textFieldFillColor = const Color(0xffFAFAFA);
const colorDarkGrey = const Color(0xff012A36);
const primaryColor = Colors.blueGrey;
MaterialColor primarySwatchOrg = Helper().createMaterialColor(Color(0xff185293));
get disableText => isDarkMode ? inactiveColor : Color(0xffc8cde3);
const inactiveColor = const Color(0xff707070);
const colorGreyBorder = const Color(0xffd6d6d6);
const colorOnPrimary = const Color(0xffFFFFFF);
const colorOnBackgroundLight = const Color(0xff979797);
get textColorBlack => isDarkMode ? Color(0xffFFFFFF) : Color(0xff303030);
get sysBlue => isDarkMode ? Color(0xff6699FF) : Color(0xff185293);
const sysLight = const Color(0xffC7D9E7); //Color(0xffCECDF3);
get bottomSheetBackgroundColor => isDarkMode ? Colors.grey[800] : Colors.white;
get inverseBWColor => isDarkMode ? Colors.white : Colors.black;

var boxShadowCard = [
  const BoxShadow(
    spreadRadius: 0.8,
    blurRadius: 1.5,
    color: Colors.black12,
    // offset: Offset(0, 0),
  ),
];
