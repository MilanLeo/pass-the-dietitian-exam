// To parse this JSON data, do
//
//     final quizHistoryModel = quizHistoryModelFromJson(jsonString);

import 'dart:convert';

QuizHistoryModel quizHistoryModelFromJson(String str) => QuizHistoryModel.fromJson(json.decode(str));

String quizHistoryModelToJson(QuizHistoryModel data) => json.encode(data.toJson());

class QuizHistoryModel {
  int? status;
  List<Result>? result;
  String? message;

  QuizHistoryModel({
    this.status,
    this.result,
    this.message,
  });

  factory QuizHistoryModel.fromJson(Map<String, dynamic> json) => QuizHistoryModel(
    status: json["status"],
    result: json["result"] == null ? [] : List<Result>.from(json["result"]!.map((x) => Result.fromJson(x))),
    message: json["message"],
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "result": result == null ? [] : List<dynamic>.from(result!.map((x) => x.toJson())),
    "message": message,
  };
}

class Result {
  String? uniqueCheck;
  int? id;
  String? title;
  String? resultBarColor;
  var percentage;
  String? createdAt;

  Result({
    this.uniqueCheck,
    this.id,
    this.title,
    this.resultBarColor,
    this.percentage,
    this.createdAt,
  });

  factory Result.fromJson(Map<String, dynamic> json) => Result(
    uniqueCheck: json["uniqueCheck"],
    id: json["id"],
    title: json["title"],
    resultBarColor: json["resultBarColor"],
    percentage: json["percentage"],
    createdAt: json["createdAt"],
  );

  Map<String, dynamic> toJson() => {
    "uniqueCheck": uniqueCheck,
    "id": id,
    "title": title,
    "resultBarColor": resultBarColor,
    "percentage": percentage,
    "createdAt": createdAt,
  };
}
