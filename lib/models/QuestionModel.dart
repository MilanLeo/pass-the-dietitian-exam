// To parse this JSON data, do
//
//     final questionModel = questionModelFromJson(jsonString);

import 'dart:convert';

import 'package:get/get.dart';
import 'package:pass_the_dietitian_exam/utility/ui/color_pallete.dart';

QuestionModel questionModelFromJson(String str) => QuestionModel.fromJson(json.decode(str));

String questionModelToJson(QuestionModel data) => json.encode(data.toJson());

class QuestionModel {
  QuestionModel({
    this.status,
    this.result,
    this.message,
  });

  int? status;
  Result? result;
  String? message;

  factory QuestionModel.fromJson(Map<String, dynamic> json) => QuestionModel(
    status: json["status"] == null ? null : json["status"],
    result: json["result"] == null ? null : Result.fromJson(json["result"]),
    message: json["message"] == null ? null : json["message"],
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "result": result == null ? null : result!.toJson(),
    "message": message == null ? null : message,
  };
}

class Result {
  Result({
    this.questionList,
    this.lastQuestionId,
    this.examRemainingTime,
  });

  List<QuestionList>? questionList;
  int? lastQuestionId;
  int? examRemainingTime;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
    examRemainingTime: json["examRemainingTime"] == null ? null : json["examRemainingTime"],
    lastQuestionId: json["lastQuestionId"] == null ? null : json["lastQuestionId"],
    questionList: json["questionList"] == null ? null : List<QuestionList>.from(json["questionList"].map((x) => QuestionList.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "lastQuestionId": lastQuestionId == null ? null : lastQuestionId,
    "examRemainingTime": examRemainingTime == null ? null : examRemainingTime,
    "questionList": questionList == null ? null : List<dynamic>.from(questionList!.map((x) => x.toJson())),
  };
}

class QuestionList {
  QuestionList({
    this.id,
    this.topicId,
    this.questions,
    this.answerDescription,
    this.answerOptions,
  });

  var id;
  var topicId;
  var questions;
  var answerDescription;
  List<AnswerOption>? answerOptions;

  factory QuestionList.fromJson(Map<String, dynamic> json) => QuestionList(
    id: json["id"] == null ? null : json["id"],
    topicId: json["topicId"] == null ? null : json["topicId"],
    questions: json["questions"] == null ? null : json["questions"],
    answerDescription: json["answerDescription"] == null ? null : json["answerDescription"],
    answerOptions: json["answerOptions"] == null ? null : List<AnswerOption>.from(json["answerOptions"].map((x) => AnswerOption.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "topicId": topicId == null ? null : topicId,
    "questions": questions == null ? null : questions,
    "answerDescription": answerDescription == null ? null : answerDescription,
    "answerOptions": answerOptions == null ? null : List<dynamic>.from(answerOptions!.map((x) => x.toJson())),
  };
}

class AnswerOption {
  AnswerOption({
    this.id,
    this.questionId,
    this.answer,
    this.isCorrect,
    this.position,
  });

  var id;
  var questionId;
  var answer;
  var isCorrect;
  var position;
  RxBool isSelected = false.obs;
  var color = whiteColor.obs;

  factory AnswerOption.fromJson(Map<String, dynamic> json) => AnswerOption(
    id: json["id"] == null ? null : json["id"],
    questionId: json["questionId"] == null ? null : json["questionId"],
    answer: json["answer"] == null ? null : json["answer"],
    isCorrect: json["isCorrect"] == null ? null : json["isCorrect"],
    position: json["position"] == null ? null : json["position"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "questionId": questionId == null ? null : questionId,
    "answer": answer == null ? null : answer,
    "isCorrect": isCorrect == null ? null : isCorrect,
    "position": position == null ? null : position,
  };
}
