// To parse this JSON data, do
//
//     final userModel = userModelFromJson(jsonString);
import 'dart:convert';

UserModel userModelFromJson(String str) => UserModel.fromJson(json.decode(str));

String userModelToJson(UserModel data) => json.encode(data.toJson());

class UserModel {
  UserModel({
    this.status,
    this.result,
    this.message,
  });

  var status;
  Result? result;
  var message;

  factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
    status: json["status"] == null ? null : json["status"],
    result: json["result"] == null ? null : Result.fromJson(json["result"]),
    message: json["message"] == null ? null : json["message"],
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "result": result == null ? null : result!.toJson(),
    "message": message == null ? null : message,
  };
}

class Result {
  Result({
    this.userDetails,
    this.accessToken,
  });

  UserDetails? userDetails;
  var accessToken;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
    userDetails: json["userDetails"] == null ? null : UserDetails.fromJson(json["userDetails"]),
    accessToken: json["accessToken"] == null ? null : json["accessToken"],
  );

  Map<String, dynamic> toJson() => {
    "userDetails": userDetails == null ? null : userDetails!.toJson(),
    "accessToken": accessToken == null ? null : accessToken,
  };
}
UserDetails userDetailsFromJson(String str) => UserDetails.fromJson(json.decode(str));
class UserDetails {
  UserDetails({
    this.id,
    this.roleId,
    this.deviceToken,
    this.firstName,
    this.lastName,
    this.fullName,
    this.countryCode,
    this.mobileNumber,
    this.countryCodeMobileNumber,
    this.email,
    this.emailverifiedAt,
    this.address,
    this.latitude,
    this.longitude,
    this.profileImage,
    this.otp,
    this.otpExpiryTime,
    this.isVerify,
    this.isActive,
    this.isNotification,
    this.createdAt,
    this.updatedAt,
    this.profileImageShortName,
    this.roleName,
  });

  var id;
  var roleId;
  var deviceToken;
  var firstName;
  var lastName;
  var fullName;
  var countryCode;
  var mobileNumber;
  var countryCodeMobileNumber;
  var email;
  var emailverifiedAt;
  var address;
  var latitude;
  var longitude;
  var profileImage;
  var otp;
  var otpExpiryTime;
  var isVerify;
  var isActive;
  var isNotification;
  var createdAt;
  var updatedAt;
  var profileImageShortName;
  var roleName;

  factory UserDetails.fromJson(Map<String, dynamic> json) => UserDetails(
    id: json["id"] == null ? null : json["id"],
    roleId: json["roleId"] == null ? null : json["roleId"],
    deviceToken: json["deviceToken"] == null ? null : json["deviceToken"],
    firstName: json["firstName"] == null ? null : json["firstName"],
    lastName: json["lastName"] == null ? null : json["lastName"],
    fullName: json["fullName"] == null ? null : json["fullName"],
    countryCode: json["countryCode"] == null ? null : json["countryCode"],
    mobileNumber: json["mobileNumber"] == null ? null : json["mobileNumber"],
    countryCodeMobileNumber: json["countryCodeMobileNumber"] == null ? null : json["countryCodeMobileNumber"],
    email: json["email"] == null ? null : json["email"],
    emailverifiedAt: json["emailverifiedAt"] == null ? null : json["emailverifiedAt"],
    address: json["address"] == null ? null : json["address"],
    latitude: json["latitude"] == null ? null : json["latitude"],
    longitude: json["longitude"] == null ? null : json["longitude"],
    profileImage: json["profileImage"] == null ? null : json["profileImage"],
    otp: json["otp"] == null ? null : json["otp"],
    otpExpiryTime: json["otpExpiryTime"] == null ? null : json["otpExpiryTime"],
    isVerify: json["isVerify"] == null ? null : json["isVerify"],
    isActive: json["isActive"] == null ? null : json["isActive"],
    isNotification: json["isNotification"] == null ? null : json["isNotification"],
    createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
    updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
    profileImageShortName: json["profileImageShortName"] == null ? null : json["profileImageShortName"],
    roleName: json["roleName"] == null ? null : json["roleName"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "roleId": roleId == null ? null : roleId,
    "deviceToken": deviceToken == null ? null : deviceToken,
    "firstName": firstName == null ? null : firstName,
    "lastName": lastName == null ? null : lastName,
    "fullName": fullName == null ? null : fullName,
    "countryCode": countryCode == null ? null : countryCode,
    "mobileNumber": mobileNumber == null ? null : mobileNumber,
    "countryCodeMobileNumber": countryCodeMobileNumber == null ? null : countryCodeMobileNumber,
    "email": email == null ? null : email,
    "emailverifiedAt": emailverifiedAt == null ? null : emailverifiedAt,
    "address": address == null ? null : address,
    "latitude": latitude == null ? null : latitude,
    "longitude": longitude == null ? null : longitude,
    "profileImage": profileImage == null ? null : profileImage,
    "otp": otp == null ? null : otp,
    "otpExpiryTime": otpExpiryTime == null ? null : otpExpiryTime,
    "isVerify": isVerify == null ? null : isVerify,
    "isActive": isActive == null ? null : isActive,
    "isNotification": isNotification == null ? null : isNotification,
    "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
    "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
    "profileImageShortName": profileImageShortName == null ? null : profileImageShortName,
    "roleName": roleName == null ? null : roleName,
  };
}
