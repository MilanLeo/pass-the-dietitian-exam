// To parse this JSON data, do
//
//     final resultModel = resultModelFromJson(jsonString);

import 'dart:convert';

ResultModel resultModelFromJson(String str) => ResultModel.fromJson(json.decode(str));

String resultModelToJson(ResultModel data) => json.encode(data.toJson());

class ResultModel {
  ResultModel({
    this.status,
    this.result,
    this.message,
  });

  int? status;
  ResultModelResult? result;
  String? message;

  factory ResultModel.fromJson(Map<String, dynamic> json) => ResultModel(
    status: json["status"] == null ? null : json["status"],
    result: json["result"] == null ? null : ResultModelResult.fromJson(json["result"]),
    message: json["message"] == null ? null : json["message"],
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "result": result == null ? null : result!.toJson(),
    "message": message == null ? null : message,
  };
}

class ResultModelResult {
  ResultModelResult({
    this.score,
    this.rightAnswer,
    this.totalQuewstion,
    this.result,
  });

  var score;
  var rightAnswer;
  var totalQuewstion;
  List<ResultElement>? result;

  factory ResultModelResult.fromJson(Map<String, dynamic> json) => ResultModelResult(
    score: json["score"] == null ? null : json["score"],
    rightAnswer: json["rightAnswer"] == null ? null : json["rightAnswer"],
    totalQuewstion: json["totalQuewstion"] == null ? null : json["totalQuewstion"],
    result: json["questionList"] == null ? null : List<ResultElement>.from(json["questionList"].map((x) => ResultElement.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "score": score == null ? null : score,
    "rightAnswer": rightAnswer == null ? null : rightAnswer,
    "totalQuewstion": totalQuewstion == null ? null : totalQuewstion,
    "questionList": result == null ? null : List<dynamic>.from(result!.map((x) => x.toJson())),
  };
}

class ResultElement {
  ResultElement({
    this.userId,
    this.topicId,
    this.subTopicId,
    this.questionId,
    this.answerId,
    this.isCorrect,
    this.id,
    this.questions,
    this.answerDescription,
    this.answerOptions,
  });

  var userId;
  var topicId;
  var subTopicId;
  var questionId;
  var answerId;
  var isCorrect;
  var id;
  var questions;
  var answerDescription;
  List<AnswerOption>? answerOptions;

  factory ResultElement.fromJson(Map<String, dynamic> json) => ResultElement(
    userId: json["userId"] == null ? null : json["userId"],
    topicId: json["topicId"] == null ? null : json["topicId"],
    subTopicId: json["subTopicId"] == null ? null : json["subTopicId"],
    questionId: json["questionId"] == null ? null : json["questionId"],
    answerId: json["answerId"] == null ? null : json["answerId"],
    isCorrect: json["isCorrect"] == null ? null : json["isCorrect"],
    id: json["id"] == null ? null : json["id"],
    questions: json["questions"] == null ? null : json["questions"],
    answerDescription: json["answerDescription"] == null ? null : json["answerDescription"],
    answerOptions: json["answerOptions"] == null ? null : List<AnswerOption>.from(json["answerOptions"].map((x) => AnswerOption.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "userId": userId == null ? null : userId,
    "topicId": topicId == null ? null : topicId,
    "subTopicId": subTopicId == null ? null : subTopicId,
    "questionId": questionId == null ? null : questionId,
    "answerId": answerId == null ? null : answerId,
    "isCorrect": isCorrect == null ? null : isCorrect,
    "id": id == null ? null : id,
    "questions": questions == null ? null : questions,
    "answerDescription": answerDescription == null ? null : answerDescription,
    "answerOptions": answerOptions == null ? null : List<dynamic>.from(answerOptions!.map((x) => x.toJson())),
  };
}

class AnswerOption {
  AnswerOption({
    this.id,
    this.questionId,
    this.answer,
    this.isCorrect,
    this.position,
  });

  var id;
  var questionId;
  var answer;
  var isCorrect;
  var position;

  factory AnswerOption.fromJson(Map<String, dynamic> json) => AnswerOption(
    id: json["id"] == null ? null : json["id"],
    questionId: json["questionId"] == null ? null : json["questionId"],
    answer: json["answer"] == null ? null : json["answer"],
    isCorrect: json["isCorrect"] == null ? null : json["isCorrect"],
    position: json["position"] == null ? null : json["position"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "questionId": questionId == null ? null : questionId,
    "answer": answer == null ? null : answer,
    "isCorrect": isCorrect == null ? null : isCorrect,
    "position": position == null ? null : position,
  };
}
