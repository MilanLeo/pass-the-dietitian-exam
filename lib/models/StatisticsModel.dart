// To parse this JSON data, do
//
//     final statisticsModel = statisticsModelFromJson(jsonString);

import 'dart:convert';

StatisticsModel statisticsModelFromJson(String str) =>
    StatisticsModel.fromJson(json.decode(str));

String statisticsModelToJson(StatisticsModel data) =>
    json.encode(data.toJson());

class StatisticsModel {
  StatisticsModel({
    this.status,
    this.overAllStatistics,
    this.result,
    this.message,
  });

  int? status;
  double? overAllStatistics;
  List<Statistics>? result;
  String? message;

  factory StatisticsModel.fromJson(Map<String, dynamic> json) =>
      StatisticsModel(
        status: json["status"] == null ? null : json["status"],
        overAllStatistics: json["overAllStatistics"] == null
            ? null
            : json["overAllStatistics"].toDouble(),
        result: json["result"] == null
            ? null
            : List<Statistics>.from(
                json["result"].map((x) => Statistics.fromJson(x))),
        message: json["message"] == null ? null : json["message"],
      );

  Map<String, dynamic> toJson() => {
        "status": status == null ? null : status,
        "overAllStatistics":
            overAllStatistics == null ? null : overAllStatistics,
        "result": result == null
            ? null
            : List<dynamic>.from(result!.map((x) => x.toJson())),
        "message": message == null ? null : message,
      };
}

class Statistics {
  Statistics(
      {this.id,
      this.title,
      this.resultBarColor,
      this.imageUrl,
      this.percentage,
      this.isShowBar});

  var id;
  var title;
  var resultBarColor;
  var imageUrl;
  var percentage;
  var isShowBar;
  factory Statistics.fromJson(Map<String, dynamic> json) => Statistics(
        id: json["id"] == null ? null : json["id"],
        title: json["title"] == null ? null : json["title"],
        resultBarColor:
            json["resultBarColor"] == null ? null : json["resultBarColor"],
        imageUrl: json["imageUrl"] == null ? null : json["imageUrl"],
        percentage:
            json["percentage"] == null ? null : json["percentage"].toDouble(),
        isShowBar: json["isShowBar"] == null ? null : json["isShowBar"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "title": title == null ? null : title,
        "resultBarColor": resultBarColor == null ? null : resultBarColor,
        "imageUrl": imageUrl == null ? null : imageUrl,
        "percentage": percentage == null ? null : percentage,
        "isShowBar": isShowBar == null ? null : isShowBar,
      };
}
