// To parse this JSON data, do
//
//     final subTopicModel = subTopicModelFromJson(jsonString);

import 'dart:convert';

SubTopicModel subTopicModelFromJson(String str) => SubTopicModel.fromJson(json.decode(str));

String subTopicModelToJson(SubTopicModel data) => json.encode(data.toJson());

class SubTopicModel {
  SubTopicModel({
    this.status,
    this.result,
    this.message,
  });

  int? status;
  Result? result;
  String? message;

  factory SubTopicModel.fromJson(Map<String, dynamic> json) => SubTopicModel(
    status: json["status"] == null ? null : json["status"],
    result: json["result"] == null ? null : Result.fromJson(json["result"]),
    message: json["message"] == null ? null : json["message"],
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "result": result == null ? null : result!.toJson(),
    "message": message == null ? null : message,
  };
}

class Result {
  Result({
    this.subTopicList,
  });

  List<SubTopicList>? subTopicList;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
    subTopicList: json["subTopicList"] == null ? null : List<SubTopicList>.from(json["subTopicList"].map((x) => SubTopicList.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "subTopicList": subTopicList == null ? null : List<dynamic>.from(subTopicList!.map((x) => x.toJson())),
  };
}

class SubTopicList {
  SubTopicList({
    this.id,
    this.parentId,
    this.title,
    this.image,
    this.mockRule,
    this.time,
    this.isFree,
    this.isCompletedAnswer,
    this.isExam,
    this.totalQuestionCount,
  });

  var id;
  var parentId;
  var title;
  var image;
  var mockRule;
  var time;
  var isFree;
  var isExam;
  var isCompletedAnswer;
  var totalQuestionCount;

  factory SubTopicList.fromJson(Map<String, dynamic> json) => SubTopicList(
    id: json["id"] == null ? null : json["id"],
    parentId: json["parentId"] == null ? null : json["parentId"],
    title: json["title"] == null ? null : json["title"],
    image: json["image"] == null ? null : json["image"],
    mockRule: json["mockRule"] == null ? null : json["mockRule"],
    time: json["time"] == null ? null : json["time"],
    isFree: json["isFree"] == null ? null : json["isFree"],
    isCompletedAnswer: json["isCompletedAnswer"] == null ? null : json["isCompletedAnswer"],
    isExam: json["isExam"] == null ? null : json["isExam"],
    totalQuestionCount: json["totalQuestionCount"] == null ? null : json["totalQuestionCount"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "parentId": parentId == null ? null : parentId,
    "title": title == null ? null : title,
    "image": image == null ? null : image,
    "mockRule": mockRule == null ? null : mockRule,
    "time": time == null ? null : time,
    "isFree": isFree == null ? null : isFree,
    "isCompletedAnswer": isCompletedAnswer == null ? null : isCompletedAnswer,
    "isExam": isExam == null ? null : isExam,
    "totalQuestionCount": totalQuestionCount == null ? null : totalQuestionCount,
  };
}
