// To parse this JSON data, do
//
//     final topicModel = topicModelFromJson(jsonString);

import 'dart:convert';

TopicModel topicModelFromJson(String str) =>
    TopicModel.fromJson(json.decode(str));

String topicModelToJson(TopicModel data) => json.encode(data.toJson());

class TopicModel {
  TopicModel({
    this.status,
    this.result,
    this.message,
  });

  int? status;
  Result? result;
  String? message;

  factory TopicModel.fromJson(Map<String, dynamic> json) => TopicModel(
        status: json["status"] == null ? null : json["status"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
        message: json["message"] == null ? null : json["message"],
      );

  Map<String, dynamic> toJson() => {
        "status": status == null ? null : status,
        "result": result == null ? null : result!.toJson(),
        "message": message == null ? null : message,
      };
}

class Result {
  Result({
    this.topicList,
  });

  List<TopicList>? topicList;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        topicList: json["topicList"] == null
            ? null
            : List<TopicList>.from(
                json["topicList"].map((x) => TopicList.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "topicList": topicList == null
            ? null
            : List<dynamic>.from(topicList!.map((x) => x.toJson())),
      };
}

class TopicList {
  TopicList(
      {this.id,
      this.title,
      this.image,
      this.imageUrl,
      this.mockRule,
      this.time,
      this.isFree,
      this.isExam,
      this.questionCounts});

  var id;
  var title;
  var image;
  var imageUrl;
  List<dynamic>? mockRule;
  var time;
  var isFree;
  var isExam;
  var questionCounts;

  factory TopicList.fromJson(Map<String, dynamic> json) => TopicList(
        id: json["id"] == null ? null : json["id"],
        title: json["title"] == null ? null : json["title"],
        image: json["image"] == null ? null : json["image"],
        imageUrl: json["imageUrl"] == null ? null : json["imageUrl"],
        mockRule: json["mockRule"] == null
            ? null
            : List<dynamic>.from(json["mockRule"].map((x) => x)),
        time: json["time"] == null ? null : json["time"],
        isFree: json["isFree"] == null ? null : json["isFree"],
        isExam: json["isExam"] == null ? null : json["isExam"],
        questionCounts:
            json["questionCounts"] == null ? null : json["questionCounts"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "title": title == null ? null : title,
        "image": image == null ? null : image,
        "imageUrl": imageUrl == null ? null : imageUrl,
        "mockRule": mockRule == null
            ? null
            : List<dynamic>.from(mockRule!.map((x) => x)),
        "time": time == null ? null : time,
        "isFree": isFree == null ? null : isFree,
        "isExam": isExam == null ? null : isExam,
        "questionCounts": questionCounts == null ? null : questionCounts,
      };
}
