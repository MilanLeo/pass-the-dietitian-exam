// To parse this JSON data, do
//
//     final subscriptionModel = subscriptionModelFromJson(jsonString);

import 'dart:convert';

SubscriptionModel subscriptionModelFromJson(String str) => SubscriptionModel.fromJson(json.decode(str));

String subscriptionModelToJson(SubscriptionModel data) => json.encode(data.toJson());

class SubscriptionModel {
  SubscriptionModel({
    this.status,
    this.result,
    this.message,
  });

  int? status;
  Result? result;
  String? message;

  factory SubscriptionModel.fromJson(Map<String, dynamic> json) => SubscriptionModel(
    status: json["status"] == null ? null : json["status"],
    result: json["result"] == null ? null : Result.fromJson(json["result"]),
    message: json["message"] == null ? null : json["message"],
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "result": result == null ? null : result!.toJson(),
    "message": message == null ? null : message,
  };
}

class Result {
  Result({
    this.planList,
  });

  List<PlanList>? planList;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
    planList: json["planList"] == null ? null : List<PlanList>.from(json["planList"].map((x) => PlanList.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "planList": planList == null ? null : List<dynamic>.from(planList!.map((x) => x.toJson())),
  };
}

class PlanList {
  PlanList({
    this.id,
    this.isInApp,
    this.planName,
    this.price,
    this.description,
  });

  var id;
  var isInApp;
  var planName;
  var price;
  List<String>? description;

  factory PlanList.fromJson(Map<String, dynamic> json) => PlanList(
    id: json["id"] == null ? null : json["id"],
    isInApp: json["isInApp"] == null ? null : json["isInApp"],
    planName: json["planName"] == null ? null : json["planName"],
    price: json["price"] == null ? null : json["price"].toDouble(),
    description: json["description"] == null ? null : List<String>.from(json["description"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "isInApp": isInApp == null ? null : isInApp,
    "planName": planName == null ? null : planName,
    "price": price == null ? null : price,
    "description": description == null ? null : List<dynamic>.from(description!.map((x) => x)),
  };
}
