import 'package:get/get.dart';

class UIController extends GetxController {
  var isLogin = false.obs;
  var deviceToken = "".obs;
  var isFirstTime = true;
  var isSubscribe = 0.obs;
  var userName = "".obs;
  var profileImage = "".obs;
  var email = "".obs;
  var firstName = "".obs;
  var lastName = "".obs;
  var startDate = "".obs;
  var planName = "".obs;
  var planPrice = "".obs;
  var id ;
  var isPurchase = false;
}
